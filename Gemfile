source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'json', '1.8.1'
gem 'rails', '4.1.6'
# Use sqlite3 as the database for Active Record
gem 'sqlite3'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer',  platforms: :ruby

gem 'bootstrap-sass'

gem 'haml'
gem 'haml-rails'

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
#gem 'spring',        group: :development

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

#Required for windows dev
#gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw]

gem "pg"
gem "gibbon"
gem "devise"
gem "delayed_paperclip"
gem "rails_admin"
gem "hopscotch-rails"
gem "animate-rails"
gem "inkwell", git: "git://github.com/WiechersV/inkwell.git"
gem "awesome_nested_set"
gem "impressionist"
gem "bcrypt-ruby", require: "bcrypt"
gem "friendly_id", "~> 5.0.0"
gem "paperclip", "~> 4.1"
gem "paper_trail", "~> 3.0.5"
gem "aws-sdk"
gem "s3_direct_upload"
gem "pagedown-rails", "~> 1.1.3"
gem "sanitize"
gem "http_accept_language"
gem "meta-tags"
gem "nprogress-rails"
gem "kaminari"
gem "bitly", "~> 0.10.1"
#gem 'twitter'
gem "posix-spawn"
gem "delayed_job_active_record"
gem "daemon-spawn"
gem "koala"
gem "omnicontacts"
gem "mandrill_mailer"

gem "omniauth-instagram"
gem "omniauth-facebook"
gem "omniauth-twitter"
gem "dynamic_sitemaps"

gem "ckeditor"
gem "newrelic_rpm"

gem "elasticsearch"
gem "elasticsearch-model"
gem "elasticsearch-rails"
gem "elasticsearch-extensions"

gem "intercom-rails"

gem "countries"
gem "country_select"
gem "phony_rails"
gem "jquery-inputmask-rails"

group :production do
    gem "unicorn"
    gem "whenever"
end

group :development, :test do
    gem "rspec", "~> 3.0"
    gem "rspec-rails", "~> 3.0"
    gem "factory_girl_rails", "~> 4.5.0"
    gem "minitest"
    gem "autotest"
    gem "autotest-standalone"
    gem "quiet_assets"
    gem "rubycritic", :require => false
    gem "thin"
    gem "rack-mini-profiler"
    gem "traceroute"
    gem "bullet"
end

group :test do
    gem "faker", "~> 1.4.3"
    gem "capybara", "~> 2.4.4"
    gem "database_cleaner", "~> 1.0.1"
    gem "launchy", "~> 2.4.3"
    gem "selenium-webdriver", "~> 2.35.1"
end

