# Scribe  

[Scribe](http://wescribe.co) works using:  

* Linux or OSX. Please, please, PLEASE, avoid Windows, unless you're developing on C# or .NET... Whatever xD
* Ruby 2.1.2  
* Rails 4.1.6  
* PostgreSQL  
* [Elasticsearch 1.4.2](http://blog.bekijkhet.com/2014/06/install-elasticsearch-in-ubuntu-1404.html) **1**  
* [PNGCrush](http://linuxpoison.blogspot.com/2010/01/pngcrush-optimize-png-file-image-to.html) **2**  
* [JPEGOptim](http://baptiste-wicht.com/posts/2010/07/tip-optimize-images-on-ubuntu-linux.html) **2**  
  
**1** - Replace 1.3 with 1.4 on the step that requires you to add the elasticsearch repository on the sources list  
**2** - Not absolutely required. These are used by [Paperclip optimizer](https://github.com/janfoeh/paperclip-optimizer). To disable, add `jpegoptim: false` and/or `pngcrush: false`to [this](https://bitbucket.org/becoldasice/scribe/src/251910147ce65ccadd80b25468ef71ae802f4f6a/config/application.rb?at=v0.0.01#cl-27) and [this](https://bitbucket.org/becoldasice/scribe/src/251910147ce65ccadd80b25468ef71ae802f4f6a/config/initializers/paperclip_optimizer.rb?at=v0.0.01#cl-10) file.  

# Steps to get Scribe up and running at your dev machine:  
1. Most important right now: **DO NOT USE** the master branch. Scribe currently works in the v0.0.01 branch. Master is currently outdated and unfit for development.  
2. git clone git@bitbucket.org:becoldasice/scribe.git / git clone https://becoldasice@bitbucket.org/becoldasice/scribe.git  
3. git checkout v0.0.01 (**!!!!!!!!!!**)  
4. git pull origin v0.0.01 (Just in case...)  
5. bundle install (Make sure you're currently using ruby 2.1.2 and Scribe gemset. I like to use [RVM](https://rvm.io/) to manage this)  
6. rake db:setup  
7. rake db:migrate  
8. rake db:seed (**Very important step**, since Scribe landing page require some existent texts on the database. You can also use a dumped version of the database. Contact me on Slack and I'll send you :D)  
8.1. If you decide to get the dump, place it under the db/ folder and run `rake db:restore`
9. [bundle exec] thin start -d  
10. Have fun!

P.S.: Scribe works in 3 different languages (English, Spanish and Portuguese) and use subdomains for that. To do so, you should edit your /etc/hosts and add the following lines there:
```
127.0.0.1       localhost.local
127.0.0.1       en.localhost.local
127.0.0.1       es.localhost.local
```
