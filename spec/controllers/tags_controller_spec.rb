require 'spec_helper'

describe TagsController do

  describe "GET#index" do
    it "renders the show page" do
      get :index
      expect(response).to render_template :index
    end

    it "assigns a new user if not logged" do
      get :index
      expect(assigns(:user)).to be_a_new(User)
    end

  end

  describe "GET#load_texts", elasticsearch: true do

    it "redirects to root if request isn't ajax" do
      get :load_texts, tags: ''
      expect(response).to redirect_to root_url
    end

    describe "search with one tag" do

      context "with results" do

        before :all do
          @text = create(:text)
          @tag = create(:tag)
          @text.insert_tag(@tag.attributes.symbolize_keys)
          @text.publish
          Tag.import force: true, refresh: true
          Text.import force: true, refresh: true
        end

        it "includes text in the results" do
          xhr :get, :load_texts, {tags: @tag.name}
          expect(assigns(:texts)).to match_array([@text])
        end

        it "renders texts blocks if text is found" do
          xhr :get, :load_texts, {tags: @tag.name}
          expect(response).to render_template(partial: '_blocks')
        end

      end

      context "search without results" do
        it "doesn't assign a variable to texts" do
          xhr :get, :load_texts, {tags: 'AAAAAAAAAAAAAAAAAAAAAAAAA'}
          expect(assigns(:texts).total).to eq(0)
        end

        it "returns message if no texts were found" do
          xhr :get, :load_texts, {tags: 'AAAAAAAAAAAAAAAAAAAAAAAAA'}
          expect(response.body).to eq(I18n.t(:no_texts_with_tags))
        end
      end

      context "search with empty string" do
        it "returns a message asking for search" do
          xhr :get, :load_texts, {tags: ''}
          expect(response.body).to eq(I18n.t(:select_one_tag))
        end
      end
    end


    describe "search with multiple tags" do
      context "with results" do
        
        before :all do
          @text = create(:text)
          @tags = create_list(:tag, 2)
          @tags.each { |tag| @text.insert_tag(tag.attributes.symbolize_keys) }
          @text.publish
          Tag.import force: true, refresh: true
          Text.import force: true, refresh: true
        end

        it "includes text in the results" do
          xhr :get, :load_texts, {tags: @tags.map { |tag| tag.name }.join(",")}
          expect(assigns(:texts).records).to include(@text)
        end

        it "renders texts blocks if text is found" do
          xhr :get, :load_texts, {tags: @tags.map { |tag| tag.name }.join(",")}
          expect(response).to render_template(partial: '_blocks')
        end
      end

    end
  end

end