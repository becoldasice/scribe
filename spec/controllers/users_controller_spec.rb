require 'spec_helper'

describe UsersController do

  describe "POST#create" do
    
    it "creates a new user" do
      expect {
        post :create, user: attributes_for(:user), format: :json
      }.to change(User, :count).by(1)
    end

    it "redirects to discover page" do
      post :create, user: attributes_for(:user), format: :json
      @json = JSON.parse(response.body).symbolize_keys
      expect(@json[:url]).to eq(discover_url)
    end

  end

end
