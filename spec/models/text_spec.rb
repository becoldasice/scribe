require 'spec_helper'

describe Text do

  before :each do
    @text = build(:text)
  end

  context "text creation" do
    it "is valid" do
      expect(@text).to be_valid
    end

    it "is a draft" do
      expect(@text.is_draft).to be true
    end

    it "is invalid without user" do
      @text.user = nil
      @text.save
      expect(@text.errors[:user].size).to eq(1)
    end
  end

  context "text publishing" do
    before :each do
      @text.save
    end

    it "is not a draft anymore" do
      expect(@text.publish).to be true
    end

    it "can't be published without title" do
      @text.update({title: nil})
      expect(@text.publish).to be false
    end

    it "can't be published without description" do
      @text.update({description: nil})
      expect(@text.publish).to be false
    end

    it "can't be published without language" do
      @text.update({language_id: nil})
      expect(@text.publish).to be false
    end

    it "can't be published without category" do
      @text.update({category_id: nil})
      expect(@text.publish).to be false
    end
  end

end

# describe Text do

#   before(:all) {
#     @text = FactoryGirl.create(:text)
#   }

#   context "creation behavior" do

#     it "is valid text" do
#     	expect(@text).to be_valid
#     end

#     it "contains valid tags" do
#     	expect(@text.tags).not_to be_nil
#     end

#     it "is still a draft" do
#     	expect(@text.is_draft).to eql(true)
#     end
#   end

#   context "publication behavior: " do

#     it "is a published text" do
#     	expect(@text.publish).to eql(true)
#     end

#     context "isn't a published text because" do

#       it "doesn't have a title" do
#       	expect(FactoryGirl.build(:text, title: nil).publish).to eql(false)
#       end

#       it "doesn't have a description" do
#       	FactoryGirl.build(:text, description: nil).publish.should_not be_valid
#       end

#       it "doesn't have a category" do
#       	FactoryGirl.build(:text, category: nil).publish.should_not be_valid
#       end

#       it "doesn't have a language" do
#       	expect(FactoryGirl.build(:text, language: nil).publish).to eql(false)
#       end
#     end
#   end

#   context "user interaction behavior: " do

#     before(:all) {
#       FactoryGirl.create(:user)
#     }

#     context "recommendations: " do

#       before {
#         @text.publish if @text.is_draft
#       }

#       it "increase recommendations count" do
#         expect{
#           User.last.recommend(Text.last)
#         }.to change{Text.last.recommendations_count}.by(1)
#       end

#       it "decrease recommendations count" do
#         User.last.recommend(Text.last)
#         expect{
#           User.last.recommend(Text.last)
#         }.to change{Text.last.recommendations_count}.by(-1)
#       end
#     end
#   end
# end