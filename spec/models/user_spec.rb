require 'spec_helper'

describe User do

  context "user creation" do

    describe "valid user" do
      
      before :each do
        @user = FactoryGirl.create(:user)
      end

      it "is valid" do
        expect(@user).to be_valid
      end

      it "is active" do
        expect(@user.is_active).to be true
      end

      it "contain preferences" do
        expect(@user.preferences).to be_valid
      end
    end

    describe "invalid user" do
    end

  end
end

# describe User do

#   before(:all)	do
#   	@user = FactoryGirl.create(:user)
#   end

#   context "creation behavior: " do

#     it "confirms email" do
#       expect(@user.email_confirmation).to eql(@user.email)
#     end

#     it "confirms password" do
#       expect(@user.password_confirmation).to eql(@user.password)
#     end

#     it "create a valid new user" do
#     	expect(@user).to be_valid
#     end

#     it "generates a valid confirmation token" do
#       expect(@user.confirmation_token).not_to be_nil
#     end

#     it "does not allow duplicated email addresses" do
#     	expect(FactoryGirl.build(:user, email: @user.email)).not_to be_valid
#     end

#     it "does not create a user without email" do
#     	expect(FactoryGirl.build(:user, email: nil)).not_to be_valid
#     end

#     it "does not create a user without a valid email" do
#     	expect(FactoryGirl.build(:user, email: 'Blah')).not_to be_valid
#     end

#     it "does not create a user without password" do
#     	expect(FactoryGirl.build(:user, password: nil)).not_to be_valid
#     end

#     it "creates a password_hash for the created user" do
#     	expect(@user.password_hash).not_to be_nil
#     end
#   end

#   context "authentication behavior: " do

#     it "authenticate successfully an user and return the object" do
#     	expect(User.authenticate(@user.email, @user.password)).to eq(@user)
#     end

#     it "returns nil if user account is deactivated" do
#       @user.deactivate_account
#       expect(User.authenticate(@user.email, @user.password)).to be_nil
#     end

#     it "returns nil if authentication fail with wrong password" do
#     	expect(User.authenticate(@user.email, "AAAAAAAAAAAAAAAAAA")).to be_nil
#     end

#     it "returns nil if authentication fail with wrong email" do
#     	expect(User.authenticate('aedaedaed@aeadaedaed.com', "asdasdasdasd")).to be_nil
#     end
#   end

#   context "interacting with other users: " do
    
#     before {
#       @another = FactoryGirl.create(:user)
#     }

#     context "follow another user" do

#       before {
#         @following_count = @user.following_count
#         @follower_count = @another.follower_count
#         @user.follow(@another)
#       }

#       it { 
#         expect(@user.follow?(@another)).to eql(true) 
#       }

#       it "increase follower user following count" do
#         expect(@user.following_count).to eql(@following_count+1)
#       end

#       it "increase followed user followers count" do
#         expect(@another.follower_count).to eql(@follower_count+1)
#       end

#       it "doesn't allow user to follow again" do
#         expect{@user.follow(@another)}.to raise_error
#       end

#       context "timeline behavior: " do

#         before {
#           @text = FactoryGirl.create(:text, user: @another)
#         }

#         it "doesn't increase timeline" do
#           expect {
#             @text
#           }.not_to change{@user.timeline_size}
#         end

#         it "increase timeline when followed user publish text" do
#           expect{
#             @text.publish
#           }.to change{@user.timeline_size}.by(1)
#         end

#         it "decrease timeline when followed user destroy text " do
#           @text.publish
#           expect {
#             @another.texts.destroy(@text)
#           }.to change{@user.timeline_size}.by(-1)
#         end
#       end
#     end
#   end

#   context "deactivation: " do
#     before {
#       @mail = @user.deactivate_account
#     }

#     it "deactivates user account" do
#       expect(@user.is_active).to eql(false)
#     end
#   end
# end