require 'spec_helper'

describe Tag do

  context "tag creation" do

    before :all do
      @tag = create(:tag)
    end

    it "is a valid tag with name" do
      expect(@tag).to be_valid
    end

    it "is invalid if already exists" do
      invalid_tag = build(:tag, {name: @tag.name})
      invalid_tag.save
      expect(invalid_tag.errors[:name].size).to eq(1)
    end
  end

  context "text interaction", elasticsearch: true do

    before :all do
      @tag = create(:tag)
      @text = create(:text)
      @tag = @text.insert_tag(@tag.attributes.symbolize_keys)
    end

    it "increases tags counter" do
      expect(@tag.texts_count).to eq(1)
    end

    it "includes text in search results" do
      @text.publish
      Tag.import force: true, refresh: true
      Text.import force: true, refresh: true

      results = Text.search_for({
        fields: ['tags.name'],
        filter: {bool: {must: {term: {is_draft: false}}}}
      },@tag.name)

      expect(results.records.include?(@text)).to be true
    end

    it "decreases tags counter" do
      @text.tags.delete(@tag)
      expect(@tag.texts_count).to eq(0)
    end

    context "search for multiple tags" do
      before :all do
        @text.reload
        @text.insert_tag(@tag.attributes.symbolize_keys)
        @new_tag = create(:tag)
        @query = "#{@tag.name} #{@new_tag.name}"
      end

      it "includes text in search results" do
        @text.insert_tag(@new_tag.attributes.symbolize_keys)
        @text.publish
        
        Text.import force: true, refresh: true

        results = Text.search_for({
          fields: ['tags.name'],
          operator: 'AND',
          minimum_should_match: "100%",
          filter: {bool: {must: {term: {is_draft: false}}}}
        }, @query)

        expect(results.records.include?(@text)).to be true
      end

      it "doesn't include text in search results" do
        @text.publish
        Text.import force: true, refresh: true

        results = Text.search_for({
          fields: ['tags.name'],
          operator: 'AND',
          minimum_should_match: "100%",
          filter: {bool: {must: {term: {is_draft: false}}}}
        }, @query)

        expect(results.records.include?(@text)).to be false
      end
    end

  end
end