require 'spec_helper'

# describe Category do

#   before(:all) do
#   	I18n.locale = Language.find(1).locale
    
#     for x in 1..10 do
#       id = Text.last.blank? ? 0 : Text.last.id
#       FactoryGirl.build(:text, id: (x+id), category_id: 2, language_id: 1, user: FactoryGirl.create(:user)).publish
#       Text.last.published_at = Time.new(2008,x)
#       Text.last.impressions = [] if x > 2
#       User.last.recommend(Text.last) if x % 2 == 0
#     end
    
#   end

#   let (:category) {
#   	Category.find(2)
#   }

#   context "last texts" do
    
#     let (:last_texts) {
#       category.list_texts_by_type(ScribeConstants::LAST_TEXTS)
#     }

#     it "first item was published after last" do
#       expect(last_texts.first.published_at).to be > last_texts.last.published_at
#     end
#   end

#   context "most viewed" do
    
#     let (:most_viewed) {
#       category.list_texts_by_type(ScribeConstants::MOST_VIEWED)
#     }

#     it "first item have more views than the last one" do
#       expect(most_viewed.first.impressionist_count(filter: :ip_address).to_i).to be > most_viewed.last.impressionist_count(filter: :ip_address).to_i
#     end
#   end

#   context "top ten" do

#     let (:top_ten) {
#       category.list_texts_by_type(ScribeConstants::TOP_TEN,1,10)
#     }

#     it "contains only 10 itens" do
#       expect(top_ten.size).to eq(10)
#     end

#     it "first item have more recommendations than the last one" do
#       expect(top_ten.first.recommendations_count.to_i).to be > top_ten.last.recommendations_count.to_i
#     end
#   end
# end