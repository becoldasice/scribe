require 'faker'

FactoryGirl.define do

	factory :user do
		association :preferences, factory: :user_preference
		first_name { Faker::Name.first_name }
		last_name { Faker::Name.last_name }
		email { Faker::Internet.free_email(first_name) }
		email_confirmation { "#{email}" }
		password { Faker::Internet.password }
		password_confirmation { "#{password}" }
		country_code { 'BR' }
		phone_country_code { Country['BR'].country_code }
		whatsapp_number { '(21) 981266510' }
		gender 'M'
	end
end