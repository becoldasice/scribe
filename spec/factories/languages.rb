require 'faker'

FactoryGirl.define do
  factory :language do
    sequence(:name) { |n| "#{Faker::Lorem.word}#{n}" }
    locale { Faker::Lorem.words(rand(1..8)).join('-').downcase }
  end
end