require 'faker'

FactoryGirl.define do
  factory :category do
    sequence(:name) { |n| "#{Faker::Lorem.word}#{n}"}
    about { Faker::Lorem.sentence }
    locale_key { Faker::Lorem.words(3).join('_') }
  end
end