# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :notification do
    type ""
    user_id 1
    seen false
    entities "MyText"
    target 1
  end
end
