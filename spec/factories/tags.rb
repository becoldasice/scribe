require 'faker'

FactoryGirl.define do
  to_create do |instance|
    unless instance.save
      raise "Invalid record: #{instance.class.model_name} : #{instance.errors.full_messages.join(", ")}"
    end
  end
end


FactoryGirl.define do
  factory :tag do 
    sequence(:name) { |n| "#{Faker::Lorem.word}#{n}" }
  end
end