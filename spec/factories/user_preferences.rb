FactoryGirl.define do
  factory :user_preference do
    email_on_follow true
    email_on_recommendations true
    email_top_ten true
    email_weekly_writers true
    email_week_trends true
    email_steve_suggestions true
    get_whatsapp_texts true
  end

end
