require 'faker'

FactoryGirl.define do

	factory :text do
		user
		category
		language
		title {Faker::Lorem.sentence(3)}
		description {Faker::Lorem.paragraph(20)}
		impressions { [FactoryGirl.create(:impression)] }
	end

	factory :impression do
		impressionable_type "Text"
		controller_name "texts"
		request_hash nil
		session_hash nil
		action_name "show"
		ip_address { Faker::Internet.ip_v4_address }
		view_name nil
		referrer nil
		message nil
	end

end