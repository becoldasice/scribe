# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'capybara/rspec'
require 'rake'
require 'elasticsearch/extensions/test/cluster/tasks'
require 'faker'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }
ENV["TEST_CLUSTER_NODES"] = "1"

## Change for different envs
## LINUX
ENV["TEST_CLUSTER_COMMAND"] = '/usr/share/elasticsearch/bin/elasticsearch'
## OSX  
## ENV["TEST_CLUSTER_COMMAND"] = '/usr/local/Cellar/elasticsearch/1.4.1/bin/elasticsearch'

# Checks for pending migrations before tests are run.
# If you are not using ActiveRecord, you can remove this line.
ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods
  config.include LoginHelper, :type => :controller
  config.include FeaturesHelper, :type => :feature
  config.infer_spec_type_from_file_location!
  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = "default"

  Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end

  config.before :suite, type: :feature do
    %x[RAILS_ENV=test rake assets:precompile --trace]
    %x[RAILS_ENV=test rake assets:clean --trace]
  end

  config.before :suite do
    if Capybara.current_driver == :rack_test
      DatabaseCleaner.strategy = :transaction
    else
      DatabaseCleaner.strategy = :truncation
    end
  end

  config.before :all, elasticsearch: true do
    Elasticsearch::Extensions::Test::Cluster.start(port: 9250) unless Elasticsearch::Extensions::Test::Cluster.running?(on: 9250)
    %w(Text Tag User).each do |name|
      model = Object.const_get(name)
      model.import force: true, refresh: true
      sleep 1
    end
    

    begin
      DatabaseCleaner.start
      FactoryGirl.lint
    ensure
      DatabaseCleaner.clean
    end
    
  end

  config.after :suite do
    %w(Text Tag User).each do |name|
      model = Object.const_get(name)
      model.__elasticsearch__.client.indices.delete index: model.index_name
    end
    Elasticsearch::Extensions::Test::Cluster.stop(port: 9250) if Elasticsearch::Extensions::Test::Cluster.running?(on: 9250)
  end

  config.after :all do
    DatabaseCleaner.clean
  end

end
Faker::Config.locale = :en
Capybara.always_include_port = true
Capybara.javascript_driver = :selenium