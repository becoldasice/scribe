module FeaturesHelper

  def populate_tags
    @tags = create_list(:tag, 5)
    @texts = create_list(:text, 10)

    @texts.each_with_index do |text, index|
      insert = @tags.first(rand(0..2))
      insert.each do |tag|
        text.insert_tag(tag.attributes.symbolize_keys)
      end
      @tags.sort!{|x,y| index.odd? ? y<=>x : x<=>y}
      text.publish
    end
    Tag.import force: true, refresh: true
    Text.import force: true, refresh: true
  end
  
end