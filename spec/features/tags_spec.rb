require "spec_helper"

feature 'Select tags', elasticsearch: true, js: true do
  background do
    populate_tags
  end
  
  scenario 'enters #index page and select tag from cloud' do
    visit tags_path
    selected = find('.tagsCloud--container > .tagsList--tag.isSelected')
    
    within '.tagsSection--highlightedTag' do
      expect(page).to have_content selected.text
    end

    within '.tagsSection--selectedTagsList' do
      expect(page).to have_content selected.text
    end

    not_selected = first('.tagsCloud--container > .tagsList--tag:not(.isSelected)')
    not_selected.click

    within '.tagsSection--highlightedTag' do
      expect(page).to have_content not_selected.text
    end

    expect(all('.tagsSection--selectedTagsList > .tagsList--tag').last['data-tag']).to eq(not_selected.text)
  end

end

feature 'Remove tags', elasticsearch: true, js: true do

  background do 
    populate_tags
  end

  scenario 'enters #index page and remove cloud selected tag by clicking on itself' do
    visit tags_path

    find('.tagsCloud--container > .tagsList--tag.isSelected').click
    expect(page).to have_content I18n.t(:select_one_tag)

    expect(first('.tagsSection--highlightedTag').text).to eq('tag')
  end

  scenario 'enters #index page and remove cloud selected tag by clicking on remove button' do
    visit tags_path

    find('.tagsSection--selectedTagsList > .tagsList--tag').find('.js-removeTag').click

    expect(page).to have_content I18n.t(:select_one_tag)

    expect(find('.tagsSection--highlightedTag').text).to eq('tag')
  end

end

feature 'Search tags', elasticsearch: true, js: true do
  background do
    populate_tags
  end

  scenario 'enters #index page and search for tag' do
    visit tags_path
    find('.tagsCloud--container > .tagsList--tag.isSelected').click

    tag_to_search = Tag.where('texts_count > ?', 0).first
    fill_in 'tag', with: tag_to_search.name
    
    tag_search = find('#tag')
    tag_search.native.send_key(:enter)

    within '.tagsSection--highlightedTag' do
      expect(page).to have_content tag_search.value
    end

    within '.tagsSection--textsContainer' do
      expect(page).to have_content tag_to_search.texts.first.title
    end
  end

  scenario 'enters #index page and search for inexistent tag' do
    visit tags_path
    fill_in 'tag', with: 'AAAAAAAAAAAAAAAAAAAAAAA'
    find('#tag').native.send_key(:enter)

    within '.tagsSection--textsContainer' do
      expect(page).to have_content I18n.t(:no_texts_with_tags)
    end
  end
end