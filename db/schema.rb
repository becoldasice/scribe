# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150115010828) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bookshelf_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bookshelves", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "bookshelf_type_id"
    t.integer  "user_id"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.text     "about"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "locale_key"
  end

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "impressions", force: true do |t|
    t.string   "impressionable_type"
    t.integer  "impressionable_id"
    t.integer  "user_id"
    t.string   "controller_name"
    t.string   "action_name"
    t.string   "view_name"
    t.string   "request_hash"
    t.string   "ip_address"
    t.string   "session_hash"
    t.text     "message"
    t.text     "referrer"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "impressions", ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index", using: :btree
  add_index "impressions", ["user_id"], name: "index_impressions_on_user_id", using: :btree

  create_table "inkwell_blog_items", force: true do |t|
    t.integer  "item_id"
    t.boolean  "is_reblog"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "owner_id"
    t.string   "item_type"
    t.string   "owner_type"
  end

  create_table "inkwell_comments", force: true do |t|
    t.integer  "user_id"
    t.text     "body"
    t.integer  "parent_id"
    t.integer  "commentable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "commentable_type"
    t.integer  "section_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.boolean  "deleted",          default: false
  end

  add_index "inkwell_comments", ["commentable_id", "commentable_type"], name: "index_inkwell_comments_on_commentable_id_and_commentable_type", using: :btree
  add_index "inkwell_comments", ["lft"], name: "index_inkwell_comments_on_lft", using: :btree
  add_index "inkwell_comments", ["parent_id"], name: "index_inkwell_comments_on_parent_id", using: :btree
  add_index "inkwell_comments", ["rgt"], name: "index_inkwell_comments_on_rgt", using: :btree
  add_index "inkwell_comments", ["user_id"], name: "index_inkwell_comments_on_user_id", using: :btree

  create_table "inkwell_favorite_items", force: true do |t|
    t.integer  "item_id"
    t.integer  "owner_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "item_type"
    t.string   "owner_type"
  end

  create_table "inkwell_followings", force: true do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inkwell_timeline_items", force: true do |t|
    t.integer  "item_id"
    t.integer  "owner_id"
    t.text     "from_source",      default: "[]"
    t.boolean  "has_many_sources", default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "item_type"
    t.string   "owner_type"
  end

  create_table "languages", force: true do |t|
    t.string   "name"
    t.string   "locale"
    t.text     "flag"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "master_scribers", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "master_scribers", ["email"], name: "index_master_scribers_on_email", unique: true, using: :btree
  add_index "master_scribers", ["reset_password_token"], name: "index_master_scribers_on_reset_password_token", unique: true, using: :btree

  create_table "notifications", force: true do |t|
    t.integer  "user_id"
    t.boolean  "seen",       default: false
    t.text     "entities"
    t.integer  "target"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "type"
  end

  create_table "postponed_texts", force: true do |t|
    t.integer  "user_id"
    t.integer  "text_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "recommendations", force: true do |t|
    t.integer  "text_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subscribers", force: true do |t|
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "locale"
  end

  create_table "tags", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "texts_count", default: 0
  end

  create_table "tags_texts", id: false, force: true do |t|
    t.integer "text_id", null: false
    t.integer "tag_id",  null: false
  end

  add_index "tags_texts", ["tag_id", "text_id"], name: "index_tags_texts_on_tag_id_and_text_id", using: :btree
  add_index "tags_texts", ["text_id", "tag_id"], name: "index_tags_texts_on_text_id_and_tag_id", using: :btree

  create_table "text_images", force: true do |t|
    t.integer  "text_id"
    t.string   "photo"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.boolean  "is_cover"
  end

  create_table "texts", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.text     "images"
    t.integer  "user_id"
    t.integer  "category_id"
    t.integer  "language_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "users_ids_who_favorite_it", default: "[]"
    t.text     "users_ids_who_comment_it",  default: "[]"
    t.text     "users_ids_who_reblog_it",   default: "[]"
    t.boolean  "is_draft",                  default: true
    t.string   "subtitle"
    t.string   "slug"
    t.datetime "published_at"
    t.integer  "recommendations_count",     default: 0
    t.integer  "unique_views_counter",      default: 0,    null: false
  end

  create_table "user_preferences", force: true do |t|
    t.boolean  "email_on_follow",          default: true
    t.boolean  "email_on_recommendations", default: true
    t.boolean  "email_top_ten",            default: true
    t.boolean  "email_weekly_writers",     default: true
    t.boolean  "email_week_trends",        default: true
    t.boolean  "email_steve_suggestions",  default: true
    t.boolean  "get_whatsapp_texts",       default: true
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "gender"
    t.text     "bio"
    t.string   "website"
    t.string   "prefered_language"
    t.text     "favorite_categories"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "follower_count",               default: 0
    t.integer  "following_count",              default: 0
    t.string   "profile_photo_file_name"
    t.string   "profile_photo_content_type"
    t.integer  "profile_photo_file_size"
    t.datetime "profile_photo_updated_at"
    t.string   "cover_photo_file_name"
    t.string   "cover_photo_content_type"
    t.integer  "cover_photo_file_size"
    t.datetime "cover_photo_updated_at"
    t.boolean  "confirmed_user",               default: false
    t.string   "confirmation_token"
    t.string   "direct_upload_url"
    t.boolean  "processed",                    default: false
    t.string   "slug"
    t.string   "password_reset_token"
    t.boolean  "public_email",                 default: false
    t.boolean  "is_active",                    default: true
    t.datetime "last_login"
    t.integer  "notifications_count",          default: 0
    t.integer  "recommendations_count",        default: 0
    t.string   "twitter_uid"
    t.string   "facebook_uid"
    t.string   "twitter_user"
    t.string   "facebook_profile_url"
    t.boolean  "done_facebook_follow_friends", default: false
    t.string   "current_oaccess_token"
    t.boolean  "done_update",                  default: false
    t.string   "facebook_name"
    t.string   "twitter_picture"
    t.string   "instagram_uid"
    t.string   "instagram_user"
    t.string   "instagram_picture"
    t.integer  "texts_count",                  default: 0
    t.string   "whatsapp_number"
    t.string   "country_code"
    t.string   "phone_country_code"
  end

end
