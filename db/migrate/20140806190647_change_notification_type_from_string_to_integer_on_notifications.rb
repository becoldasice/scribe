class ChangeNotificationTypeFromStringToIntegerOnNotifications < ActiveRecord::Migration
  def change
  	remove_column :notifications, :type
  	add_column :notifications, :type, :integer
  end
end
