class AddDirectUploadUrlToUsers < ActiveRecord::Migration
  def change
    add_column :users, :direct_upload_url, :string
  end
end
