class AddIsCoverToTextImages < ActiveRecord::Migration
  def change
    add_column :text_images, :is_cover, :boolean
  end
end
