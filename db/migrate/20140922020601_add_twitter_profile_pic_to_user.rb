class AddTwitterProfilePicToUser < ActiveRecord::Migration
  def change
    add_column :users, :twitter_profile_pic, :string
  end
end
