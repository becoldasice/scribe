class AddWhatsappNumberToUsers < ActiveRecord::Migration
  def change
    add_column :users, :whatsapp_number, :string
  end
end
