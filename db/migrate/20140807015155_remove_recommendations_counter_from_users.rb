class RemoveRecommendationsCounterFromUsers < ActiveRecord::Migration
  def change
    #remove_column :users, :recommendations_count, :integer
    rename_column :users, :notification_count, :notifications_count
  end
end
