class RemoveSubTitleFromTexts < ActiveRecord::Migration
  def change
    remove_column :texts, :sub_title, :string
  end
end
