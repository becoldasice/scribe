class ChangeTwitterProfilePicToTwitterPicture < ActiveRecord::Migration
  def change
    rename_column :users, :twitter_profile_pic, :twitter_picture
  end
end
