class CreateUserPreferences < ActiveRecord::Migration
  def change
    create_table :user_preferences do |t|
      t.boolean :email_on_follow
      t.boolean :email_on_recommendations
      t.boolean :email_top_ten
      t.boolean :email_weekly_writers
      t.boolean :email_week_trends
      t.boolean :email_steve_suggestions
      t.boolean :get_whatsapp_texts
      t.integer :user_id

      t.timestamps
    end
  end
end
