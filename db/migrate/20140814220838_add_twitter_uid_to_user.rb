class AddTwitterUidToUser < ActiveRecord::Migration
  def change
    add_column :users, :twitter_uid, :integer, default: nil
  end
end
