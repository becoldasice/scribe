class ChangeFacebookUidFromIntegerToString < ActiveRecord::Migration
  def change
    change_column :users, :facebook_uid, :string
  end
end
