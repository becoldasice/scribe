class AddDoneUpdateToUsers < ActiveRecord::Migration
  def change
    add_column :users, :done_update, :boolean, default: :false
  end
end
