class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :password_hash
      t.string :password_salt
      t.string :first_name
      t.string :last_name
      t.string :country
      t.string :gender
      t.text :bio
      t.string :website
      t.text :favorite_languages
      t.text :favorite_categories
      t.string :profile_photo
      t.string :cover_photo

      t.timestamps
    end
  end
end
