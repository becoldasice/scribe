class CreateTextImages < ActiveRecord::Migration
  def change
    create_table :text_images do |t|
      t.integer :text_id
      t.string :photo

      t.timestamps
    end
  end
end
