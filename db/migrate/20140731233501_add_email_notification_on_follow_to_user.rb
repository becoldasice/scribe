class AddEmailNotificationOnFollowToUser < ActiveRecord::Migration
  def change
    add_column :users, :email_notification_on_follow, :boolean
  end
end
