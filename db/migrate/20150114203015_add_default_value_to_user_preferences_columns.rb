class AddDefaultValueToUserPreferencesColumns < ActiveRecord::Migration
  def change
    change_column :user_preferences, :email_on_follow, :boolean, default: true
    change_column :user_preferences, :email_on_recommendations, :boolean, default: true
    change_column :user_preferences, :email_top_ten, :boolean, default: true
    change_column :user_preferences, :email_weekly_writers, :boolean, default: true
    change_column :user_preferences, :email_week_trends, :boolean, default: true
    change_column :user_preferences, :email_steve_suggestions, :boolean, default: true
    change_column :user_preferences, :get_whatsapp_texts, :boolean, default: true

    User.all.find_each do |us|
      us.create_preferences!
    end
  end
end
