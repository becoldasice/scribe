class CreateTexts < ActiveRecord::Migration
  def change
    create_table :texts do |t|
      t.string :title
      t.text :description
      t.text :images
      t.integer :user_id
      t.integer :category_id
      t.integer :language_id

      t.timestamps
    end
  end
end
