class AddTextsCountToUsers < ActiveRecord::Migration
  def change
    add_column :users, :texts_count, :integer, default: 0

    User.all.find_each do |u|
      User.update_counters(u.id, {
        texts_count: u.published_texts.count
      })
    end
  end
end
