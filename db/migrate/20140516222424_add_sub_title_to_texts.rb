class AddSubTitleToTexts < ActiveRecord::Migration
  def change
    add_column :texts, :sub_title, :string
  end
end
