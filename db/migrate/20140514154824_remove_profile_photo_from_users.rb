class RemoveProfilePhotoFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :profile_photo, :string
  end
end
