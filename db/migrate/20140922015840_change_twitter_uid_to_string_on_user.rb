class ChangeTwitterUidToStringOnUser < ActiveRecord::Migration
  def change
  	change_column :users, :twitter_uid, :string
  end
end
