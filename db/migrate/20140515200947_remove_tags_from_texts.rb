class RemoveTagsFromTexts < ActiveRecord::Migration
  def change
    remove_column :texts, :tags, :text
  end
end
