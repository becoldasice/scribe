class ChangeFbNameToFacebookName < ActiveRecord::Migration
  def change
    rename_column :users, :fb_name, :facebook_name
  end
end
