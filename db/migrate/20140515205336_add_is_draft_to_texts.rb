class AddIsDraftToTexts < ActiveRecord::Migration
  def change
    add_column :texts, :is_draft, :boolean, default: true
  end
end
