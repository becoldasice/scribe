class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :type
      t.integer :user_id
      t.boolean :seen, default: false
      t.text :entities
      t.integer :target

      t.timestamps
    end
  end
end
