class RemoveEmailNotificationsFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :email_notifications, :text
    remove_column :users, :email_notification_on_follow, :boolean
  end
end
