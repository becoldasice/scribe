class AddLocaleKeyToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :locale_key, :string
  end
end
