class AddDoneFacebookFollowFriendsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :done_facebook_follow_friends, :boolean, default: false
  end
end
