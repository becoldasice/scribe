class AddTextsCountToTags < ActiveRecord::Migration
  def change
    add_column :tags, :texts_count, :integer, default: 0
  end
end
