class AddInstagramPictureToUser < ActiveRecord::Migration
  def change
    add_column :users, :instagram_picture, :string
  end
end
