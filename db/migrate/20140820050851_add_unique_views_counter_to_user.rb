class AddUniqueViewsCounterToUser < ActiveRecord::Migration
  def change
    add_column :users, :unique_views_counter, :integer
  end
end
