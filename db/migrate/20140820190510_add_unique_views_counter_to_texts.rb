class AddUniqueViewsCounterToTexts < ActiveRecord::Migration
  def change
    add_column :texts, :unique_views_counter, :integer, null: false, default: 0

    ids = Set.new
    Text.find_each {
    	|t|
    	Text.update_counters(t.id, unique_views_counter: t.impressionist_count(filter: :ip_address))
    }
  end
end