class TextsTableMigrations < ActiveRecord::Migration
  def change
  	change_table :texts do |t|
	  t.integer :recommendations_count, default: 0
	end
  end
end
