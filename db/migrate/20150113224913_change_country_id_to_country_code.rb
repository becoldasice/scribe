class ChangeCountryIdToCountryCode < ActiveRecord::Migration
  def change
    add_column :users, :country_code, :string
    User.all.find_each do |us|
      country = if us.prefered_language.eql?("pt-BR") || us.prefered_language.blank?
        Country[Country.find_by_name('Brazil').first]
      elsif us.prefered_language.eql?("en")
        Country[Country.find_by_name('United States').first]
      else
        Country[Country.find_by_name('Spain').first]
      end

      us.update({
        country_code: country.alpha2
      })
    end
    remove_column :users, :country_id, :integer
  end
end
