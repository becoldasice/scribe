class CreateTextsTagsJoinTable < ActiveRecord::Migration
  def change
    create_join_table :texts, :tags do |t|
      t.index [:text_id, :tag_id]
      t.index [:tag_id, :text_id]
    end
  end
end
