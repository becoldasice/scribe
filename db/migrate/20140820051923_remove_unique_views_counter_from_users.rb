class RemoveUniqueViewsCounterFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :unique_views_counter, :integer
  end
end
