class AddSlugToTexts < ActiveRecord::Migration
  def change
    add_column :texts, :slug, :string
  end
end
