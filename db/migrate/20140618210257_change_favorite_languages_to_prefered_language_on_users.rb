class ChangeFavoriteLanguagesToPreferedLanguageOnUsers < ActiveRecord::Migration
  
  def change
  	rename_column :users, :favorite_languages, :prefered_language
  	change_column :users, :prefered_language, :string
  end

end
