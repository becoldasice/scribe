class AddPublishedAtToText < ActiveRecord::Migration
  def change
    add_column :texts, :published_at, :datetime
  end
end
