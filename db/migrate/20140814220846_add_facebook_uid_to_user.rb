class AddFacebookUidToUser < ActiveRecord::Migration
  def change
    add_column :users, :facebook_uid, :integer, default: nil
  end
end
