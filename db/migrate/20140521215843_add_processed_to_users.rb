class AddProcessedToUsers < ActiveRecord::Migration
  def change
    add_column :users, :processed, :boolean, default: false
  end
end
