class AddDeletedToInkwellComments < ActiveRecord::Migration
  def change
    add_column :inkwell_comments, :deleted, :boolean, default: :false
  end
end
