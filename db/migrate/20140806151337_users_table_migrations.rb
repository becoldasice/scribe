class UsersTableMigrations < ActiveRecord::Migration
  
  def change
  	change_table :users do |t|
	  t.integer :notification_count, :recommendations_count, default: 0
	  t.text :email_notifications, default: {follow: true, new_tl_text: true, weekly_digest: true, recommended_text: true, new_comment_on_text: true}.to_yaml
	end
  end

end
