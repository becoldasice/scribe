class ChangeDefaultForEmailNotificationOnFollow < ActiveRecord::Migration
  
  def change
  	change_table :users do |t|
	  t.change :email_notification_on_follow, :boolean, {default: true}
	end
  end
end