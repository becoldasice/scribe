class AddSectionIdToComments < ActiveRecord::Migration
  def change
    add_column :inkwell_comments, :section_id, :integer
  end
end
