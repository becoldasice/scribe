class RemoveProfileFromUsers < ActiveRecord::Migration
  def change
    remove_attachment :users, :profile
  end
end
