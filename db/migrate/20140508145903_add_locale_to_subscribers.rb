class AddLocaleToSubscribers < ActiveRecord::Migration
  def change
    add_column :subscribers, :locale, :string
  end
end
