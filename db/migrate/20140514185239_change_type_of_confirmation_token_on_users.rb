class ChangeTypeOfConfirmationTokenOnUsers < ActiveRecord::Migration
  def change
  	change_column :users, :confirmation_token, :string, default: nil
  end
end
