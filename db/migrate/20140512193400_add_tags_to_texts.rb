class AddTagsToTexts < ActiveRecord::Migration
  def change
    add_column :texts, :tags, :text
  end
end
