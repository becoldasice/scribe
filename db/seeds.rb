unless Rails.env.eql?("test")
  # This file should contain all the record creation needed to seed the database with its default values.
  # The data can then be loaded with the rake db:seed (or find_or_create_byd alongside the db with db:setup).
  #
  # Examples:
  #
  #   cities = City.find_or_create_by([{ name: 'Chicago' }, { name: 'Copenhagen' }])
  #   Mayor.find_or_create_by(name: 'Emanuel', city: cities.first)
  cat1 = Category.find_or_create_by(name: "Poemas e Poesias")
  cat1.update({about: "É uma das formas de arte mais antigas. É uma obra literária escrita em verso ou prosa, que destaca a beleza dos sentimentos por meio de palavras,  usando a “estética” da linguagem.", locale_key: "poems_and_poetries"})
  cat2 = Category.find_or_create_by(name: "Contos")
  cat2.update({about: "É uma narrativa ficcional, que pode ser totalmente criado pelo autor, ou pode ser baseado em fatos reais. Como todos os textos de ficção, o conto apresenta um narrador, personagens, ponto de vista e enredo.", locale_key: "short_stories"})
  cat3 = Category.find_or_create_by(name: "Crônicas e Opiniões")
  cat3.update({about: "É um ensaio argumentativo sobre um determinado tema, e demonstra expressando opinião do escritor.", locale_key: "articles"})

  Language.find_or_create_by({name: "English", locale: "en", flag: "en_flag.png"})
  Language.find_or_create_by({name: "Español", locale: "es", flag: "es_flag.png"})
  Language.find_or_create_by({name: "Portugues (Brasil)", locale: "pt-BR", flag: "pt-br_flag.png"})

  scribe = User.create({
    email: 'scribe@wescribe.co',
    email_confirmation: 'scribe@wescribe.co',
    first_name: 'Steve',
    last_name: 'Scriber',
    password: '123456',
    password_confirmation: '123456',
    country_code: Country['BR'],
    phone_country_code: Country['BR'].country_code,
    whatsapp_number: '21981266510',
    slug: 'scribe'
  })

  scribe.texts << Text.create({
    title: 'Scribe',
    description: '<p class="text-element">Em um ambiente virtual cheio de interações
      e ruídos, é cada vez mais difícil para os escritores conseguirem conquistar e
      cativar o seu público. Da mesma forma, os leitores lutam em busca de novidade
      e variedade: novos autores, estilos, formatos. </p><p class="text-element"><br
      class="text-element"></p><blockquote class="text-element">Como resultado deste
      ambiente caótico, os escritores foram forçados a assumir uma segunda profissão:
      Autopromotores.</blockquote><blockquote class="text-element"><br class="text-element
      parent-blockquote"></blockquote><p class="text-element">Foi pensando neste contexto
      que criamos o <b class="text-element">Scribe</b>, um o lugar onde os escritores
      vão desfrutar da liberdade de serem eles mesmos, sentindo-se vivos. É um lugar
      onde você se libertará de todas as algemas modernas: Sistemas de pagamento, configurações
      de editores de texto, ou mesmo a obrigação de encontrar leitores. Scribe elimina
      a distância entre escritores e leitores, permitindo a troca de experiências, perspectivas
      e interações.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b
      class="text-element">Cada leitor uma preferência.</b></p><p class="text-element"><br
      class="text-element"></p><p class="text-element">São mais de 130 milhões de blogs
      na internet, mas constantemente nos sentimos sem nada para ler. Achar um novo
      autor ou apenas um bom texto parece cada vez mais difícil. </p><p class="text-element"><br
      class="text-element"></p><p class="text-element">Mas, e se colocássemos em um
      mesmo lugar diversos autores, com seus textos separados por categorias, listados
      por temas, além de te dar a possibilidade avaliar todo o conteúdo, seguir os novos
      autores que encontrar e trocar mensagens com amigos e pessoas com o mesmo interesse
      que você?</p><p class="text-element"><br class="text-element"></p><p class="text-element">Assim
      é o Scribe, um lugar aonde você não irá mais sofrer para achar bons textos e poderá
      compartilhar tudo com seus amigos.</p><p class="text-element"><br class="text-element"></p><p
      class="text-element"><b class="text-element">Scribe é sobre ler e escrever. O
      Scribe é SOCIAL</b>.</p><p class="text-element"><br class="text-element"></p><p
      class="text-element">Oferecemos um editor web-based, simples e clean, que não
      requer conhecimento de programação ou horas de configurações complexas, permitindo
      que você se concentre no mais importante, escrever.  </p><p class="text-element"><br
      class="text-element"></p><p class="text-element">Os <b class="text-element">leitores</b>
      poderão encontrar facilmente novos escritores e textos, seja por meio de nossas
      categorias pré-determinadas, tags, Idiomas, melhores avaliados ou recém-adicionados.</p><p
      class="text-element"><br class="text-element"></p><p class="text-element">No <b
      class="text-element">Scribe</b> os usuários poderão interagir de inúmeras formas.
      Você seguirá seus escritores favoritos, trocará mensagens, deixará comentários
      e avaliará seus textos. </p><p class="text-element"><br class="text-element"></p><p
      class="text-element">Já para os <b class="text-element">escritores</b> será o
      ambiente ideal para construir sua audiência, conhecer seus seguidores, receber
      feedbacks e melhorar suas habilidades de escrita.</p><p class="text-element"><br
      class="text-element"></p>',
    category_id: 3,
    language_id: 3,
    is_draft: false,
    subtitle: 'Cada escritor, um público. Conte a sua história e desfrute a liberdade de ser você.',
    slug: 'scribe'
  })
  
  scribe.texts << Text.create({
    title: 'Scribe',
    description: '<p class="text-element">Today, it is increasingly difficult to build
      and captivate writers and readers. Cluttered, distracting websites overwhelm rather
      than inspire. As a result, talented writers are forced to take on the daunting
      task of self-marketing. Similarly, readers struggle to find new authors and personalize
      their reading experience.  </p><p class="text-element"><br class="text-element"></p><blockquote
      class="text-element">With this in mind, we created Scribe. </blockquote><p class="text-element"><br
      class="text-element"></p><p class="text-element"><b class="text-element">Scribe
      </b>is THE social network for writers and readers. Scribe provides a simple web-based
      editor that doesn’t require fancy coding knowledge or hours of complex configuration. </p><p
      class="text-element"><br class="text-element"></p><blockquote class="text-element">Scribe
      is SOCIAL</blockquote><p class="text-element"><br class="text-element"></p><p
      class="text-element"><b class="text-element">Scribe</b> bridge the gap between
      writer and reader. Writers focus their attention on writing; readers easily find
      the texts they want to read. </p><p class="text-element"><br class="text-element"></p><p
      class="text-element">Scribe users interact with writers and readers in multiple
      ways:</p><p class="text-element">1. Follow your favorite writers. </p><p class="text-element">2.
      Share texts with friends on Facebook or Twitter. </p><p class="text-element">3.
      Leave comments. </p><p class="text-element">4. Use Tags to find preferred texts.</p><p
      class="text-element"><br class="text-element"></p><blockquote class="text-element">Scribe
      is SIMPLE.</blockquote><p class="text-element"><br class="text-element"></p><p
      class="text-element">Unlike other writing platforms, Scribe uses a web-based editor
      that doesn’t require knowledge of design or coding to personalize their reading
      and writing experience. </p><p class="text-element"><br class="text-element"></p><p
      class="text-element"><b class="text-element">Join Scribe today!</b> </p>',
    category_id: 3,
    language_id: 1,
    is_draft: false,
    subtitle: 'All Writers and Readers Welcome.',
    slug: 'scribe-613'
  })

  scribe.texts << Text.create({
    title: 'Scribe - Boas práticas',
    description: '<p class="text-element">Hey Scribers, tudo bem?</p>
      <p class="text-element"> </p>
      <p class="text-element">Eu, Steve Scriber, vou te ajudar a aproveitar da melhor maneira a experiência que oferecemos através de nossa plataforma.</p>
      <p class="text-element">Os tópicos a seguir são algumas dicas para que você consiga aproveitar cada recurso oferecido, qualquer dúvida é só deixar um comentário no texto e/ou enviar um e-mail para scribe@wescribe.co</p>
      <p class="text-element"> </p>
      <p class="text-element"><strong class="text-element">1 – Explore os textos</strong></p>
      <p class="text-element">O Scribe é um ótimo ambiente para leitura e descoberta de novos talentos. Dividimos a nossa plataforma para que você possa navegar por meio de <strong class="text-element">três categorias, são elas: contos, poemas e crônicas/opiniões.</strong></p>
      <p class="text-element">Além desta divisão, você também pode explorar os textos por meio as abas Descubra, Populares e Tags.</p>
      <p class="text-element">Na aba descubra você encontrará textos de todas as categorias listados por nosso algoritmo. Esta aba dá a chance de todos os escritores/textos da plataforma serem encontrados e é similar ao antigo botão "estou com sorte" do google. </p>
      <p class="text-element">Já na aba populares, você encontrará os textos mais recomendados da plataforma. </p>
      <p class="text-element">Por último, na aba tags você poderá explorar e buscar textos usando as tags. Isso permite que você filtre textos pelos temas que eles abordam, independente de sua categoria. </p>
      <p class="text-element"> </p>
      <p class="text-element"><strong class="text-element">2 – Siga os escritores</strong></p>
      <p class="text-element">Após explorar os textos você poderá seguir os escritores que você descobriu enquanto navegava. Essa função é a mais importante da plataforma, pois seguindo um usuário você terá acesso a todos os textos que ele publicar diretamente em sua timeline. Você nunca perderá aqueles artigos, crônicas, contos e poemas incríveis. </p>
      <p class="text-element"> </p>
      <p class="text-element"><strong class="text-element">3 – Compartilhe os textos em outras redes sociais</strong></p>
      <p class="text-element">Você já explorou um pouco a plataforma, descobriu um novo escritor e já está seguindo-o, agora chegou a vez de você compartilhar os textos que você mais gostou com seus amigos. Assim você estreita seus laços com a nossa comunidade e ajuda os escritores a ganharem mais fãs, e quem sabe se tornarem escritores famosos. </p>
      <p class="text-element"> </p>
      <p class="text-element"><strong class="text-element">4 – Escreva e use os recursos de nosso editor de texto</strong></p>
      <p class="text-element">Potencialmente somos todos leitores - isso é algo que ninguém dúvida- mas a ideia do Scribe é mostrar que também somos todos escritores.</p>
      <p class="text-element">Para dar um visual mais profissional a suas palavras,  oferecemos um editor de texto com alguns recursos simples e fáceis de usar. São eles:</p>
      <p class="text-element"> </p>
      <p class="text-element"><strong class="text-element">a) Negrito</strong></p>
      <p class="text-element"><em class="text-element">b) Itálico</em></p>
      <blockquote class="text-element">c) Blockquote</blockquote>
      <p class="text-element"> </p>
      <p class="text-element">Use e explore estes recursos em seus textos. Por meio deles você poderá dar formas mais atrativas às suas palavras, além de facilitar a leitura para seus fãs.</p>
      <p class="text-element"> </p>
      <p class="text-element"><strong class="text-element">5 – Use fotos de capa</strong></p>
      <p class="text-element">Uma boa foto de capa pode levar o seu texto a ter até 3 vezes mais visualizações. Então use e abuse desse recurso, assim suas palavras vão alcançar ainda mais leitores.</p>
      <p class="text-element"> </p>
      <p class="text-element"><strong class="text-element">6 – Saiba ler as estatísticas de seus textos</strong></p>
      <p class="text-element">Outra ferramenta importante fornecida pelo Scribe é a estatística dos seus textos. Com ela é possível você analisar como seus leitores estão reagindo aos seus textos e como chegaram até ele. Fornecemos aos nossos usuários os seguintes números:</p>
      <p class="text-element"> </p>
      <p class="text-element">a) Visualizações – é o total de vezes que um texto foi acessado. Se um leitor acessar duas vezes um texto, uma segunda visualização é registrada.</p>
      <p class="text-element"> </p>
      <p class="text-element">b) Visualizações únicas - reúne todas as visualizações de um usuário durante a mesma sessão e considera só uma visualização por usuário.</p>
      <p class="text-element"> </p>
      <p class="text-element">c) Referentes - Indica como os leitores chegaram ao seu texto, seja por meio do seu perfil no Scribe, timeline, ou por outras redes como facebook e twitter. </p>
      <p class="text-element"> </p>
      <p class="text-element"><strong class="text-element">7 – Complete seu perfil</strong></p>
      <p class="text-element">O seu perfil é seu cartão de boas vindas aos seus fãs.</p>
      <p class="text-element">É muito importante manter seu perfil atualizado com suas informações, história, fotos e redes sociais (twitter, instagram e facebook). Dessa forma os leitores podem taguear os escritores quando compartilham seus textos, o que estreita os laços com o seu público.</p>
      <p class="text-element"> </p>
      <p class="text-element"><strong class="text-element">8 – Use as tags</strong></p>
      <p class="text-element">As tags de seu texto devem conter as palavras-chave/temas do que será abordado. Assim quando um usuário usar a aba de busca por tags, o nosso sistema conseguirá localizar mais facilmente seu texto, aumentando o alcance das suas palavras.</p>
      <p class="text-element"> </p>
      <p class="text-element"><strong class="text-element">9- Interaja, recomende e comente.</strong></p>
      <p class="text-element">A cada dia que passa nós oferecemos a vocês mais possibilidades de interação. Agora o Scribe conta com o botão recomendar (o curtir de nossa plataforma) e também a possibilidade de comentar a cada parágrafo do texto (botão "+"). </p>
      <p class="text-element">Então se você gostou de um texto, recomende e deixe seu comentário. Se alguém comentou em seu texto, responda e comece a criar um vínculo com seus fãs. </p>
      <p class="text-element"> </p>
      <p class="text-element"><strong class="text-element">10 - Importe gradualmente seus textos antigos para o Scribe</strong></p>
      <p class="text-element">O Scribe é uma ótima forma de você reunir todo o conteúdo já publicado por você. Faça isso de forma gradual, para que seus fãs leiam pouco a pouco seus textos usando a nossa timeline. Se você possuir 20 textos antigos, é melhor que publique 1 por dia, por 20 dias, ao invés de todos em um dia só. </p>
      <p class="text-element"> </p>
      <p class="text-element">Scribers, essa são algumas de nossas dicas para que você consiga aproveitar ao máximo a nossa plataforma. Se tiver alguma dúvida e feedback mande e-mail para feedback@wescribe.co e responderemos o mais rápido possível. </p>',
    category_id: 3,
    language_id: 3,
    is_draft: false,
    subtitle: 'Como aproveitar ao máximo os recursos oferecidos',
    slug: 'scribe-boas-praticas'
  })
  
  scribe.texts << Text.create({
    title: 'Scribe Privacy Policy',
    description: '<b class="text-element">General information<br class="text-element"></b><p
      class="text-element">This policy sets out our privacy practices and explains how
      we handle the information we collect when you visit and use our sites, services,
      mobile applications, products, and content provided by Scribe, in existence now
      or in the future (“Scribe Services”). </p><p class="text-element"><br class="text-element"></p><blockquote
      class="text-element">Please Read it carefully.</blockquote><p class="text-element"><b
      class="text-element">What we may collect</b></p><p class="text-element">We collect
      information about what Scribe pages you access or visit, information about your
      mobile device (such as device or browser type), information volunteered by you
      (such as through registration), the URLs of websites that referred you to us,
      and e-mail addresses of those who communicate with us via email.</p><p class="text-element"><br
      class="text-element"></p><p class="text-element">When you log into Scribe Services
      or load a web page from Scribe Services, we collect and store your Internet Protocol
      address. We may use this information to fight spam, malware, and identity theft;
      to personalize Scribe Services for you; or to generate aggregate, non-identifying
      information about how people use Scribe Services.</p><p class="text-element"><br
      class="text-element"></p><p class="text-element">When you create your Scribe account,
      and authenticate via a third-party service like Twitter, we may collect, store,
      and periodically update the contact lists associated with that third-party account,
      so that we can make it easy for you to connect with your existing contacts from
      that service who are also on Scribe.</p><p class="text-element"><br class="text-element"></p><p
      class="text-element"><b class="text-element">Email communications with us</b></p><p
      class="text-element">We may occasionally need to email you some administrative
      information, tell you something important about your account or changes to our
      services, or update you on new policies. In really tough situations, like when
      we got dumped that one time, we might just need to vent about how unfair life
      is. Except for that last scenario, which will not actually happen, these administrative
      communications are considered a basic part of Scribe Services, and you may not
      be able to opt out from receiving them. You can always opt out of non-administrative
      emails.<br class="text-element"></p><p class="text-element"><br class="text-element"></p><p
      class="text-element"><i class="text-element">We will never email you to ask for
      your password or other account information. If you receive such an email, send
      it immediately to us, so we can take action against the evildoer.</i></p><p class="text-element"><i
      class="text-element"><br class="text-element"></i></p><p class="text-element"><b
      class="text-element">Disclosure of your information</b></p><p class="text-element">The
      information we collect is used to provide and improve Scribe Services and content
      and prevent abuse. We don’t sell personal information about our users to any third
      party.</p><p class="text-element"><br class="text-element"></p><p class="text-element">While
      Scribe endeavors to provide the highest level of protection for your information,
      we may share your personal information with third parties in limited circumstances,
      including but not limited to: (1) when your consent is given; or (2) when we have
      a good faith belief it is required by law, such as pursuant to a subpoena or other
      judicial or administrative order.</p><p class="text-element"><br class="text-element"></p><p
      class="text-element">If we’re going to release your information, our policy is
      to provide you with advance notice unless we are prohibited from doing so by law
      or court order (e.g., under laws such as 18 U.S.C. § 2705(b), also known as gag
      orders). If you do not challenge the disclosure request, we may be legally required
      to turn over your information.</p><p class="text-element">We may disclose your
      information without providing you with prior notice if we believe it’s necessary
      to prevent imminent and serious bodily harm to a person. In that case, we will
      endeavor to provide you with post-disclosure notice when permitted by law.</p><p
      class="text-element"><br class="text-element"></p><p class="text-element">We will
      independently object to requests for access to information about users of our
      site that we believe to be improper.</p><p class="text-element">If we’re acquired
      by or merged with another company and your information becomes subject to a different
      privacy policy, we’ll notify you before the transfer. You can opt out of the new
      policy by deleting your account during the notice period.</p><p class="text-element"><br
      class="text-element"></p><p class="text-element"><b class="text-element">Public
      user content</b></p><p class="text-element">Scribe is meant for publishing public,
      not private, content. By default, whatever you share through Scribe Services is
      public. Although we do provide tools that let you write and edit draft content
      prior to publication, you should assume that any content you provide us may become
      publicly accessible.<br class="text-element"></p><p class="text-element"><br class="text-element"></p><p
      class="text-element">Content published and shared through Scribe Services is publicly
      accessible, which means that everyone, including search engines, will be able
      to see it. This content may also be copied and shared by others throughout the
      Internet, including through features native to Scribe Services, such as commenting
      and embedding.</p><p class="text-element">You are free to remove published content
      from your account, or even disable your account entirely. However, because of
      the fundamentally open nature of the Internet, the strong possibility that others
      will comment on or embed your content, and technological limitations inherent
      to Scribe Services, copies of your content may exist elsewhere indefinitely, including
      in our systems.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b
      class="text-element">Cookies</b></p><p class="text-element">A cookie is a small
      piece of text, which often includes an anonymous unique identifier, sent to and
      saved by your web browser when you access a website.<br class="text-element"></p><p
      class="text-element">We use cookies to enable our servers to recognize your web
      browser and tell us how and when you use Scribe Services. We use cookies to identify
      whether you have logged in and recognize that your web browser has accessed aspects
      of Scribe </p><p class="text-element">Services, and we may associate that information
      with your account if you have one. This information, in turn, is sometimes used
      to personalize your experiences on Scribe Services when you are logged in. To
      measure the deliverability of our emails to users, we may embed information in
      them, such as a web beacon or tag.<br class="text-element"></p><p class="text-element">Most
      browsers have an option for disabling cookies, but if you disable them you won’t
      be able to log into your Scribe account, and won’t be able to use the vast majority
      of Scribe Services.</p><p class="text-element"><br class="text-element"></p><p
      class="text-element">We respect Do Not Track (“DNT”) settings in browsers. If
      you are logged out of our services and have DNT enabled, we will not set cookies
      that can be used to aggregate information about your usage.</p><p class="text-element"><br
      class="text-element"></p><p class="text-element"><b class="text-element">Data
      Storage</b></p><p class="text-element">Scribe uses third-party vendors and hosting
      partners, such as Amazon, to provide the necessary hardware, software, networking,
      storage, and related technology we need to run Scribe. Although Scribe owns its
      code, databases, and all rights to the Scribe application, <i class="text-element"><b
      class="text-element">you retain all rights to your content.</b></i></p><p class="text-element"><br
      class="text-element"></p><p class="text-element">Server logs: Like most websites,
      our servers automatically record the page request made when you visit our sites.
      We have two types of server logs: nginx and application. These server logs may
      include your web request, IP address, browser type, browser language, the date
      and time of your request, and one or more cookies that may uniquely identify your
      browser. We will delete all server logs after 9 months or earlier.</p><p class="text-element"><br
      class="text-element"></p><p class="text-element">Here is an anonymized example
      of an nginx log entry for a user who views a post:</p><p class="text-element">01.01.01.01 — —
      [10/Apr/2014:18:04:24 +0000]+0.018 “GET /p/xxxxxxxxxxx HTTP/1.1" 410 2467 “http://www.google.com"
      “Mozilla/5.0 (Windows; U; Win 9x 4.90; SG; rv:1.9.2.4) Gecko/20101104 Netscape/9.1.0285"
      “1010101010101:10x10x10x10x” “-” “wescribe.co”</p><p class="text-element">The
      parts are as follows:</p><p class="text-element">• IP address (01.01.01.01)</p><p
      class="text-element">• Timestamp + request time ([10/Apr/2014:18:04:24 +0000]+0.018)</p><p
      class="text-element">• HTTP request, method + path + HTTP version (“GET /p/xxxxxxxxxxx
      HTTP/1.1”)</p><p class="text-element">• HTTP status returned (410)</p><p class="text-element">•
      Response length in bytes (2467)</p><p class="text-element">• Referrer (http://www.google.com)</p><p
      class="text-element">• User agent (Mozilla/5.0 (Windows; U; Win 9x 4.90; SG; rv:1.9.2.4)
      Gecko/20101104 Netscape/9.1.0285)</p><p class="text-element">• Internal transaction
      ID (1010101010101:10x10x10x10x)</p><p class="text-element">• Scribe client identifier
      (“-”)</p><p class="text-element">• Host (wescribe.co)</p><p class="text-element"><br
      class="text-element"></p><p class="text-element"><b class="text-element">Children</b></p><p
      class="text-element">Scribe Services are intended for general audiences and are
      not directed to children under 13. We don’t knowingly collect personal information
      from children under 13. If you become aware that a child has provided us with
      personal information, please contact us. If we learn that a child under 13 has
      provided us with personal information, we take steps to remove such information
      and terminate the child’s account.</p><p class="text-element"><br class="text-element"></p><p
      class="text-element"><b class="text-element">Changes to this Policy</b></p><p
      class="text-element">Scribe may periodically update this policy. We will notify
      you about significant changes in the way we treat personal information by sending
      a notice to the primary email address specified in your Scribe account or by placing
      a prominent notice on our site before the changes take effect.</p><p class="text-element">The
      most current version of the policy will always be posted on Scribe official account
      and we will archive former versions of the policy.</p><p class="text-element"><br
      class="text-element"></p><p class="text-element"><b class="text-element">Questions</b></p><p
      class="text-element">We welcome questions, concerns, and feedback about this policy.
      If you have any suggestions for us, feel free to let us know at feedback@wescribe.co</p><p
      class="text-element"><br class="text-element"></p><p class="text-element">See
      you Scribers! </p><p class="text-element"><br class="text-element"></p><p class="text-element"><br
      class="text-element"></p>',
    category_id: 3,
    language_id: 1,
    is_draft: false,
    subtitle: 'Privacy is important. We respect yours.',
    slug: 'scribe-privacy-policy'
  })

  scribe.texts << Text.create({
    title: 'Scribe Rules',
    description: '<p class="text-element">Scribe is designed for many people to share their opinions, thoughts, and ideas. For this to be done in a civilized manner, we have some rules. They are simple and pretty obvious. </p><p class="text-element"><br class="text-element"></p><blockquote class="text-element">As shared space we want many different ideas to thrive, we need to have a few ground rules. </blockquote><p class="text-element"><br class="text-element"></p><p class="text-element"><b class="text-element">Things You Shouldn’t Do</b></p><p class="text-element"><br class="text-element"></p><p class="text-element"><b class="text-element">Threats of Violence</b></p><p class="text-element">Don’t threaten other users with violence.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b class="text-element">Hateful Slurs</b></p><p class="text-element">Recognizing the wide orbit that we ought to give to freedom of expression, our general view is that the best response to hateful speech is more speech, rather than censorship, but we reserve the right to take down hateful slurs.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b class="text-element">Spam</b></p><p class="text-element">Don’t spam. Don’t use deceptive tags or links or otherwise mislead other users about what you’ve posted. Don’t embed malicious code in your posts. Don\'t use deceptive means to generate revenue or traffic, or create posts with the primary purpose of affiliate marketing.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b class="text-element">Mass Registration and Automation</b></p><p class="text-element">Don’t register accounts or post content automatically, systematically, or programmatically.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b class="text-element">Copyright and Trademark Infringement</b></p><p class="text-element">Respect the copyrights and trademarks of others. Unless you are authorized to use someone else\'s copyrighted or trademarked work (either expressly or by legal exceptions and limitations like fair use), don\'t do it. It is our policy to respond to notices of alleged copyright infringement as per our “Scribe Terms of Service” and the Digital Millennium Copyright Act.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b class="text-element">Deceptive Impersonation</b></p><p class="text-element">Don’t impersonate someone else in a deceptive way. Parody and ridicule are fine, but make clear that that’s what you’re doing.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b class="text-element">Porn</b></p><p class="text-element">No porn. There are other places to post that. You know where those websites are.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b class="text-element">Privacy Violations</b></p><p class="text-element">Don\’t use Scribe to fish for personal information in deceptive ways. Don\'t post content that violates others’ privacy, including personally identifying or confidential information like credit card numbers, social security numbers, or non-public contact information.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b class="text-element">Phishing and Other Frauds</b></p><p class="text-element">Don’t use Scribe to engage in phishing or fraud.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b class="text-element">Promotion and Glorification of Self-Harm</b></p><p class="text-element">Don\'t actively promote or glorify self-harm, by which we mean cutting, eating disorders like anorexia or bulimia, and suicide.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b class="text-element">If You Break the Rules</b></p><p class="text-element">
      <i class="text-element">If you violate these policies, you may receive a notice via email. If you don’t explain yourself or fix the problem, your account may be suspended and your IP address may be blocked.</i> </p><p class="text-element">We do our best to act fairly, but we always reserve the right to suspend accounts or remove content, without notice, for any reason, but particularly to protect our services, infrastructure, users, or community. We reserve the right to enforce, or not enforce, these policies at our sole discretion, and these policies don\'t create a duty or contractual obligation for us to act in any particular manner.</p><p class="text-element"><br class="text-element"></p><p class="text-element">See you Scribers! </p>',
    category_id: 3,
    language_id: 1,
    is_draft: false,
    subtitle: '',
    slug: 'scribe-rules'
  })

  scribe.texts << Text.create({
    title: 'Scribe Terms of Service',
    description: '<b class="text-element">Scribe Terms of Service</b><p class="text-element"><i
      class="text-element">It will be as painless and straightforward as possible. We
      guarantee it!</i></p><p class="text-element"><br class="text-element"></p><p class="text-element">This
      is a contract (“Terms”) between you ("Author") and A Scribe Corporation (“Scribe,”
      or "Company”), applicable when you use our sites, services, mobile applications,
      products, and content provided by Scribe, globally, in existence now or in the
      future (“Scribe Services”).</p><p class="text-element"><br class="text-element"></p><p
      class="text-element"><b class="text-element">These terms are a binding agreement</b></p><p
      class="text-element">Even if your eyes normally glaze over the word “Terms,” it’s
      a good idea to read this agreement carefully before you use Scribe. For one thing,
      we want your feedback and suggestions on how to make our service better. (We’d
      love to hear from you: email your input to feedback@wescribe.co .) <b class="text-element">For
      another, by using Scribe Services, you agree to be bound by everything in these
      Terms. If you don’t agree to the Terms, please don’t use Scribe.</b></p><p class="text-element"><br
      class="text-element"></p><p class="text-element"><b class="text-element">Your
      content</b></p><p class="text-element"><b class="text-element">You own the rights
      to the content you post on Scribe.</b> Scribe does not claim ownership over any
      of it. However, by posting or transferring content to Scribe, Author gives us
      permission to use Author''s content solely in order to complete necessary operations
      to provide Scribe Services, including, but not limited to, storing, displaying
      and reproducing Author''s content. Scribe will never sell Author''s content to
      third parties without Author''s explicit permission.</p><p class="text-element"><br
      class="text-element"></p><p class="text-element">By publishing on Scribe, Author
      agree to allow others to view Author''s content.</p><p class="text-element">Scribe
      does not pre-screen user content, but does have the right (though no obligation)
      to refuse or remove any content Author posts or transfers to Scribe Services for
      any reason.</p><p class="text-element"><br class="text-element"></p><p class="text-element">Author
      is free to delete Author''s content from Scribe at any time, though there may
      be a delay in removing it from public view due to operational requirements. Scribe
      retains, but does not publicly display, backup copies of Author''s deleted content
      on Scribe''s servers for 30 days after Author deletes his/her account. If Author
      deletes his/her account or content, it may be permanently unrecoverable.</p><p
      class="text-element"><br class="text-element"></p><p class="text-element"><b class="text-element">Author
      is responsible for the content submitted to Scribe and assumes all risks associated
      with it, including anyone else’s reliance on its accuracy, or claims relating
      to intellectual property or other legal rights. </b>By posting, Author represents
      that Author owns or has the necessary rights to post the content on Scribe, and
      that doing so does not conflict with any other licenses granted.</p><blockquote
      class="text-element">Aside: We get a lot of questions about whether you can post
      content you own on Scribe if you’ve already posted it elsewhere, like on your
      blog. The answer is yes. If you own the content, you are welcome to copy it from
      other places and publish it on Scribe (and vice versa) as long as you did not
      give exclusive rights to other platforms or publishers.</blockquote><p class="text-element"><br
      class="text-element"></p><p class="text-element"><b class="text-element">Our content</b></p><p
      class="text-element">The look and feel of Scribe Services are copyright A Scribe
      Corporation. All rights reserved. Some portions of the site are licensed pursuant
      to third party open source licensing. For the remainder of the site, you may not
      duplicate, copy, or reuse any portion of the HTML/CSS, JavaScipt, logos, or visual
      design elements without express written permission from Scribe unless otherwise
      permitted by law.</p><p class="text-element"><b class="text-element"><br class="text-element"></b></p><p
      class="text-element"><b class="text-element">We may modify these Terms at any
      time</b></p><p class="text-element">We can change these Terms at any time. If
      the changes are material, Scribe will give notice by email or posting a message
      on the site before the changes go into effect. The notice will designate a reasonable
      amount of time (the “Notice Period”) after which the new terms will go into effect
      for all users. If Author does not agree to the new terms, Author should delete
      his/her account within the Notice Period. If Author do not delete his/her account
      within the Notice Period, Author''s content and use of the site will be subject
      to the new terms going forward.</p><p class="text-element"><br class="text-element"></p><p
      class="text-element"><b class="text-element">Scribe may modify Services at any
      time</b></p><p class="text-element">Scribe will do its best to provide a reliable
      and evolving service, but may change, terminate, or restrict access to any aspect
      of the service, at any time, without notice.</p><p class="text-element"><br class="text-element"></p><p
      class="text-element"><b class="text-element">Privacy</b></p><p class="text-element">When
      Author uses Scribe Services, Author consents to the collection and use of information
      as detailed in the text “Scribe Privacy Police”. If Author is outside the United
      States, Author consents to the transfer, storage, and processing of Author''s
      information—including but not limited to the content Author posted or transferred
      to the site and any personal information—to and within the United States and other
      countries.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b
      class="text-element">No children!</b></p><p class="text-element">Scribe is only
      for people thirteen (13) years old and over. If Author is under thirteen (13),
      Author will not be allowed on Scribe. If Scribe learns that a child under thirteen
      (13) is using Scribe, Company will terminate the child’s account.</p><p class="text-element"><br
      class="text-element"></p><p class="text-element"><b class="text-element">Security
      and Responsible Disclosures</b></p><p class="text-element">Security is a very
      serious subject, and Scribe is committed to finding opportunities to make Company''s
      services better.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b
      class="text-element">Third-party account use for sign-ins</b></p><p class="text-element">Scribe
      currently uses third parties including, but not limited to Facebook to create
      and authorize user accounts. By using those third-party services, Author agrees
      to their terms of use, privacy policy, and other agreements between Author and
      the third-party.</p><p class="text-element"><b class="text-element"><br class="text-element"></b></p><p
      class="text-element"><b class="text-element">Scribe rules</b></p><p class="text-element">Because
      Scribe is shared space, and the Company has an interest in allowing many different
      ideas to thrive, a few ground rules must be established. A complete list of our
      rules can be found in the text Scribe rules. Failure to comply with them may result
      in the Company taking action such as removing content, or suspending or deleting
      Author''s account.</p><p class="text-element"><br class="text-element"></p><p
      class="text-element"><b class="text-element">DMCA copyright policy</b></p><p class="text-element">Scribe
      deals with copyright infringement on Scribe Services in accordance with the Digital
      Millennium Copyright Act. Scribe has a policy of terminating repeat copyright
      infringers in appropriate circumstances.The contact information for Scribe’s Designated
      Agent for receipt of notices of claimed infringement is feedback@wescribe.co</p>',
    category_id: 3,
    language_id: 1,
    is_draft: false,
    subtitle: 'Effective Date: 21/08/2014',
    slug: 'scribe-terms-of-service'
  })

  scribe.texts << Text.create({
    title: 'Security and Responsible Disclosures',
    description: '<b class="text-element">Security is a priority in Scribe</b><p class="text-element">
      and we are always on the lookout for opportunities to make our service even better.
      If you are a security researcher and discover a vulnerability in one of our products,
      we would love to discuss your findings and would be grateful for your report.</p><p
      class="text-element"><br class="text-element"></p><p class="text-element">You
      may not destroy or degrade the performance of our products and services, or violate
      the privacy and integrity of user accounts and data. As long as your research
      stays within the bounds of these criteria, we welcome the dialogue.</p><p class="text-element">To
      report a vulnerability, please email us at feedback@wescribe.co</p>',
    category_id: 3,
    language_id: 1,
    is_draft: false,
    subtitle: '',
    slug: 'security-and-responsible-disclosures'
  })

  scribe.texts << Text.create({
    title: 'Regras do Scribe',
    description: '<p class="text-element">O</p><b class="text-element"> Scribe </b><p
      class="text-element">foi criado para que todas as pessoas possam compartilhar
      suas histórias e opiniões. Para isso ser feito de maneira civilizada, nós temos
      algumas regras. Elas são bem óbvias e básicas.</p><p class="text-element"><br
      class="text-element"></p><p class="text-element"><b class="text-element">Coisas
      que você NÃO deve fazer!</b></p><p class="text-element"><b class="text-element"><br
      class="text-element"></b></p><p class="text-element"><b class="text-element">Ameaças
      de violência </b></p><p class="text-element">Não ameace outros usuários com a
      violência.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b
      class="text-element">Discurso de ódio</b></p><p class="text-element">O Scribe
      é uma plataforma livre e apoia a liberdade de expressão. Acreditamos que a a melhor
      resposta ao discurso de ódio é mais discurso, ao invés de censura, mas reservamo-nos
      o direito de deletar textos com discurso de ódio.</p><p class="text-element"><br
      class="text-element"></p><p class="text-element"><b class="text-element">Spam</b></p><p
      class="text-element">Não faça Spam. Não utilize meios enganosos para gerar receita
      ou de tráfego para seu site.  Não crie mensagens com o objetivo principal de marketing!
      Você pode e deve usar a plataforma para o seu beneficio, mas o Scribe é uma plataforma
      livre de propagandas.</p><p class="text-element"><br class="text-element"></p><p
      class="text-element"><b class="text-element">Violação de direitos autorais e trademark</b>
       </p><p class="text-element">Respeite os direitos autorais e marcas comerciais
      de terceiros. A menos que você seja autorizado a usar os direitos autorais ou
      marcas registradas de trabalho de outra pessoa. </p><p class="text-element"><br
      class="text-element"></p><p class="text-element"><b class="text-element">Representação
      enganosa</b> </p><p class="text-element">Não se faça passar alguém de uma forma
      enganosa. Paródia e piadas são bem vindas, mas deixar claro que é isso que você
      está fazendo.</p><p class="text-element"><br class="text-element"></p><p class="text-element"><b
      class="text-element">Pornografia</b></p><p class="text-element">Simplesmente NÃO.
      Há outros lugares para postar isso e você sabe onde encontrar.</p><p class="text-element"><br
      class="text-element"></p><p class="text-element"><b class="text-element">Violação
      de privacidade</b></p><p class="text-element">Não use Scribe para pescar informações
      pessoais de formas enganosas. Não publique conteúdo que viole a privacidade dos
      outros, incluindo identificação pessoal ou informações confidenciais como números
      de cartão de crédito, números de segurança social, ou informações de contato não-pública.</p><p
      class="text-element"><br class="text-element"></p><p class="text-element"><b class="text-element">Phishing
      e outras fraudes </b></p><p class="text-element">Não use Scribe para phishing
      ou fraude. </p><p class="text-element"><br class="text-element"></p><p class="text-element"><b
      class="text-element">Promoção e Glorificação de comportamento autodestrutivo</b></p><p
      class="text-element">Não promova ativamente ou glorifique comportamento autodestrutivo,
      o que queremos dizer com isso?Não estimule de NENHUMA maneira em seus textos  distúrbios
      como anorexia ou bulimia e suicídio.</p><p class="text-element"><br class="text-element"></p><p
      class="text-element"><b class="text-element">Se você quebrar as regras </b></p><p
      class="text-element">Se você violar estas políticas, você receberá um aviso por
      e-mail. Se você não se explicar ou resolver o problema, sua conta poderá ser exluida
      e seu endereço de IP será bloqueado. <br class="text-element"></p><p class="text-element"><br
      class="text-element"></p><p class="text-element">Fazemos o nosso melhor para agir
      de forma justa, mas nós sempre nos reservamos o direito de suspender contas ou
      remover conteúdo, sem aviso prévio, por qualquer motivo, mas particularmente para
      proteger os nossos serviços, os usuários e a nossa comunidade. </p><p class="text-element"><br
      class="text-element"></p><p class="text-element">Nos vemos por aqui Scriber! </p><p
      class="text-element"><br class="text-element"></p><p class="text-element"><br
      class="text-element"></p>',
    category_id: 3,
    language_id: 3,
    is_draft: false,
    subtitle: '',
    slug: 'regras-do-scribe'
  })

  scribe.texts << Text.create({
    title: 'Segurança no Scribe',
    description: '<p class="text-element">A segurança é uma prioridade no Scribe e estamos
      sempre à procura de oportunidades para fazer o nosso serviço ainda melhor. Se
      você descobrir alguma vulnerabilidade em um de nossos serviços, nós gostaríamos
      de discutir suas descobertas e ficaríamos grato por seu contato.</p><p class="text-element"><br
      class="text-element"></p><p class="text-element">Ninguém deve destruir ou prejudicar
      o desempenho de nossos produtos e serviços, ou violar a privacidade e a integridade
      de contas de usuário e de dados. Enquanto a sua descoberta permanece dentro dos
      limites desses critérios, estaremos abertos para diálogo. </p><p class="text-element"><br
      class="text-element"></p><p class="text-element">Para relatar qualquer vulnerabilidade,
      envie um email para feedback@wescribe.co</p>',
    category_id: 3,
    language_id: 3,
    is_draft: false,
    subtitle: '',
    slug: 'seguranca-no-scribe'
  })
end