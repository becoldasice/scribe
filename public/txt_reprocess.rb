ng_parser = Nokogiri::HTML::DocumentFragment
Text.where({is_draft: false}).order("created_at DESC").each {
|text|

nset = Nokogiri::XML::NodeSet.new(Nokogiri::HTML::Document.new())
txt_frag = ng_parser.parse(text.description).children

txt_frag.reverse.each {
|children|

if children.text?
children = ng_parser.parse "<p class='text-element'>#{children.content()}</p>"
elsif children.matches?('div') && (!children['class'].nil? && !children['class'].include?('contains-image'))
children.name = 'p'
end

childrenClasses = 'text-element'
children['contenteditable'] = false

children.traverse {
|el|
classes = 'text-element'

if !el.parent.nil?
if el.parent().name.upcase == 'BLOCKQUOTE'
classes << ' parent-blockquote'
elsif el.parent().name.upcase == 'DIV'

if !el.name.upcase.eql?('IMG') && !el.name.upcase.eql?('BR') && !el.name.upcase.eql?('TEXT')
logger.debug "DIV CHILDREN: #{el.name}"
childrenClasses << ' contains-image full-show-parent'

logger.debug "Children classes: #{childrenClasses}"
classes << ' text-center is-full-show'
else
classes = ''
end

elsif el.parent().name.upcase == 'FIGURE'
el['contenteditable'] = false

if el.name.upcase == 'DIV'
classes << ' image-container'
elsif el.name.upcase == 'FIGCAPTION'
classes << ' image-caption'
end
end
end

el['class'] = classes
el['style'] = '' unless el['style'].nil?
el['contenteditable'] = false
}

children['class'] = childrenClasses
nset.push children unless (children.at_css('br').nil? && children.content.blank?)
}

text.description = nset.reverse.to_s
text.save
}