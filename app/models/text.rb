class Text < ActiveRecord::Base
	extend FriendlyId
	include Searchable

	before_save :post_process_post
	before_destroy :clean_timeline

  after_commit on: [:create] do
    __elasticsearch__.index_document unless self.is_draft
  end

  after_commit on: [:update] do
    __elasticsearch__.update_document unless self.is_draft
  end

	friendly_id :slugs, use: :slugged

	belongs_to :user, touch: true, counter_cache: true
	belongs_to :category, touch: true
	belongs_to :language
	has_many :text_images, dependent: :destroy
	has_many :recommendations, dependent: :destroy
	has_many :recommenders, -> { order 'recommendations.created_at ASC' }, :through => :recommendations, :source => :user
	has_and_belongs_to_many :tags, touch: true, uniq: true,
		after_add: [ lambda {|a,c| a.__elasticsearch__.index_document; c.__elasticsearch__.index_document }, :increase_tags_counter],
		before_remove: [:decrease_tags_counter],
		after_remove: [ lambda {|a,c| a.__elasticsearch__.update_document; c.__elasticsearch__.update_document } ]

	acts_as_inkwell_post
	skip_callback :create, :after, :processing_a_post

	is_impressionable counter_cache: true, unique: true, column_name: :unique_views_counter

	validates :user, presence: true

	# 
	# ELASTICSEARCH
	#

	settings nGram_analyzer_settings(3,15) do
		mappings do
			indexes :title, type: 'string', index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"
			indexes :subtitle, type: 'string', index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"
			indexes :description, type: 'string', index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"

			indexes :category do
				indexes :name, type: 'string', index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"
			end

			indexes :tags do
				indexes :name, type: 'string', index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"
			end

			indexes :user do
				indexes :first_name, type: 'string', index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"
				indexes :last_name, type: 'string', index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"
			end
		end
	end


	def as_indexed_json(options={})
		self.as_json(
			include: {
				tags: {only: :name},
				user: {methods: [:name], only: [:name]},
				category: {only: :name}
			}
		)
	end

	# 
	# SELF
	# 

	class << self

		def generate_suggestions(qt, excluded = nil)
	    @excluded_user = []
	    @excluded_user << @current_user.id unless @current_user.blank?

	    @excluded_suggestions = []
	    @excluded_suggestions << excluded unless excluded.nil?

	    Text.where
	    	.not(user_id: @excluded_user, id: @excluded_suggestions, is_draft: true)
	    	.where(language: Language.where(locale: I18n.locale).first)
	    	.limit(qt)
	    	.order("RANDOM()")
		end
	end

	# 
	# ATTRIBUTES
	# 

	def cover
		self.text_images.where(is_cover: true).first
	end

	def publish
		if is_publishable
			
			if self.is_draft
				self.is_draft = false
				self.published_at = Time.now
				processing_a_post
			end
			return self.save
		end
		is_publishable
	end

	def references
		refs = {
			facebook: 0,
			twitter: 0,
			timeline: 0,
			profile: 0,
			others: 0
		}
		self.impressions.each { |imp|
			if !/facebook/i.match(imp.referrer).blank?
				refs[:facebook] += 1
			elsif !/twitter|t.co/i.match(imp.referrer).blank?
				refs[:twitter] += 1
			elsif !/timeline/i.match(imp.referrer).blank?
				refs[:timeline] += 1
			elsif !/#{self.user.slug}\/profile/.match(imp.referrer).blank?
				refs[:profile] += 1
			else
				refs[:others] += 1
			end
		}
		refs.sort_by { |k,v|
			v
		}.reverse
	end

	#
	# INSTANCE METHODS
	#

	def insert_tag(tag)
		name = tag[:name].strip.downcase
		tag = Tag.where(name: name).first_or_create

		Rails.logger.debug "TAG: #{tag.inspect}"
		Rails.logger.debug "EXISTS? #{self.tags.exists?(tag)}"

		return nil if self.tags.exists?(tag)

		self.tags << tag
		tag
	end

	# 
	# PRIVATE
	# 

	private

	def increase_tags_counter(tag)
		tag.increment!(:texts_count)
	end

	def decrease_tags_counter(tag)
		tag.decrement!(:texts_count)
	end

	def post_process_post
		ng_parser = Nokogiri::HTML::DocumentFragment
		nset = Nokogiri::XML::NodeSet.new(Nokogiri::HTML::Document.new())
		txt_frag = ng_parser.parse(self.description).children

		txt_frag.search('//script | //iframe').each do |node|
			node.remove
		end


		if txt_frag.search('img:not([data-pre-process]):not([data-processed])[contains(data-image)]').size > 0
			txt_frag.search('img:not([data-pre-process]):not([data-processed])[contains(data-image)]').each do |img|
				img['data-pre-process'] = true
			end
			self.delay({run_at: 40.seconds.from_now}).rewrite_images
		end

		txt_frag.reverse.each do |children|


			if children.text?
				content = children.content.nil? ? children : children.content
				unless content.strip.blank?
					children.name = 'p'
					children.content = content
				end
			elsif children.matches?('div') && children.search('img').size == 0
				children.name = 'p'
			end

			##next if children.nil?

			childrenClasses = 'text-element'
			childrenClasses << ' contains-image full-show-parent' if children.matches?('div') && children.search('figure').size != 0

			children.traverse do |el|
				classes = 'text-element'

				unless el.parent.nil? || el.parent.name.eql?("#document-fragment")
					classes << ' parent-blockquote' if el.parent().matches?('blockquote')
					classes << ' image-container' if el.parent().matches?('figure') && el.matches?('div')
					classes << ' image-caption' if el.parent().matches?('figure') && el.matches?('figcaption')
					classes << ' text-center is-full-show' if el.parent().matches?('div') && el.matches?('figure')
					classes = '' if el.parent().matches?('div') && el.text?
				end

				el['class'] = classes
				el.delete('style') unless el['style'].nil?
				el.delete('contenteditable')
			end

			children['class'] = childrenClasses
			children.delete('contenteditable')
			nset.push children
		end

		self.description = nset.reverse.to_s
	end

	def is_publishable
		!self.user.blank? && !self.category.blank? && !self.language.blank? && !self.title.blank? && !self.description.blank?
	end

	def rewrite_images
		ng_parser = Nokogiri::HTML::DocumentFragment
		txt_frag = ng_parser.parse(self.description)

		imgs = txt_frag.search('img[data-image][data-pre-process]')
		imgs.each do |img|
			if img['src'].include?('blob')
				img_url = TextImage.find(img['data-image']).photo.url(:full_show)
				img['data-processed'] = true
				img['src'] = img_url
			else
				img['data-processed'] = true
			end
			img.delete('data-pre-process')
			img.delete('id')
		end

		self.description = txt_frag.to_s
		self.save
	end

	def clean_timeline
		Inkwell::TimelineItem.delete_all(item_id: self.id, item_type: Inkwell::Constants::ItemTypes::POST)
		Inkwell::BlogItem.delete_all(id: Inkwell::BlogItem.where(item_id: self.id, item_type: Inkwell::Constants::ItemTypes::POST).pluck(:id))
	end

	def slugs
		if %w(new edit delete remove).include?(self.title)
			[[:title, :id]]
		else
			[
				:title,
	      [:title, :id]
			]
		end
	end
	
end
