class Ckeditor::AttachmentFile < Ckeditor::Asset
  has_attached_file :data, 
    styles: {
      full_show: ["800>", :jpg],
      square_show: ["567x567>", :jpg],
      side_show: ["275x735>", :jpg]
    },
    convert_options: {
      all: "-quality 90 -interlace Plane -background white -flatten -alpha off"
    },
    storage: :s3,
    s3_credentials: "#{Rails.root}/config/aws.yml",
    path: ":class/:attachment/:id/:style/:filename",
    url: ":s3_domain_url",
    acl: 'private',
    processors: [:bulk]

  validates_attachment :data, content_type: {content_type: ["image/jpeg", "image/png", "image/gif"]}, file_name: {matches: [/png\Z/i, /jpe?g\Z/i, /gif\Z/i,]}
  validates_attachment_presence :data
  validates_attachment_size :data, :less_than => 100.megabytes

  process_in_background :data
  after_post_process -> { Paperclip::BulkQueue.process(data) }

  def url_thumb
    @url_thumb ||= Ckeditor::Utils.filethumb(filename)
  end
end
