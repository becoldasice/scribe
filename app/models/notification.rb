class Notification < ActiveRecord::Base
	belongs_to :user

	enum type: [:FOLLOWED, :RECOMMENDED, :PUBLISHED, :COMMENTED]
end