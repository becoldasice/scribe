class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    record.errors.add attribute, (options[:message]) unless
      value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  end
end

class User < ActiveRecord::Base
	extend FriendlyId
	include Searchable

	before_save :encrypt_password
	before_create { generate_token(:confirmation_token) }

	after_create {
		self.create_preferences!
		unless Rails.env.eql?('test')
			steve_scriber = User.friendly.find('scribe')
			self.follow(steve_scriber) if steve_scriber && self != steve_scriber
		end
	}

  after_commit on: [:create] do
    __elasticsearch__.index_document if self.is_active
  end

  after_commit on: [:update] do
    __elasticsearch__.update_document if self.is_active
  end

	attr_accessor :password, :password_confirmation, :email_confirmation, :profile_photo, :cover_photo, :name
	validates :email, :first_name, :last_name, :password, presence: true, on: :create
	validates :email, :password, confirmation: true, on: :create
	validates :email_confirmation, :password_confirmation, presence: true, on: :create
	validates :email, email: {message: :invalid_email}, uniqueness: true
	validates :slug, uniqueness: true

	phony_normalize :whatsapp_number, as: :whatsapp_number, :default_country_code => :country_code
	validates :whatsapp_number, :phony_plausible => true, length: {minimum: 10, allow_blank: true}, presence: false
	validates :email, :first_name, :last_name, :slug, presence: true, on: :update

	has_attached_file :profile_photo, 
		styles: {
			medium: ["150x150#", :jpg], 
			thumbs: ["70x70#", :jpg]
		}, 
		convert_options: {
			original: "-quality 80 -interlace Plane",
			medium: "-quality 80 -interlace Plane",
			thumbs: "-quality 80 -interlace Plane"
		},
		default_url: ":placeholder_profile",
		acl: 'private'

	has_attached_file :cover_photo, 
		styles:  {
			profile: ["x400>", :jpg], 
			timeline: ["x350>", :jpg]
		},
    convert_options: {
    	original: "-quality 80 -interlace Plane",
    	profile: "-quality 80 -interlace Plane",
    	timeline: "-quality 80 -interlace Plane"
    },
		default_url: ":placeholder_cover",
		acl: 'private'

	validates_attachment :profile_photo, content_type: {content_type: ["image/jpeg", "image/png", "image/gif"]}, file_name: {matches: [/png\Z/i, /jpe?g\Z/i, /gif\Z/i]}
	validates_attachment :cover_photo, content_type: {content_type: ["image/jpeg", "image/png", "image/gif"]}, file_name: {matches: [/png\Z/i, /jpe?g\Z/i, /gif\Z/i]}

	serialize :email_notifications

	friendly_id :slugs, use: :slugged

	has_one :preferences, class_name: "UserPreference", dependent: :destroy
	has_many :texts
	has_many :notifications, dependent: :destroy
	has_many :recommendations, dependent: :destroy

	acts_as_inkwell_user

	# 
	# ELASTICSEARCH
	# 

	settings nGram_analyzer_settings(3,10) do

		mappings do
			indexes :first_name, type: "string", index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"
			indexes :last_name, type: "string", index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"
			indexes :bio, type: "string", index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"

			indexes :texts do
				indexes :title, type: "string", index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"
				indexes :subtitle, type: "string", index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"
				indexes :description, type: "string", index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"
			end
		end

	end

	def as_indexed_json(options={})
		self.as_json(
			include: {
				texts: {only: [:title, :subtitle, :description]}
			}
		)
	end

	# 
	# ATTRIBUTES
	# 

	def name
		"#{self.first_name} #{self.last_name}"
	end

	def published_texts
		self.texts.where({is_draft: false})
	end

	def drafts
		self.texts.where({is_draft: true})
	end

	def follow(user)
		if user.is_active
			super
			UserMailer.delay(run_at: 10.seconds.from_now).new_follower(self, user) if user.preferences.email_on_follow
		end
	end
  
	def joined_with_facebook(friends)
		unless friends.blank?
		  friends.each do |friend|
		    UserMailer.delay({run_at: 10.seconds.from_now}).just_started(self, friend)
		  end
		end
		self.done_facebook_follow_friends = true
		save!
	end

	def timeline(page = 1, per = 4)
		timeline_itens = Inkwell::TimelineItem.where(owner_id: self.id, owner_type: Inkwell::Constants::OwnerTypes::USER).order("created_at DESC")
		texts = []
		timeline_itens.to_a.uniq { |item|
			item.item_id
		}.each do |item|
			unless item.item_type != Inkwell::Constants::ItemTypes::POST
				texts << item.item_id
			end
		end
		texts_array = Text.where("id IN (?)", texts).order("published_at DESC").to_a
		Kaminari.paginate_array(texts_array).page(page).per(per)
	end

	def recommend(text)
		self.recommended?(text) ? undo_recommendation(self.recommendations.where(text: text).first) : do_recommendation(text)
	end

	def recommended?(text)
		self.recommendations.exists?(text_id: text.id)
	end

	def post_comment(comment_params)
		comment = self.comments.create(comment_params)
		
		json_comment = {
			authorAvatarUrl: comment.user.profile_photo.url(:thumbs),
			authorName: comment.user.name,
			comment: comment.body,
			authorId: comment.user.id,
			id: comment.id,
			sectionId: comment.section_id
		}

		json_comment[:parent_id] = User.process_comment_reply(self, comment) unless comment.parent_id.blank?
		UserMailer.delay({run_at: 10.seconds.from_now}).commented_text(self, comment) unless comment.commentable.user.id.eql?(self.id)
		
		json_comment
	end

	def deactivate_account
		self.is_active = false
		save!
		UserMailer.delay({run_at: 10.seconds.from_now}).deactivate_account(self)
	end

	def send_reset_email
		generate_token(:password_reset_token)
		save!
		PasswordRecoveryMailer.delay({run_at: 10.seconds.from_now}).recover_password(self)
	end

	# 
	# 	SELF
	# 

	class << self

		def authenticate(email, password)
			user = where(email: email).first
			if user && user.is_active && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
				user.last_login = Time.now
				user.save!
				user
			end
		end

		def facebook_auth(auth_data)
			user = User.where("facebook_uid = ? OR email = ?", auth_data.uid, auth_data.extra.raw_info.email).first || create_from_facebook(auth_data)
			user.update({
				facebook_uid: auth_data.uid.to_s,
				facebook_profile_url: auth_data.extra.raw_info.link,
				facebook_name: "#{auth_data.extra.raw_info.first_name} #{auth_data.extra.raw_info.last_name}",
				current_oaccess_token: auth_data.credentials.token,
				last_login: Time.now
			})
			user
		end

		def twitter_auth(auth_data)
			user = User.where(twitter_uid: auth_data.uid.to_s).first || create_from_twitter(auth_data)
			user.update({
				twitter_uid: auth_data.uid.to_s,
				twitter_user: auth_data.info.nickname,
				twitter_picture: auth_data.info.image.blank? ? nil : auth_data.info.image,
				last_login: Time.now
			})
			user
		end

		def link_facebook(current, auth_data)
			exists = User.where(facebook_uid: auth_data[:uid]).first
			unless exists.blank?
				exists.update({
					facebook_uid: nil,
					facebook_profile_url: nil,
					facebook_name: nil,
					current_oaccess_token: nil
				})
			end
			current.update({
				facebook_uid: auth_data.uid.to_s,
				facebook_profile_url: auth_data.extra.raw_info.link,
				facebook_name: "#{auth_data.extra.raw_info.first_name} #{auth_data.extra.raw_info.last_name}",
				current_oaccess_token: auth_data.credentials.token
			})
		end

	  %w(twitter instagram).each do |service|
	    define_method("link_#{service}") do |current, auth_data|
				exists = User.where("#{service}_uid" => auth_data[:uid]).first

				unless exists.blank?
					attrs = exists.attributes.select{ |k,v| k.start_with?(service)}
					attrs.each{|k,v| attrs[k] = nil}
					exists.update(attrs)
					exists.update({is_active: false})
				end

				current.update({
					:"#{service}_uid" => auth_data[:uid],
					:"#{service}_user" => "@#{auth_data[:info][:nickname]}",
					:"#{service}_picture" => auth_data[:info][:image]
				})
	    end
	  end
		 
		def friends_by_uid(user)
			graph = Koala::Facebook::API.new(user.current_oaccess_token)
			fb_friends = graph.get_connections("me", "/friends")
			User.where('facebook_uid IN (?)', fb_friends.map{|f| f["id"]})
		end

		def friends_by_email(contacts)
			User.where('email IN (?)', contacts.map{|c| c[:email]})
		end

		def invite_contact(user, contact)
			if validate_email(contact)
				UserMailer.delay({run_at: 10.seconds.from_now}).invite_friend(user, contact)
				true
			else
				false
			end
		end

		def mass_invite(user, contacts)
			contacts.each do |c|
				UserMailer.delay({run_at: 10.seconds.from_now}).invite_friend(user, c) if User.validate_email(c)
			end
		end

		def merge(current, to_merge)
			to_merge_followers = User.where(id: to_merge.followers)
			to_merge_following = User.where(id: to_merge.followings)
			to_merge_texts = to_merge.texts

			to_merge_followers.each do |follower|
				follower.follow current unless follower.follow?(current) || follower.eql?(current)
				follower.unfollow to_merge
			end

			to_merge_following.each do |following|
				current.follow following unless current.follow? following
				to_merge.unfollow following
			end

			to_merge_texts.each do |text|
				text.update({user: current})
			end
			to_merge.update({facebook_uid: nil, facebook_profile_url: nil, facebook_name: nil})
			to_merge.deactivate_account
		end

		def process_comment_reply(user, reply)
			arel_comment = Inkwell::Comment.arel_table

			comments = Inkwell::Comment.select(:user_id, :commentable_id, :body).where(
				arel_comment[:id].eq(reply.parent_id)
				.or(arel_comment[:parent_id].eq(reply.parent_id))
			).distinct(:user_id)

			comments.each { 
				|c|
				UserMailer.delay({run_at: 10.seconds.from_now}).replied_comment(c.user, user, Text.find(c.commentable_id), c.body) unless c.user.id.eql?(user.id)
			}.delay({run_at: 10.seconds.from_now})

			return reply.parent_id
		end

		# 
		# PRIVATE
		# 

		private

		def create_from_facebook(auth_data)
			pass = SecureRandom.base64

			User.new({
				first_name: auth_data.extra.raw_info.first_name,
				last_name: auth_data.extra.raw_info.last_name,
				email: auth_data.extra.raw_info.email,
				email_confirmation: auth_data.extra.raw_info.email,
				gender: auth_data.extra.raw_info.gender.blank? ? nil : auth_data.extra.raw_info.gender[0],
				profile_photo: auth_data.extra.image.blank? ? nil : JSON.load(URI.parse(auth_data.info.image<<"&redirect=false"))["data"]["url"],
				cover_photo: auth_data.extra.raw_info.cover.blank? ? nil : auth_data.extra.raw_info.cover.source,
				password: pass,
				password_confirmation: pass
			})
		end

		def create_from_twitter(auth_data)
			name_array = auth_data.info.name.split(" ")
			pass = SecureRandom.base64
			User.new({
				first_name: name_array[0],
				last_name: name_array[1],
				bio: auth_data.info.description,
				password: pass,
				password_confirmation: pass,
				is_active: false
			})
		end

	end

	#
	# MAILERS
	#

	class MailerTasks
		class << self
			
			def top_ten
				User.where('is_active = ? AND email IS NOT NULL AND (prefered_language = ? OR prefered_language IS NULL)', true, 'pt-BR').find_each do |u|
					SystemMailer.delay({run_at: 10.seconds.from_now}).send_top_ten(u) if u.preferences.email_top_ten
				end
			end

			def weekly_writers
				User.where('is_active = ? AND email IS NOT NULL AND (prefered_language = ? OR prefered_language IS NULL)', true, 'pt-BR').find_each do |u|
					SystemMailer.delay({run_at: 10.seconds.from_now}).weekly_writers(u) if u.preferences.email_weekly_writers
				end
			end

			def steve_suggestions(day)
				User.where('is_active = ? AND email IS NOT NULL AND (prefered_language = ? OR prefered_language IS NULL)', true, 'pt-BR').find_each do |u|
					SystemMailer.delay({run_at: 10.seconds.from_now}).steve_suggestions(day, u) if u.preferences.email_steve_suggestions
				end
			end

			def week_trends
				User.where('is_active = ? AND email IS NOT NULL AND (prefered_language = ? OR prefered_language IS NULL)', true, 'pt-BR').find_each do |u|
					SystemMailer.delay({run_at: 10.seconds.from_now}).week_trends(u) if u.preferences.email_week_trends
				end
			end
			
		end
	end

	# 
	# PRIVATE
	# 

	private

	def do_recommendation(text)
		self.recommendations.create!(text: text)
		text.reload
		if text.recommendations_count % 3 == 0 && text.user.preferences.email_on_recommendations
			UserMailer.delay({run_at: 10.seconds.from_now}).recommended_text(text.user, text, text.recommenders.last(3))
		end
	end

	def undo_recommendation(recommendation)
		self.recommendations.destroy(recommendation)
	end

	def self.validate_email(email)
		!/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i.match(email).blank?
	end

	def encrypt_password
		if password.present?
			self.password_salt = BCrypt::Engine.generate_salt
			self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
		end
	end
	
	def generate_token(col)
		self[col] = SecureRandom.base64
	end

	def slugs
		[
			:name,
      [:name, :id]
		]
	end
end
