class TextImage < ActiveRecord::Base

	belongs_to :text

	attr_accessor :deferred_photo

	has_attached_file :photo, 
		styles: lambda {
			|photo|
			photo.instance.generate_styles
		},
    convert_options: {
    	all: "-quality 100 -interlace Plane -background white -flatten -alpha off"
    },
		acl: 'private',
		processors: [:bulk]

	validates_attachment :photo, content_type: {content_type: ["image/jpeg", "image/png", "image/gif"]}, file_name: {matches: [/png\Z/i, /jpe?g\Z/i, /gif\Z/i,]}

	after_save :process_deferred
	process_in_background :photo
	after_post_process -> { Paperclip::BulkQueue.process(photo) }

	def generate_styles
		styles = {}
		unless self.is_cover.nil?
			if self.is_cover
				styles[:half_list] = ["500x250#", :jpg]
				styles[:full_list] = ["1000x500#", :jpg]
			else
				styles[:full_show] = ["800>", :jpg]
				styles[:square_show] = ["567x567>", :jpg]
				styles[:side_show] = ["275x735>", :jpg]
			end
		end
		styles
	end

	private

	def process_deferred
		unless self.deferred_photo.nil?
      current_format = File.extname(self.deferred_photo)
      basename = File.basename(self.deferred_photo, current_format)
      dst = Tempfile.new([basename, '.jpg'])

      Paperclip.run("convert", "'#{self.deferred_photo}' -quality 100 -interlace Plane -background white -flatten -alpha off '#{dst.path}'")
      Paperclip.run("jpegoptim", "-o --strip-all #{dst.path}")

			self.photo = dst
			self.deferred_photo = nil
			save!
		end
	end
end