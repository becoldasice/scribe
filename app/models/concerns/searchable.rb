require 'elasticsearch/model'

module Searchable
  extend ActiveSupport::Concern

  included do
    include Elasticsearch::Model
    include Elasticsearch::Model::Callbacks
    after_touch() { __elasticsearch__.index_document }
    index_name Rails.env.eql?('test') ? [Rails.env, model_name.collection.gsub(/\//, '-')].join('_') : model_name.collection

    class QueryBuilder
      def initialize(opts)
        @query_options = default_options.merge(opts)
      end

      def default_options
        {
          query_type: :multi_match
        }
      end

      def build_query(term)
        es_query = {
          @query_options[:query_type] => {
            query: "#{term}",
            fields: @query_options[:fields],
            operator: @query_options[:operator],
            type: @query_options[:type],
            minimum_should_match: @query_options[:minimum_should_match]
          }
        }

        unless @query_options[:filter].blank?
          filter = @query_options[:filter]
          filtered_query = {
            filtered: {
              query: es_query,
              filter: @query_options[:filter],
            }
          }
          return {
            query: filtered_query
          }
        end

        return {
          query: es_query
        }
      end
    end

    class << self
      
      def search_for(opts, term)
        query = QueryBuilder.new(opts).build_query(term)
        self.search(query)
      end

      def nGram_analyzer_settings(min, max)
        {
          analysis: {
            filter: {
              nGram_filter: {
                type: "edgeNGram",
                side: "front",
                min_gram: min,
                max_gram: max,
                token_chars: ["letter", "digit", "punctuation", "symbol"]
              }
            },
            analyzer: {
              "ngram_analyzer" => {
                type: "custom",
                tokenizer: "whitespace",
                filter: ["lowercase", "asciifolding", "nGram_filter"]
              },
              whitespace_analyzer: {
                type: "custom",
                tokenizer: "whitespace",
                filter: ["lowercase", "asciifolding"]
              }
            }
          }
        }
      end
    end

    def set_index(name)
      return Rails.env.eql?("test") ? "test_#{name.to_s}" : name
    end
  end
end