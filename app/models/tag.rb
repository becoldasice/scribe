class Tag < ActiveRecord::Base
  include Searchable

	has_and_belongs_to_many :texts
	validates :name, uniqueness: true


  settings nGram_analyzer_settings(3,10) do
    mappings do
      indexes :name, type: 'string', index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"

      indexes :texts do
        indexes :title, type: "string", index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"
        indexes :subtitle, type: "string", index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"
        indexes :description, type: "string", index_analyzer: "ngram_analyzer", search_analyzer: "whitespace_analyzer"
      end
    end
  end

  class << self

    def reset_texts_count
      Tag.find_each do |tag|
        count = tag.texts.count
        tag.update({texts_count: count})
      end
    end

    def get_cloud(size)
      Tag.all.order("texts_count DESC").limit(size)
    end

    def merge_duplicates
      ts = Tag.where("LOWER(tags.name) LIKE LOWER(tags.name)").group_by{|t|t.name.gsub(/#/,'').strip.downcase}

      ts.each do |k,tags|
        tags.sort!{|x,y| y.texts_count <=> x.texts_count}
        first = tags.shift
        tags.each do |tag|
          tag.texts.each do |text|
            text.tags<<first
            text.tags.delete(tag)
          end
          tag.destroy!
        end
      end
      
    end

  end
end