class Language < ActiveRecord::Base
	validates :name, :locale, uniqueness: true, presence: true

	has_many :texts
end
