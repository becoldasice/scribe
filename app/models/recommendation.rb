class Recommendation < ActiveRecord::Base
	belongs_to :user, counter_cache: true
	belongs_to :text, counter_cache: true
end