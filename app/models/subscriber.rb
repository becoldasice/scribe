class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    record.errors.add attribute, (options[:message]) unless
      value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  end
end

class Subscriber < ActiveRecord::Base
	validates :email, uniqueness: true, presence: true, email: {message: :invalid_email}
end
