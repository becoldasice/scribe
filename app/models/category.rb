 class Category < ActiveRecord::Base
 	include Searchable
	validates :name, uniqueness: true, presence: true

	has_many :texts

	def list_texts_by_type(type, language = Language.where(locale: I18n.locale).first,page = 1, per=5)
		@list = self.texts(true).includes(:user, :tags, :language).where(language: language, is_draft: false)
		case type
		when ScribeConstants::TOP_TEN
			top_ten(page, per)
		when ScribeConstants::LAST_TEXTS
			last_texts(page, per)
		when ScribeConstants::MOST_VIEWED
			most_viewed(page, per)
		end
	end

	private

	def last_texts(page, per)
		@list.order("published_at DESC")
			.page(page)
			.per(per)
	end

	def most_viewed(page, per)
		@list.order("unique_views_counter DESC")
			.where("published_at >= ?", 1.week.ago)
			.page(page)
			.per(per)
	end

	def top_ten(page, per)
		@list.order("recommendations_count DESC")
			.where("published_at >= ?", 1.week.ago)
			.page(page)
			.per(per)
	end
end