class SystemMailer < AbstractMailer

  def send_top_ten(user)
    language = user.prefered_language || I18n.locale
    send_mail(language, 'top-10-of-the-week-scribe', user, "Os melhores textos da semana no Scribe", {})
  end

  def welcome_email(user)
    language = user.prefered_language || I18n.locale
    send_mail(language, 'welcome-tpl', user, "Bem vindo ao Scribe!", {})
  end  

  def steve_suggestions(day, user)
    language = user.prefered_language || I18n.locale
    send_mail(language, "steve-recommenda-#{day}", user, "Recomendações do Steve Scriber", {})
  end

  def weekly_writers(user)
    @recs = Text.select("*")
      .from("(#{Text.select('DISTINCT ON (user_id) *')
              .where('published_at >= ? AND user_id IN (?)', 1.week.ago, user.following_ids)
              .order('user_id').to_sql}) tmp"
      ).order("tmp.published_at DESC")
      .limit(4)
      .to_a || []

    if @recs.size < 4
      @recs.concat(Text.select("*")
        .from("(#{Text.select('DISTINCT ON (user_id) *')
                  .where("published_at >= ? AND user_id <> ?", 1.week.ago, user.id)
                  .order('user_id DESC').to_sql}) tmp"
        ).order('RANDOM()')
        .limit(4-@recs.size)
        .to_a
      )
    end

    vars = {
      'WEEKLY_REC_CTA' => I18n.t(:weekly_writers_mail_cta),
      'WEEKLY_REC_END' => I18n.t(:weekly_writers_mail_end_html, timeline_url: timeline_url)
    }

    @recs.each_with_index do |rec, idx|
      vars["WEEKLY_REC_#{idx+1}"] = "#{rec.user.name} escreveu:"
      vars["WEEKLY_REC_#{idx+1}_COVER"] = rec.cover.blank? ? "#{root_url}#{ActionController::Base.helpers.asset_path('covers/categories/text_default_'<<(rec.category_id.to_s)<<'.jpg')}" : rec.cover.photo.url
      vars["WEEKLY_REC_#{idx+1}_HREF"] = text_url(rec)

      caption = "<h3 style='font-weight: 900; margin-bottom: 5px;'> <a href='#{text_url(rec)}' style='color: #343838; font-size: inherit; text-decoration: none;'>#{rec.title}</a></h3>"
      caption << "<h5 style='font-weight: 900; color: #343838; margin: 10px 0;'>#{rec.subtitle}</h5>" unless rec.subtitle.nil?
      caption << "<span style='line-height: 1.1'>#{Sanitize.clean(rec.description).truncate(140, omission: '')}"
      caption << "<a href='#{text_url(rec)}' style='color: blue'>...</a>"
      caption << "</span>"

      vars["WEEKLY_REC_#{idx+1}_CAPT"] = caption
    end

    send_mail(I18n.locale, 'weekly-writers-tpl', user, I18n.t(:weekly_writers_mail_title), vars)
  end

  def week_trends(user)
    @categories = Category.all
    @texts = []
    @users = User.where("texts_count > 2 AND prefered_language = ?", 'pt-BR').order("RANDOM()").limit(2)
    language = user.prefered_language || I18n.locale

    vars = {
      'SEE_PROFILE' => I18n.t(:see_profile)
    }

    @categories.each_with_index do |cat, idx|
      if cat.id.eql?(3)
        cat.texts.where('published_at >= ?', 1.week.ago).order("recommendations_count DESC").first(2).each do |t|
          @texts << t
        end
      else
        @texts << cat.texts.where('published_at >= ?', 1.week.ago).order("recommendations_count DESC").first
      end
    end

    @texts.each_with_index do |rec, idx|
      vars["RECOMMENDED_TXT_#{idx+1}"] = rec.title
      vars["RECOMMENDED_TXT_#{idx+1}_URL"] = text_url(rec)
      vars["RECOMMENDED_TXT_#{idx+1}_PIC"] = rec.cover.blank? ? "#{root_url}#{ActionController::Base.helpers.asset_path('covers/categories/text_default_'<<(rec.category_id.to_s)<<'.jpg')}" : rec.cover.photo.url
    end

    @users.each_with_index do |u, idx|
      vars["RECOMMENDED_#{idx+1}"] = u.name
      vars["RECOMMENDED_#{idx+1}_PIC"] = u.profile_photo.url(:medium)
      vars["RECOMMENDED_#{idx+1}_URL"] = user_show_url(u)
    end

    send_mail(I18n.locale, 'week-trends-tpl', user, "Os mais populares desta semana no Scribe", vars)
  end
end