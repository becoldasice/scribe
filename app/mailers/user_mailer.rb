class UserMailer < AbstractMailer

  def deactivate_account(user)
    send_mail(user.prefered_language, 'deactivate-account-tpl', user, I18n.t(:deactivate_account_mail_title), {'DEACTIVATE_ACCOUNT' => I18n.t(:account_deactivated)})
  end

  %w(new_follower just_started).each do |mail|
    define_method("#{mail}") do |user, recipient|
      vars = {
        "#{mail}_1" => I18n.t("#{mail}_mail_description_pt1_html".to_sym),
        "#{mail}_2" => I18n.t("#{mail}_mail_description_pt2_html".to_sym),
        'USER_NAME' => user.name,
        'USER_PROFILE_URL' => user_show_url(user),
        'SEE_PROFILE' => I18n.t(:see_profile)
      }
      send_mail(recipient.prefered_language, "#{mail.sub('_','-')}-tpl", recipient, I18n.t("#{mail}_mail_title"), vars)
    end
  end

  def invite_friend(user, contact)
    language = user.prefered_language || I18n.locale
    vars = {
      'INVITED_1' => I18n.t(:invited_mail_pt1),
      'USER_NAME' => user.name,
      'INVITED_2' => I18n.t(:invited_mail_pt2_html),
      'USER_PROFILE_URL' => user_show_url(user),
      'SEE_PROFILE' => I18n.t(:see_profile),
      'SIGNUP' => I18n.t(:signup),
      'SIGNUP_URL' => root_url
    }
    recipient = {
      email: contact,
      name: ''
    }
    send_mail(language, 'invite-friend-tpl', recipient, I18n.t(:invited_mail_title), vars)
  end

  def recommended_text(user, text, recommenders)
    language = user.prefered_language || I18n.locale
    vars = {
      'RECOMMENDED_1' => I18n.t(:recommended_mail_pt1_html, text: text.title, text_url: text_url(text)),
      'SEE_PROFILE' => I18n.t(:see_profile),
      'RECOMMENDED_2' => I18n.t(:recommended_mail_pt2_html),
      'TEXT_URL' => text_url(text, anchor: "share-board"),
      'SHARE_TEXT' => I18n.t(:share_your_text)
    }
    recommenders.each_with_index do |r, idx|
      vars["RECOMMENDER_#{idx+1}"] = r.name
      vars["RECOMMENDER_#{idx+1}_PIC"] = r.profile_photo.url(:thumbs)
      vars["PROFILE_RECOMMENDER_#{idx+1}"] = user_show_url(r)
    end
    send_mail(language, 'recommended-text-tpl', user, I18n.t(:recommended_mail_title), vars)
  end

  def commented_text(user, comment)
    text = comment.commentable
    language = text.user.prefered_language || I18n.locale
    vars = {
      'COMMENT_DESC' => I18n.t(:commented_mail_content_html, user_profile: user_show_url(user), user: user.name, text_url: text_url(text), text_title: text.title),
      'PROFILE_URL' => user_show_url(user),
      'PROFILE_PIC' => user.profile_photo.url(:medium),
      'COMMENT' => comment.body,
      'TEXT_URL' => text_url(text, anchor: "share-board"),
      'SHARE_TEXT' => I18n.t(:share_your_text)
    }
    send_mail(language, 'new-comment-tpl', text.user, I18n.t(:commented_mail_title), vars)
  end

  def replied_comment(commenter, replier, text, comment)
    language = commenter.prefered_language || I18n.locale
    vars = {
      'COMMENT_DESC' => I18n.t(:replied_comment_mail_content_html, user_profile: user_show_url(replier), user: replier.name, text_url: text_url(text), text_title: text.title),
      'PROFILE_URL' => user_show_url(replier),
      'PROFILE_PIC' => replier.profile_photo.url(:medium),
      'COMMENT' => comment,
      'TEXT_URL' => text_url(text, anchor: "share-board"),
      'SHARE_TEXT' => I18n.t(:share_your_text)
    }
    send_mail(language, 'new-comment-tpl', commenter, I18n.t(:replied_comment_mail_title), vars)
  end

  private

  def send_mail(language, template, recipient, subject, vars)
    I18n.with_locale(language) do
      mandrill_mail(
          template: template,
          subject: subject,
          to: {email: recipient.email, name: recipient.name},
          vars: vars,
          important: true,
          inline_css: true,
          async: true
      ).deliver
    end
  end

end
