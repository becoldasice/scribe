class AbstractMailer < MandrillMailer::TemplateMailer
  default from: "scribe@wescribe.co", from_name: "Scribe"
  
  private
  def send_mail(language, template, recipient, subject, vars)
    I18n.with_locale(language) do
      mandrill_mail(
          template: template,
          subject: subject,
          to: {email: recipient.email, name: recipient.name},
          vars: vars,
          important: true,
          inline_css: true,
          async: true
      ).deliver
    end
  end
end
