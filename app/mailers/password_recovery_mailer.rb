class PasswordRecoveryMailer < MandrillMailer::TemplateMailer
  default from: "scribe@wescribe.co", from_name: "Scribe"

  def recover_password(user)
  	@user = user
  	mandrill_mail(
          template: 'reset-password-tpl',
          subject: I18n.t(:reset_pass_title),
          to: {email: user.email},
          vars: {
            'RESET_PASS_DESC' => I18n.t(:reset_pass_desc, name: @user.name),
            'RESET_PASS_LINK' => reset_password_edit_url(password_reset_token: @user.password_reset_token)
          },
          important: true,
          inline_css: true,
          async: true
      ).deliver
  end

end
