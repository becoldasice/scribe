function initLandingPage () {

  $(document).off('.loadCategoryTexts')
    .on('click.loadCategoryTexts', '.categoriesMenu--category', function (e) {
      var loadFrom = e.target .dataset.loadFrom
      , textsQt = e.target.dataset.textsQt
      , $postsContainer = $('.landingPage--otherPosts')
      , $ajaxLoader = $postsContainer.find('.landingPage--ajaxLoader');

      $('.categoriesMenu').find('.isActive').removeClass('isActive');
      this.classList.add('isActive');
      $postsContainer.find('.landingPage--categoryTexts').fadeOut('slow', function () {
        $(this).remove();
      });

      $.ajax({
        method: 'GET',
        url: loadFrom,
        data: {
          limit: textsQt
        },
        beforeSend: function () {
          $ajaxLoader.show("fast");
        }
      }).done(function (data, status, jqxHR) {
        $(data).css({display: 'none'});
        $postsContainer.find('.scribeGrid--newContainer').append(data);

        $ajaxLoader.hide('fast', function () {
          $(data).fadeIn('fast');
        });

      }).always(function (evt) {
        if($ajaxLoader.is(':visible')) {
          $ajaxLoader.delay(400).hide('fast');
        }
      });

    });

  $(document).off('.toggleModal')
    .on('click.toggleModal', '.js-toggleModal', function (e) {
      e.preventDefault();
      if($('.offcanvas-show').length > 0) {
        $('.offcanvas-show').removeClass('offcanvas-show');
      }
      $(this.dataset.modal).modal('show');
    });

  $(document).off('.validateForm')
    .on('submit.validateForm', '.js-validateForm', function (e) {
      var $form = $(this),
        isValid = true;

      $form.find('.scribeForm--input').css({
        border: '0'
      });

      _.each($form.find('.scribeForm--input'), function (el, idx) {
        if(el.value === '') {
          $(el).css({
            border: '1px solid red'
          });
          isValid = false;
        }
      });

      _.each($form.find('.js-matchPrevious'), function (el, idx) {
        var $prev = $(el).prev();

        if($prev.val() !== $(el).val()) {
          $(el).add($prev).css({
            border: '1px solid red'
          });

          isValid = false;
        }
      });

      return isValid;
    });

  $(document).off('.login')
    .on({
      'ajax:success.login' : function (evt, data, status, jqXHR) {
        window.location = data.url;
      },
      'ajax:error.login': function (evt, jqXHR, status, error) {
        var errorMessage = document.createElement('p'),
          $elf = $(this);

        errorMessage.classList.add('bg-error', 'flash', 'scribeGrid--newContainer');
        errorMessage.textContent = JSON.parse(jqXHR.responseText).error;
        document.body.appendChild(errorMessage);

        setTimeout(function () {
          $(errorMessage).slideUp('slow', function () {
            $(this).remove();
            $elf.find('.scribeForm--input').css({
              border: '0'
            });
          })
        }, 2000);
      }
    }, '.loginForm');

  $(document).off('.signup')
    .on({
      'ajax:success.signup': function (evt, data, status, jqXHR) {
        window.location = data.url;
      },
      'ajax:error.signup': function (evt, jqXHR, status, error) {
        var $elf = $(this),
          errors = JSON.parse(jqXHR.responseText);

        _.forIn(errors, function (value, key, obj) {
          var error = document.createElement('small');


          _.each(value, function (el, idx) {
            error.innerHTML += el+"<br/>";
          });

          error.style.display = 'block';
          error.style.color = 'red';

          $(error).insertAfter($elf.find('#user_'+key));
          setTimeout(function () {
            $(error).slideUp('slow', function () {
              $(this).remove();
            });
            $elf.find('.scribeForm--input').css({
              border: '0'
            });
          }, 2500);
        });
      }
    }, '.signupForm');

  $(document).off('.clearErrors')
    .on('focus.clearErrors', '.scribeForm--input', function (e) {
      $(this).css({
        border: '0'
      });
    });

  $('.categoriesMenu--category').first().click();
}

$(document).ready(initLandingPage);
$(document).on('page:load', initLandingPage);