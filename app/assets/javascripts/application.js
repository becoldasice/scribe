//= require jquery
//= require jquery_ujs
//= require jquery.infinitescroll.min
//= require jquery.inputmask
//= require side-comments.min
//= require background-check.min
//= require load-image.all.min
//= require s3_direct_upload
//= require ckeditor/init
//= require bootstrap
//= require sanitize
//= require markdown.converter
//= require markdown.sanitizer
//= require nprogress
//= require nprogress-turbolinks
//= require hopscotch
//= require_tree .

function initApplication() {
  var localStorage = window.localStorage;

  // Initialize infinite scroll
  (function (tl, cat, fol, discover, popular) {
    // Defautl infinitescroll options
    var infiniteScrollOptions = {
      navSelector: "nav.pagination",
      nextSelector: "nav.pagination a[rel=next]",
      itemSelector: null,
      animate: false,
      loading: {
        finishedMsg: '',
        img: 'data:image/gif;base64,R0lGODlhAQABAHAAACH5BAUAAAAALAAAAAABAAEAAAICRAEAOw==',
        msgText: ''
      }
    };

    // Remove any previous isntances of infinitescroll binded to window
    $(window).unbind('.infscr');

    // Initialize infinitescroll for the current page, if necessary
    if (tl.length) {
      tl.infinitescroll("destroy");
      infiniteScrollOptions.itemSelector = ".timelineSection > .timelineSection--textBlocks > .textBlock--container";
      tl.infinitescroll(infiniteScrollOptions);
    }

    if (fol.length) {
      fol.infinitescroll("destroy");
      infiniteScrollOptions.itemSelector = "#fol-section .user-grid-container";
      fol.infinitescroll(infiniteScrollOptions);
    }

    if (cat.length) {
      infiniteScrollOptions.itemSelector = ".categoriesContainer > .scribeGrid--row > .textBlock--container";
      cat.infinitescroll("destroy");
      cat.infinitescroll(infiniteScrollOptions);
    }

    if (discover.length) {
      discover.infinitescroll("destroy");
      infiniteScrollOptions.itemSelector = ".discoverContainer > .scribeGrid--row > .textBlock--container";
      discover.infinitescroll(infiniteScrollOptions);
    }

    if (popular.length) {
      popular.infinitescroll("destroy");
      infiniteScrollOptions.itemSelector = ".popularContainer > .scribeGrid--row > .textBlock--container";
      popular.infinitescroll(infiniteScrollOptions);
    }

  })($('.timelineSection > .timelineSection--textBlocks'), 
     $('.categoriesContainer > .scribeGrid--row'), 
     $('#fol-section'),
     $('.discoverContainer > .scribeGrid--row'),
     $('.popularContainer > .scribeGrid--row')
  );

  $('body').tooltip({
    selector: '[data-toggle="tooltip"]',
    trigger: 'hover'
  });

  $(document).off('.lowercase-email').on('submit.lowercase-email', '#login-form, #forgot-password, .js-lowercase-values', function (e) {
    if($(this).find('#email').val().length > 0){
      $(this).find('#email').val($(this).find('#email').val().toLowerCase());
      return true;
    } else {
      e.preventDefault();
      e.stopPropagation();
      $(this).find('#email').css({borderColor: 'red'});
      return false;
    }
  });

  $(document).off('.offcanvas-toggle')
    .on('click.offcanvas-toggle', '.js-toggleOffcanvas', function (e) {
      $('#scribe-application-container, .scribeNavbar, .scribeNavbar--mobileHeader').toggleClass('offcanvas-show');
    });
  
  $(document).off('.fbshare').on('click.fbshare', '.facebook-share', function (e) {
    e.preventDefault();
    e.stopPropagation();
    FB.ui({
      method: 'share',
      display: 'popup',
      href: $(this).data('href')
    }, function (response) {
      console.log(response);
    });
  });

  $(document).off('.dismiss')
    .on('click.dismiss', '.js-dismissParent', function (e) {
      $(this).parent().fadeOut();
    });

  $(document).off('.inviteSingle').on({
    'ajax:before.inviteSingle': function (evt) {
      if ($(this).find('#email').val() === '') {
        return false;
      }
    },
    'ajax:success.inviteSingle': function (evt, data, status, jqXHR) {
      var responseElement = document.createElement('h5');
      responseElement.textContent = data.result;
      
      $('.invitationBox').append(responseElement);

      setTimeout(function () {
        $(responseElement).fadeOut('slow', function () {
          $(this).remove();
        });
      }, 2000);
    }
  }, '.js-inviteSingleForm');

  $(document).off('.follow-all').on('click.follow-all', '#follow-all-email', function (e) {
    e.preventDefault();
    var action = $(this).data('action'),
        emails = $(this).data('emails');

    $.ajax({
      method: "POST",
      url: action,
      data: {
        emails: emails
      },
      dataType: 'json'
    }).done(function (data, status, jqXhr) {
      $('#skip-step').click();
    }).error(function (jqXhr, status, message) {
      console.log(message);
    });
  });

  $(document).off('.invite-all').on('click.invite-all', '#invite-all', function (e) {
    $('#mail-contacts-form').submit();
  });

  $(document).off('.twshare')
    .on('click.twshare', '.js-twitterShare', function (e) {
      e.preventDefault();
      e.stopPropagation();
      var self = this;
      $.ajax({
        type: "POST",
        url: $(this).data('shortener'),
        success: function (data) {
          var twUrl = 'https://twitter.com/share?url='+data.url+'&text='+$(self).data('text')+'&via='+$(self).data('via');
          window.open(twUrl, 'sharer', 'toolbar=0,status=0,width=650,height=254');
        }
      });
    });

  $(document).off('.gPlusShare')
    .on('click.gPlusShare', '.js-gPlusShare', function (e) {
      e.preventDefault();
      e.stopPropagation();
      var gplusUrl = 'https://plus.google.com/share?url='+$(this).data('url');
      window.open(gplusUrl, 'sharer', 'toolbar=0,status=0,width=650,height=254');
    });

  $(document).off('.follow-unfollow')
    .on('ajax:success', '.js-followUnfollow', function (e, data) {
      $('.suggestionBoard.isHidden').slideDown();

      if ($(e.target).parent().hasClass('suggestion--action')) {
        $(e.target).parents('.suggestion').fadeOut("fast", function () {
          $(this).remove();
        });
      }

      $(this).toggleClass('isFollowing');
      $(this).text(data.refreshed_current);

      $(data.my_refreshed_container).find('a').text(data.my_refreshed_counter);
      if ($(data.his_refreshed_container).length > 0){
        $(data.his_refreshed_container).find('a').text(data.his_refreshed_counter);
      }
    });

  $(document).off('.skip-step').on('click.skip-step', '#skip-step', function (e) {
    e.preventDefault();
    $('#existing-contacts').fadeOut('slow');
    $('#mail-contacts-list').delay(600).fadeIn('slow');
  });

  $(document).off('.select-all').on('click.select-all', '#select-all', function (e) {
    e.preventDefault();
    var checkboxes = $('#mail-contacts-form').find('input[type="checkbox"]');
    checkboxes.prop('checked', !checkboxes.prop('checked'));
  });

  $(document).off('.sup-trigger').on('click.sup-trigger', '.sup-trigger', function (e) {
    e.preventDefault();
    $('.loginModal').modal('show');
    return false;
  });

  //SMOOTH SCROLL: http://css-tricks.com/snippets/jquery/smooth-scrolling/
  $(function() {
    $('a[href*=#]:not([href=#])').on('click', function(e) {
      e.preventDefault();
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });
  });

  if ($('.flash').length > 0) {
    setTimeout(function () {
      $('.flash').slideUp(400, function () {
        setTimeout ( function () { 
          $(this).remove();
        }, 1000);
      });
    }, 5000);
  }

  if ($('#updater-icon').length > 0) {

    var stepTitles = JSON.parse($('#scribe-updates').attr('data-titles')),
        stepContents = JSON.parse($('#scribe-updates').attr('data-contents')),
        tour = {
          id: 'scribe-updates',
          steps: [
            {
              title: '<div class="steve-scriber-thumb"></div><br/><div class="text-center">'+stepTitles[0]+'</div>',
              content: stepContents[0],
              target: 'updater-icon',
              placement: 'bottom',
              arrowOffset: 'center',
              xOffset: 'center',
              width: 350,
              padding: 10,
              showCloseButton: true
            }
          ],
          onEnd: function () {
            $('#updater-icon').addClass('zoomOutRight');
            setTimeout(function () {
              $.ajax({
                method: 'PUT',
                url: $('#scribe-updates').attr('data-finish'),
                success: function (data) {
                  $('#updater-icon').remove();
                }
              });
            }, 500);
          }
        };

    $('#updater-icon').on('click', function (e) {
      e.preventDefault();
      $(this).removeClass('tada');
      hopscotch.startTour(tour);
    });
  }

  if (localStorage.getItem('show-recommend') == null){
    localStorage.setItem('show-recommend', 0);
  }

  var rcount = Number(localStorage.getItem('show-recommend'));

  if ($('#recommendation').length > 0 && !window.mobileCheck()) {
    setTimeout(function () {

      var calloutMgr = hopscotch.getCalloutManager();
      calloutMgr.createCallout({
        title: $('.js-recommend').data('cout-title'),
        content: $('.js-recommend').data('cout-content'),
        id: 'recommend-text',
        target: 'recommendation',
        placement: 'right',
        yOffset: '-25px',
        arrowOffset: '25px'
      });

      setTimeout(function() {
        localStorage.setItem('show-recommend', ++rcount);
        calloutMgr.removeCallout('recommend-text');
      }, 5000);

    }, 1000);
  }

  var visitedTexts = Number(localStorage.getItem('visited-texts'));

  if ( visitedTexts >= 3 && $('.registrationModal').length > 0) {
    $('.registrationModal').modal({
      backdrop: 'static'
    });

    $('.link-account-icon').on('click', function() {
      localStorage.removeItem('visited-texts');
      _paq.push(['trackGoal', 4]);
      return true;
    });

    $('.registrationModal').on('hidden.bs.modal', function() {
      _paq.push(['trackGoal', 5]);
    });
  }

  $(document).off('.expand-container')
    .on('click.expand-container', '.js-expandContainer', function (e) {
      var $extensible = $(this).find('.extensibleContainer'),
        width = 30;


      if($extensible.hasClass('isOpened')) {
        $extensible.removeClass('isOpened');
      } else {
        _.each($extensible.children(), function (el) {
          width += $(el).outerWidth(true);
        });
        $extensible.addClass('isOpened');
      }

      $extensible.css({
        width: width,
        padding: "0 10px"
      });
    });

  $(document).off('.expandBio')
    .on('click.expandBio', '.js-expandBio', function (e) {
      var current = $(this).text(),
        next = $(this).data('expanded');

      $(this).parent().toggleClass('bioExpanded');
      $(this).toggleClass('isExpanded');
      $(this).text(next);
      $(this).data('expanded', current);
    });

  $(document).off('.slideContainer')
    .on('click.slideContainer', '.js-slideDownContainer', function (e) {
      var self = this,
        detailsContainer = $(this).parents('.textBlock--actionsContainer').next().find('.textBlock--detailsContainer'),
        shownElement = detailsContainer.children(':visible').get(0);

      if(shownElement) {
        if(shownElement.classList.contains('textBlock--'+self.dataset.container)) {
          detailsContainer.slideToggle("200", function () {
            $(this).find('.textBlock--' + self.dataset.container).fadeOut('fast');
          });
        } else {
          detailsContainer.children(':visible').fadeOut("fast");
          setTimeout(function () {
            detailsContainer.find('.textBlock--' + self.dataset.container).fadeIn('fast');
          }, 200);
        }

      } else {
        detailsContainer.slideToggle("200", function () {
          $(this).find('.textBlock--' + self.dataset.container).fadeIn('fast');
        });
      }

    });
}

$(document).ready(initApplication);
$(document).on('page:load', initApplication);
$(document).on('ajaxStart',   function() { NProgress.start(); });
$(document).on('ajaxComplete',  function() { NProgress.done(); });

window.mobileCheck = function() {
var check = false;
(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
return check; };