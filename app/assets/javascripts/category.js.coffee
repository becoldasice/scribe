ready = ->
	if window.localStorage.getItem('last-tab') == undefined || window.localStorage.getItem('last-tab') == null
		unless $('#categories-list').length == 0
			$('#categories-list').find('li').first().addClass 'active'
			$('.tab-pane').first().addClass 'in active'
	else
		unless $('#categories-list').length == 0
			$(window.localStorage.getItem('last-tab')).addClass 'in active'
			$("a[data-target=#{window.localStorage.getItem('last-tab')}]").parent().addClass 'active'

	$('.js-currentCategory').text $('#categories-list').find('.active').text()

	$(document).off '.expand'
	  .on 'click.expand', '[data-toggle="expand-container"]', (e) ->
	  	e.preventDefault()
	  	$(this).toggleClass 'expand'
	  	return

	$(document).off '.record-tab'
	  .on 'click.record-tab', '#categories-list > li > a', (e) ->
	  	window.localStorage.setItem 'last-tab', $(this).data('target')
	  	$('.js-currentCategory').text($(this).text())
	  	return

$(document).ready ready
$(document).on 'page:load', ready