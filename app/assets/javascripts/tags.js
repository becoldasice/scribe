function initTagsPage() {

  function loadTexts() {
    var $textsContainer = $('.tagsSection--textsContainer')
    , loadUrl = $('.tagsSection--search').attr('action')
    , $ajaxLoader = $textsContainer.find('.tagsSection--ajaxLoader')
    , $selectedListContainer = $('.tagsSection--selectedTagsList')
    , currentOccupier = $textsContainer.children('h2').length > 0 ? 'h2' : '.tagsSection--tagsTexts'
    , tags = $selectedListContainer.children().length > 0 ? _.map($selectedListContainer.children(), function (el) { return el.dataset.tag; }).join() : '';

    $textsContainer.find(currentOccupier).fadeOut('slow', function () {
      $(this).remove();
    });


    var last = $('.tagsSection--selectedTagsList').find('.tagsList--tag').last().get(0);
    if (last !== undefined) {
      $('.tagsSection--highlightedTag').text(last.dataset.tag);
    } else {
      $('.tagsSection--highlightedTag').text('tag');
    }

    $.ajax({
      method: 'GET',
      url: loadUrl,
      data: {tags: tags},
      beforeSend: function () {
        $ajaxLoader.show("fast");
      }
    }).done(function (data, status, jqxHR) {
      $(data).css({display: 'none'});
      $textsContainer.append(data);

      $ajaxLoader.hide('fast', function () {
        $(data).fadeIn('fast');
      });

    }).always(function (evt) {
      if ($ajaxLoader.is(':visible')) {
        $ajaxLoader.delay(400).hide('fast');
      }
    }).fail(function (jqXHR, status, errorThrown) {
      var error = document.createElement('h2');
      error.textContent = jqXHR.responseText;
      error.classList.add('u-centralized', 'u-lightFont');

      $ajaxLoader.hide('fast', function () {
        $textsContainer.append(error);
      });
    });
  }

  function createRemoveTag() {
    var remove = document.createElement('span');

    remove.classList.add('js-removeTag', 'u-lightFont', 'u-pullRight');
    $(remove).css({
      textAlign: 'right',
      padding: '0 5px',
      cursor: 'pointer'
    });

    remove.textContent = 'x';
    return remove;
  }

  function removeTag() {
    addOrRemoveFromCloud(this.parentNode.dataset.tag, false);
    $(this).parent().remove();
    loadTexts();
  }

  function addOrRemoveFromCloud(tag, add) {
    var cloud =  _.map($('.tagsList--tag.fromCloud'), function (el) { return el.textContent; });

    if (_.contains(cloud, tag)) {
      var matched = _.find($('.tagsList--tag.fromCloud'), function (el) { return el.textContent === tag; });
      if (add) {
        matched.classList.add('isSelected');
      } else {
        matched.classList.remove('isSelected');
      }
    }

  }

  $(document).off('.selectTagsFromCloud')
    .on('click.selectTagsFromCloud', '.js-selectTag', function (e) {
      if (this.classList.contains('isSelected')) {
        var selected = _.find($('.tagsSection--selectedTagsList').children(), function (el) { return el.dataset.tag === e.target.textContent; });
        removeTag.call(selected.querySelector('.js-removeTag'));
      } else {
        var $tag = $(e.target).clone();

        $tag.removeClass('js-selectTag')
          .removeClass('fromCloud');

        $tag.data('tag', $tag.text());
        $tag.append(createRemoveTag());

        $('.tagsSection--selectedTagsList').append($tag);
        e.target.classList.add('isSelected');
        loadTexts();
      }
    });

  $(document).off('.searchForTag')
    .on('submit.searchForTag', '.tagsSection--search', function (e) {
      e.preventDefault();
      var newTag = document.createElement('li')
      , newTagContent = $(e.target).find('#tag').val()
      , $selectedTagsContainer = $('.tagsSection--selectedTagsList');

      if (newTagContent.trim() !== '') {
        newTag.textContent = newTagContent;
        newTag.dataset.tag = newTagContent;
        newTag.classList.add('tagsList--tag', 'isSelected');

        newTag.appendChild(createRemoveTag());
        $selectedTagsContainer.append(newTag);

        addOrRemoveFromCloud(newTagContent, true);
      }

      loadTexts();
    });

  $(document).off('.removeTag')
    .on('click.removeTag', '.js-removeTag', removeTag);

  $('.js-selectTag').length > 0 ? $('.js-selectTag').first().click() : loadTexts();
}

$(document).ready(initTagsPage);
$(document).on('page:load', initTagsPage);