(function () {
  var Editable, ScribeSideComments, textInit,
    SideComments = require('side-comments'),
    TextEditor = null,
    __hasProp = {}.hasOwnProperty,
    __extends = function (child, parent) {
      var key;
      for (key in parent) {
        if (__hasProp.call(parent, key)) {
          child[key] = parent[key];
        }
      }

      function Ctor() {
        this.constructor = child;
      }

      Ctor.prototype = parent.prototype;
      child.prototype = new Ctor();
      child.__super__ = parent.prototype;
      return child;
    };

  ScribeSideComments = (function (_super) {
    __extends(ScribeSideComments, _super);

    function ScribeSideComments() {
      return ScribeSideComments.__super__.constructor.apply(this, arguments);
    }

    ScribeSideComments.prototype.sectionSelected = function () {
      ScribeSideComments.__super__.sectionSelected.apply(this, arguments);

      $(document).on('keyup.saveComment', '.comment-form .comment-box[contenteditable]', function (e) {
        window.localStorage.setItem('lastComment', $(this).text());
      });
    };

    ScribeSideComments.prototype.hideComments = function () {
      var $cBox;
      if (this.activeSection) {
        $cBox = this.activeSection.$el.find('.comment-box[contenteditable]');
        ScribeSideComments.__super__.hideComments.apply(this, arguments);

        if (window.localStorage.getItem('lastComment')) {
          $cBox.text(window.localStorage.getItem('lastComment'));
        }

        $(document).off('.saveComment');
        window.localStorage.removeItem('lastComment');
      }
    };

    return ScribeSideComments;

  })(SideComments);

  Editable = (function () {

    /*
        Default options for the Editable element
    */
    Editable.prototype.options = {
      defaultUpdateTime: 5000,
      titleMaxLength: 65,
      subtitleMaxLength: 100,
      mappedKeys: {
        BACKSPACE: 8,
        ENTER: 13,
        COMMA: 44,
        DELETE: 46,
        A: 65
      }
    };

    Editable.prototype.updater = null;

    /*
        Constructor. Creates a new Editable instance
        @param {object} text    Object containing all the information relevant to the current text
                                (Actions, values and containers)
    */
    function Editable(text) {
      this.text = text;
      this.initialize();
    };

    /*
        Sanitize and update all the values from the text: Title, Subtitle and Description
    */
    Editable.prototype.updateValues = function (update) {
      this.text.values.title = this.text.containers.title.text();
      this.text.values.subtitle = this.text.containers.subtitle.text();
    };

    /*
        Updates text without changing it's state (Published/Draft)
    */
    Editable.prototype.update = function (redirect_after) {
      var self = this,
        editor = CKEDITOR.instances.ck_editor_ta.element;
      self.updateValues(true);
      $.ajax({
        url: editor.$.form.action,
        data: {
          text: self.text.values
        },
        method: 'PUT'
      }).done(function (data, status, jqXHR) {
        if (redirect_after) {
          window.location.href = data.redirect_to;
        } else {
          self.text.actions.tagsForm.attr('action', data.tags_url);
        }
      }).error(function (jqXHR, status, errorThrown) {
        $('#update-notice').css({
          color: '#FC9F94'
        }).text(self.text.messages.updateError).fadeIn();

        setTimeout(function () {
          $('#update-notice').fadeOut().css({
            color: '#353838'
          }).text(self.text.messages.updateSuccess);
        }, 2000);
      });
    };

    /*
      Override default keydown event handler for the title and subtitle containers
      
      If the key pressed is enter or the current container reaches it's maximum allowed length, prevents default behaviour. 
      After reaching the characters limit, we must permit the user to press the following keys
      Backspace (8),
      Delete(46),
      Ctrl + A(65)

      @param {object} e           Jquery event object
      @param {object} current     Current container (Title or Subtitle)
      @param {Integer} maxLength  Container maximum allowed length
    */
    Editable.prototype.titleAndSubtitleKeydown = function (e, current, maxLength) {
      if ( e.which === this.options.mappedKeys.ENTER || (current.textContent.length >= maxLength && !_.has(_.invert(this.options.mappedKeys), e.which)) ) {
        e.preventDefault();
        if (current.textContent.length > maxLength) {
          current.textContent = current.textContent.substring(0, maxLength);
        }
      } else if (e.which === this.options.mappedKeys.BACKSPACE) {
        e.preventDefault();
        document.execCommand('delete', false);
      } else if (e.which === this.options.mappedKeys.DELETE ) {
        e.preventDefault();
        document.execCommand('forwardDelete', false);
      } else if (e.ctrlKey && e.which === this.options.mappedKeys.A) {
        e.preventDefault();
        document.execCommand('selectAll', false);
      }
    };

    /*
        Override default paste event handler for the title and subtitle containers.

    */
    Editable.prototype.titleAndSubtitlePaste = function (e, current, maxLength) {
      var clipboardData = e.originalEvent.clipboardData.getData('text'),
        clipboardDataLength = clipboardData.length,
        currentContentLength = current.textContent.length;

      if (currentContentLength === maxLength) {
        e.preventDefault();
      } else if (currentContentLength < maxLength && (clipboardDataLength + currentContentLength) > maxLength) {
        e.preventDefault();
        document.execCommand('insertText', false, clipboardData.substring(0, maxLength - currentContentLength));
      }
    };
    
    /*
        Updates the word counter for the current section

        @param {Integer} count 
    */
    Editable.prototype.updateWordsCounter = function (count) {
      $('#words-counter').text(count + " " + $('#words-counter').data('words'));
    };

    /*
        Shows user a modal informing upload error

        #TODO: Refactor function
    */
    Editable.prototype.uploadError = function () {
      $('#text-header').addClass('error-overlay');
      $('#upload-error').addClass('fade-in');

      setTimeout(function () {
        $('#text-header').removeClass('error-overlay');
        $('#upload-error').removeClass('fade-in');
      }, 2500);
    };

    /*
        Check files to for cover image before uploading

        @param {jQuery object} $container   Container where the preview image should be added
        @param {object} file                File to be uploaded
    */
    Editable.prototype.checkFiles = function ($container, file) {
      var validFile = file.type.match(/image\/(png|jpg|gif|jpeg)/gi).length > 0;

      if(validFile) {
        loadImage(file, function (img) {
          $container.find('img').attr('src', img.src);
        }, {
          noRevoke: true
        });
      } else {
          this.uploadError();
      }

      return validFile;
    };

    /*
        Prepare the adequate tag structure using data sent from the server

        @param {object} data    Server response containing tag information
    */
    Editable.prototype.prepareTagFromData = function (data) {
      var tagContainer = document.createElement('span'),
        tag = document.createElement('span'),
        deleteTag = document.createElement('span');

      tagContainer.classList.add('tag');

      deleteTag.classList.add('delete-tag');
      deleteTag.dataset.deleteUrl = data.url;
      deleteTag.dataset.confirm = data.confirm;
      deleteTag.textContent = 'x';

      tag.textContent = data.obj.name;

      tagContainer.appendChild(tag);
      tagContainer.appendChild(deleteTag);
      return tagContainer;
    };

    /*
        Rearrange tags position

        @param {Integer} to     New position for tags
    */
    Editable.prototype.rearrangeInput = function (to) {
      this.text.actions.tagsForm.find('#tag-name').css({
        left: to
      });
    };

    /*
        Rearrange tags container size

        @param {Integer} to     New size for tags container
    */
    Editable.prototype.rearrangeTagsContainer = function (to) {
      this.text.containers.tags.css({
        width: to
      });
    };

    /*
        Fucking ugly hack to fix overflow problem on tags container

        @param {jQuery object} $el  Element whose direction should be fixed
    */
    Editable.prototype.fixOverflowHach = function ($el) {
      el.css({
        direction: 'rtl'
      });

      setTimeout(function () {
        el.css({
          direction: ''
        });
      }, 10);

      $('#left, #right').data('target', 0);
    };

    /*
        Initialize the Editable object
    */
    Editable.prototype.initialize = function () {
      var self = this;

      if(self.text.containers.tags.find('.tag').length > 0) {
        var maxSum = 0;

        $.each(self.text.actions.tagsForm.find('.tag'), function (idx, el) {
          maxSum += $(el).outerWidth(true);
        });

        if (self.text.containers.tags.find('.tag').length === 5) {
          self.text.actions.tagsForm.find('#tag-name').attr('disabled', true);
        }
        self.rearrangeInput(maxSum);
        self.rearrangeTagsContainer(maxSum);
      }

      /*
          EVENTS
      */
      $(document).off('.title-event')
        .on({
          'keydown.title-event': function (e) {
            self.titleAndSubtitleKeydown(e, this, self.options[this.id+'MaxLength']);
          },
          'paste.title-event': function (e) {
            var result = self.titleAndSubtitlePaste(e, this, self.options[this.id+'MaxLength']);
            return result;
          },
          'keyup.title-event': function (e) {

            if(this.textContent.trim() !== '') {

              if(self.updater === null) {
                self.updater = setTimeout(function () {
                  self.update(false);
                  clearTimeout(self.updater);
                  self.updater = null;
                }, self.options.defaultUpdateTime);
              }
            }
          }
        }, '#title.editable, #subtitle.editable');

      $(document).off('.delete-image')
        .on('keydown.delete-image', function (e) {
          if(e.which === self.options.mappedKeys.BACKSPACE && $('.focused-figure').length > 0) {
            e.preventDefault();
          }
        });

      $(document).off('.set-category')
          .on('click.set-category', '#text-category > li > a', function (e) {
            e.preventDefault();
            self.text.values.category_id = $(e.target).data('value');
            $(e.target).parents('#text-category').parent().find('.description').text(e.target.textContent.substr(0,3).toUpperCase());

            if(self.text.containers.category.find('[data-selected]').length > 0 )
              delete self.text.containers.category.find('[data-selected]').get(0).dataset.selected;
            e.target.dataset.selected = true;
          });

      $(document).off('.set-language')
          .on('click.set-language', '#text-language > li > a', function (e) {
            e.preventDefault();
            self.text.values.language_id = $(e.target).data('value');
            $(e.target).parents('#text-language').parent().find('.description').text(e.target.textContent.substr(0,3).toUpperCase());

            if(self.text.containers.language.find('[data-selected]').length > 0)
              delete self.text.containers.language.find('[data-selected]').get(0).dataset.selected;
            e.target.dataset.selected = true;
          });

      self.text.containers.coverUploader.S3Uploader({
        remove_completed_progress_bar: true,
        remove_failed_progress_bar: true,
        before_add: function (file) {
          return self.checkFiles($('#text-header'), file);
        }
      });

      self.text.containers.coverUploader.on('s3_uploads_start', function (e) {
        $('#new-text-pic-req').removeClass('fade-in');
        setTimeout(function () {
          $('#nprogress').height('100px');
        }, 100);
      });

      self.text.containers.coverUploader.on('s3_upload_failed', function (e, content) {
        self.uploadError();
      });

      self.text.containers.coverUploader.on('s3_uploads_complete', function (e, content) {
        $('#text-header').removeClass('no-cover')
          .find('#cover-photo > img')
          .attr('src', content.url);
        self.update();
      });

      self.text.containers.coverUploader.parent().hover(function (e) {
        $('#new-text-pic-req').addClass('fade-in');
      }, function (e) {
        $('#new-text-pic-req').removeClass('fade-in');
      });

      self.text.containers.coverUploader.parent().click(function (e) {
        $('#new-text-pic-req').removeClass('fade-in');
        return true;
      });

      self.text.containers.tags.on('click', '.delete-tag', function (e) {
        e.stopPropagation();
        var toDelete = window.confirm($(this).data('confirm')),
          button = this;

        if(toDelete) {
          $.ajax({
            url: $(this).data('delete-url'),
            method: 'DELETE'
          }).done(function (data, status, jqXHR) {
            if(data) {
              var tgtWidth = $(button).parent().outerWidth(true),
                ipt = self.text.actions.tagsForm.find('#tag-name');

              self.rearrangeInput(ipt.position().left - tgtWidth);
              self.rearrangeTagsContainer(self.text.containers.tags.width() - tgtWidth);

              $(button).parent().fadeOut('fast', function () {
                $(button).parent().remove();
                if(self.text.actions.tagsForm.find('#tag-name').attr('disabled')) {
                  self.text.actions.tagsForm.find('#tag-name').attr('disabled', false);
                }
                self.text.actions.tagsForm.find('#tag-name').focus();
              });
            }
          });
        }
      });

      self.text.actions.tagsForm.find('#tag-name').on('click', function (e) {
        e.stopPropagation();
        e.preventDefault();

        //TODO: Implement redirection to tags page
      });

      self.text.actions.tagsForm.find('#tag-name').on('keypress', function (e) {
        if(e.which === self.options.mappedKeys.COMMA ) {
          self.text.actions.tagsForm.submit()
        } else if ( e.which !== self.options.mappedKeys.ENTER && e.target.value.length === 18) {
          //TODO: Elaborate way to show this limit to the user!
          return false;
        }
      });

      self.text.actions.tagsForm.on('ajax:before', function () {
        var proceed = true,
          $tagInput = self.text.actions.tagsForm.find('#tag-name'),
          error;

        if($tagInput.val().trim() === '') {
          error = document.createElement('span');
          error.textContent = $('#text-messages').data('empty-tag');
        }

        if(self.text.containers.tags.find('.tag').length > 0) {
          $.each(self.text.containers.tags.find('.tag'), function (idx, el) {
            if($tagInput.val() === $(el).find('span').text()) {
              error = document.createElement('span');
              error.textContent = self.text.messages.uniqueTag;
              return false;
            }
          });
        }

        if(error !== undefined) {
          proceed = false;
          error.classList.add('error-msg');

          self.text.actions.tagsForm.addClass('has-errors');
          self.text.actions.tagsForm.append(error);

          setTimeout(function () {
            $(error).fadeOut('fast', function () {
              self.text.actinos.removeClass('has-errors');
              $(error).remove();
            });
          }, 2000);
        }

        return proceed
      });

      self.text.actions.tagsForm.on('ajax:success', function (e, data) {
        var val = data.obj.name,
          tagContainer = self.prepareTagFromData(data),
          elWidth,
          iptLeft;

        self.text.containers.tags.append(tagContainer);

        elWidth = $(tagContainer).outerWidth(true);
        iptLeft = self.text.actions.tagsForm.find('#tag-name').position().left;

        self.rearrangeInput(iptLeft + elWidth);
        self.rearrangeTagsContainer(self.text.containers.tags.width() + elWidth);

        self.text.actions.tagsForm.find('#tag-name').val('');
        if(self.text.containers.tags.find('.tag').length === 5) {
          self.text.actions.tagsForm.find('#tag-name').attr('disabled', true);
          self.fixOverflowHack(self.text.actions.tagsForm);
        }
      });

      self.text.actions.tagsLeft.on('click', function (e) {
        var target = Number(this.dataset.target);

        if(target!== 0) {
          self.text.actions.tagsForm.find('.tags-container').css({
            left: "+=" + $(self.text.containers.tags.find('.tag')[(target-1)]).outerWidth(true) + "px"
          });
          $('#left, #right').attr('data-target', --target);
        } else {
          self.fixOverflowHack(self.text.actions.tagsForm);
        }
      });

      self.text.actions.tagsRight.on('click', function (e) {
        var target = Number(this.dataset.target);

        if(target!== 4 && self.text.containers.tags.find('.tag')[target] !== undefined) {
          self.text.actions.tagsForm.find('.tags-container').css({
            left: "-=" + $(self.text.containers.tags.find('.tag')[(target)]).outerWidth(true) + "px"
          });
          $('#left, #right').attr('data-target', ++tgt);
        }
      });

      $(document).off('.disabled-click')
        .on('click.disabled-click', 'a.cke_button_disabled', function (e) {
          e.preventDefault();
          e.stopPropagation();
          return false;
        });

      $(document).on('click', '#make-image.disabled', function (e) {
        e.preventDefault();
        e,stopPropagation();
        return false;
      });

      self.text.actions.hideHeader.on('click', function (e) {
        $(this).toggleClass('glyphicon-chevron-up, glyphicon-chevron-down');

        if(self.text.containers.commandsBar.hasClass('navbar-fixed-top')) {
          self.text.containers.commandsBar.removeClass('navbar-fixed-top');
        }
        $('#text-header').toggleClass('hide-header');

        if($('#text-header').hasClass('editor')) {
          $('#text-header').slideToggle('slow');
        } else {
          if($('#text-header').hasClass('hide-header') && !$('.scribeNavbar').hasClass('up')) {
            $('.scribeNavbar').addClass('up');
          }
        }
      });
    };

    return Editable;
  })();

  textInit = function () {
    var didScroll = false,
      lastScrollTop = 0,
      delta = 5,
      navbarHeight = $('.scribeNavbar').outerHeight(),
      hasScrolled = function () {
        var st = $(this).scrollTop();

        if(Math.abs(lastScrollTop - st) <= delta) {
          return;
        }

        if(st > lastScrollTop && st > navbarHeight) {
          $('.scribeNavbar').addClass('up');
        } else {
          if(st + $(window).height() < $(document).height()) {
            $('.scribeNavbar').removeClass('up');
          }
        }

        lastScrollTop = st;
      };

    $(window).off('.reader-scroll')
      .on('scroll.reader-scroll', function (e) {
        didScroll = true;
      });

    setInterval(function () {
      if(didScroll) {
        hasScrolled();
        didScroll = false;
      }
    }, 250);

    $(window).off('.window-scroller')
      .on('scroll.window-scroller', function (e) {
        var fromTop =  navigator.userAgent.match(/Firefox\/(\d+)/) ? $(document).scrollTop() : $('body').scrollTop();
        $('#cke_1_top').toggleClass('navbar-fixed-top textContainer', fromTop > 350);
        $('#cke_1_top').toggleClass('cke_reset_all', fromTop <= 350);
      });

    $.each($('#text').find('img'), function (idx, el) {
      if(el.dataset.width !== undefined && el.dataset.width.trim() !== '') {
        $(el).closest('.image-container').css({
          maxWidth: el.dataset.width + "px"
        });
      }
      if(el.dataset.deferedUrl !== undefined && el.dataset.deferedUrl.trim() !== '') {
        $.ajax({
          method: 'GET',
          url: el.dataset.deferedUrl,
        }).done(function (data, status, jqXHR) {
          el.src = el.dataset.deferedUrl;
        });
      }
    });

    if($('#text').length > 0) {
      $.each(
        $('#text').children().filter(function (idx, el) {
          var contents = $(el).find('.text-element').not('br');

          if($(el).contents().filter(function (idx, el) {
            if(el.nodeName.toUpperCase() !== 'BR' && el.textContent.trim() !== '') {
              return el;
            }
          }).length > 0) {
            return el
          }
        })
      , function (idx, el) {
        el.dataset.sectionId = idx + 1;
        el.classList.add('commentable-section');
      });

      $('.js-recommend').on('ajax:success', function (e, data) {
        $(this).parent().toggleClass('isRecommended')
        $('.js-recommend').text(data.status);
        $('.js-recommendation-count').text(data.counter);
        hopscotch.getCalloutManager().removeAllCallouts();
      });

      var currentUser = null,
        comments = null;

      $.ajax({
        method: 'GET',
        url: $('#text').data('commentline-load')
      }).done(function (data, status, jqXHR) {
        currentUser = data.current_user;
        comments = data.comments;

        var sideComments = new ScribeSideComments('#text', currentUser, comments);

        if(currentUser === null) {
          $(document).off('.add-comment')
            .on('click.add-comment', '.add-comment', function (e) {
              $('.loginModal').modal('show');
            });
        }

        sideComments.on('commentPosted', function (commentData) {
          commentData.comment = $('.side-comment.active').find('.comment-box[contenteditable]').text();
          
          if(commentData.comment.trim() !== '') {
            $.ajax({
              method: 'POST',
              url: $('#text').data('commentline-post'),
              data: {
                section: commentData.sectionId,
                comment: commentData.comment,
                parent: commentData.parentId
              }
            }).done(function (dt, st, jqX) {
              dt.parentId = dt.parent_id;
              sideComments.insertComment(dt);
            });
          }
        }).on('commentDeleted', function (comment) {
          $.ajax({
            method: 'DELETE',
            url: $('#text').data('commentline-delete'),
            data: {
              comment: comment.id
            }
          }).done(function (dt, st, jqX) {
            sideComments.removeComment(dt.section_id, dt.comment_id, dt.parent_id);
          });
        });
      });
    } else if($('#ck_editor_ta').length > 0) {

      TextEditor = new Editable({
        containers: {
          title: $('#title'),
          subtitle: $('#subtitle'),
          category: $('#text-category'),
          language: $('#text-language'),
          coverUploader: $('#text_image'),
          tags: $('#tags')
        },
        values: {
          id: $('#my_text').val(),
          title: $('#title').text(),
          subtitle: $('#subtitle').text(),
          description: $('#text').html(),
          category_id: $('#text-category').find('[data-selected]').data('value'),
          language_id: $('#text-language').find('[data-selected]').data('value')
        },
        actions: {
          hideHeader: $('#hide-header'),
          tagsForm: $('#tags-form'),
          tagsLeft: $('#left'),
          tagsRight: $('#right'),
          deleteTag: $('.delete-tag'),
          recommend: $('#recommend-btn')
        }
      });

    $('#title.editable').focus();
    }
  };

  $(document).ready(textInit);
  $(document).on('page:load', textInit);

}).call(this);