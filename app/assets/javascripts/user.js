function initUsersJS() {

  function setWhatsappMask() {
    $('#user_whatsapp_number').inputmask("+999999999999[9]");
  }

  function showCountryCode() {
    var $wNumberElement = $('#user_whatsapp_number'),
      $selectedCode = $('#user_phone_country_code').find(':selected');

    if ($wNumberElement.val().replace(/\+|_/g, '').trim() === '' && $selectedCode.val() !== '') {
      $wNumberElement.val($selectedCode.val());
      setWhatsappMask();
    }
  }

  function hideCountryCode() {
    var $wNumberElement = $('#user_whatsapp_number');
    if ($wNumberElement.val().replace(/\+|_/g, '').length === 2) {
      $wNumberElement.val('');
      setWhatsappMask();
    }
  }

  $(document).off('.changeCountryCode')
    .on('change.changeCountryCode', '#user_phone_country_code', showCountryCode);

  $(document).off('.whatsappNumberFocus')
    .on('focus.whatsappNumberFocus', '#user_whatsapp_number', showCountryCode);

  $(document).off('.whatsappNumberBlur')
    .on('blur.whatsappNumberBlur', '#user_whatsapp_number', hideCountryCode);

  $(document).off('.whatsappNumberHover')
    .on({
      'mouseenter.whatsappNumberHover': showCountryCode,
      'mouseleave.whatsappNumberHover': hideCountryCode
    }, '#user_whatsapp_number');

  $(document).off('.editUser')
    .on('submit.editUser', '.edit-form:not(.user-preferences)', function (e) {
      e.preventDefault();
      var $elf = $(this);
      $.ajax({
        method: 'PUT',
        url: this.dataset.updateUrl,
        data: $(this).serialize()
      }).done(function (data, status, jqXHR) {
        window.location = data.url;
      }).fail(function (jqXHR, status, errorThrown) {
        var errors = JSON.parse(jqXHR.responseText);

        _.forIn(errors, function (value, key, obj) {
          var error = document.createElement('small');


          _.each(value, function (el) {
            error.innerHTML += el + "<br/>";
          });

          error.style.display = 'block';
          error.style.color = 'red';

          $(error).insertAfter($elf.find('#user_' + key));
          setTimeout(function () {
            $(error).slideUp('slow', function () {
              $(this).remove();
            });
          }, 2500);
        });

      });

    });

  setWhatsappMask();
}

$(document).ready(initUsersJS);
$(document).on('page:load', initUsersJS);