(function () {
  "use strict";

  function execImage (file, instance) {
    loadImage(file, function (img) {
      formatImageFromSource(img.src, instance);
      instance.commands.save.disable();
      instance.commands.publishText.disable();
      instance.commands.deleteText.disable();
      instance.commands.addImage.disable();
    } , {
      noRevoke: true
    });

  };

  function formatImageFromSource(src, instance) {

    var figureContainerElement = new CKEDITOR.dom.element('div'),
      figureElement = new CKEDITOR.dom.element('figure'),
      figCaptionElement = new CKEDITOR.dom.element('figcaption'),
      imgContainerElement = new CKEDITOR.dom.element('div'),
      imgElement = new CKEDITOR.dom.element('img'),
      pMessageContainer = new CKEDITOR.dom.element('div'),
      sThumb = new CKEDITOR.dom.element('div'),
      pMessage = new CKEDITOR.dom.element('small'),
      pHolderImg = new CKEDITOR.dom.element('img');

    figureContainerElement.addClass('text-element')
      .addClass('contains-image')
      .addClass('full-show-parent')
      .setAttribute('contenteditable', false);

    figureElement.addClass('text-element')
      .addClass('text-center')
      .addClass('is-full-show')
      .setAttribute('contenteditable', false);

    figCaptionElement.addClass('text-element')
      .addClass('image-caption')
      .addClass('placeholdify')
      .setAttribute('data-placeholder', $('#text-messages').data('caption'))
      .setAttribute('contenteditable', true);

    imgContainerElement.addClass('text-element')
      .addClass('image-container')
      .setAttribute('contenteditable', false);

    imgElement.addClass('text-element')
      .addClass('placeholder-image')
      .setAttribute('src', src)
      .setAttribute('contenteditable', false)
      .setAttribute('id', '_preview');

    pHolderImg.setAttribute('src', src)
       .addClass('text-element')
       .addClass('placeholder-image');

    pMessageContainer.addClass('text-element')
      .addClass('preview-image');

    sThumb.addClass('text-element')
      .addClass('steve-scriber-thumb');

    pMessage.addClass('text-element')
      .addClass('text-center')
      .appendText($('#text-messages').data('processing-image'));
      
    pMessageContainer.append(sThumb);
    pMessageContainer.append(pMessage);

    $(figCaptionElement.$).off('.figcaption-events')
      .on({
        'keydown.figcaption-events': function (e) {
          if(e.which === 8 && this.textContent.trim() === '') {
            var el = figureContainerElement.getPrevious() || figureContainerElement.getNext(),
              newRange = instance.createRange();

            if(imgElement.$.id !== '_preview') {
              $(this).prev().addClass('focused-figure');
              el = imgElement;
              instance.focusManager.focus(true);
            }

            newRange.moveToPosition(el,CKEDITOR.POSITION_AFTER_START);
            e.preventDefault();
            e.stopPropagation();

          } else if(e.which === 46) {
            var rangeElement = instance.getSelection().getRanges()[0],
              rangeElementAncestor = rangeElement.getCommonAncestor(true, false),
              rangeElementLength = (rangeElementAncestor.getLength && rangeElementAncestor.getLength()) || rangeElement.endOffset,
              newRange = instance.createRange();

            if( (rangeElement.startOffset === rangeElement.endOffset) && rangeElement.endOffset === rangeElementLength) {
              newRange.moveToPosition(figureContainerElement.getNext(), CKEDITOR.POSITION_AFTER_START);
              e.preventDefault();
              e.stopPropagation();
            } else {
              newRange = null;
            }

          } else if(e.which === 13) {
            var newRange = instance.createRange();
            newRange.moveToPosition(figureContainerElement.getNext(), CKEDITOR.POSITION_AFTER_START);
          }

          if(newRange){
            instance.getSelection().selectRanges([newRange]);
            return false;
          }
        },
        'focus.figcaption-events': function (e) {
          // Remove zero-width spaces inserted  
          // by CKEditor when ranges are changed
          if(this.textContent.length === 1) {
            var code = this.textContent.charCodeAt(0);
            if(code === 8203) {
              this.innerHTML = ''
            }
          }
          $(this).removeClass('placeholdify');
        },
        'blur.figcaption-events': function (e) {
          if(this.textContent.trim() === '')
            $(this).addClass('placeholdify');
        }
      });

    pHolderImg.$.onload = function () {
      var imageWidth = this.naturalWidth;
      this.style.display = 'none';
      
      if (imageWidth < instance.container.$.clientWidth) {
        $(imgContainerElement.$).css({width: imageWidth});
        imgElement.$.dataset.width = imageWidth;
      }

      $(imgContainerElement.$).css({
        position: 'relative'
      });

      $(pMessageContainer.$).css({
        minWidth: "300px",
        width: "100%",
        position: "absolute",
        top: 0,
        height: imgElement.$.clientHeight,
        background: "rgba(0,0,0,.3)"
      });

      $(sThumb.$).css({
        margin: "0 auto"
      });

      if(imgContainerElement.$.clientHeight < 200) {        
        $(sThumb.$).css({
          display: "none"
        });
      } else {
        $(pMessageContainer.$).css({
          paddingTop: "5%"
        });
      }

      $(pMessage.$).css({
        margin: "0 auto",
        color: "white",
        paddingTop: "10px",
        width: "50%",
        fontSize: "85%",
        display: "block",
        textAlign: 'center'
      });

      imgContainerElement.append(pMessageContainer);
    };

    imgContainerElement.append(imgElement);
    imgContainerElement.append(pHolderImg);
    figureElement.append(imgContainerElement);
    figureElement.append(figCaptionElement);

    figureContainerElement.append(figureElement);

    // The config helper plugin add on listener to
    // the focus event and use it to clean 
    // the editor structure.
    // So we need to programatically set focus
    // on the editor before inserting images,
    // otherwise, the image will be removed
    // by the confighelper plugin event
    instance.focusManager.focus(true);
    instance.insertElement(figureContainerElement);

    pHolderImg.remove();
  };


  CKEDITOR.plugins.add("addImage", {
    init: function (editor) {

      editor.addCommand("addImage", {
        exec: function (instance) {          

          $('#insert_image').S3Uploader({
            remove_completed_progress_bar: true,
            remove_failed_progress_bar: true,
            before_add: function (file) {
              var validFile = file.type.match(/image\/(png|jpg|gif|jpeg)/gi).length > 0;
              if(validFile){
                execImage(file, instance);
                return true;
              } else {
                return false;
              }
            }
          });

          $('#insert_image').on({
            's3_upload_complete': function (e) {
              setTimeout(function () {
                NProgress.start();
              }, 100);
            },
            'ajax:success': function (e, data) {
              var imgPreview = $(instance.document.$).find('#_preview').get(0);

              imgPreview.dataset.image = data.id;

              $(imgPreview).siblings('.preview-image').remove();

              $.ajax({
                method: 'GET',
                url: data.url
              }).done(function (data, status, jqXHR) {
                //TODO: Find out if this can happen by any chance
                formatImageFromSource(data.url, instance);
                NProgress.done();

                instance.commands.save.enable();
                instance.commands.publishText.enable();
                instance.commands.deleteText.enable();
                instance.commands.addImage.enable();

              }).error(function (er, status, jqXHR) {

                imgPreview.dataset.deferedUrl = data.url;
                imgPreview.id = '';

                NProgress.done();

                instance.commands.save.enable();
                instance.commands.publishText.enable();
                instance.commands.deleteText.enable();
                instance.commands.addImage.enable();
              });

            }
          });
        }
      });

      editor.ui.addButton("AddImage", {
        label: "Image",
        command: "addImage",
        icon: null
      });
    }
  });
})();