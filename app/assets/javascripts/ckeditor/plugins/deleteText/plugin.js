(function () {
  "use strict";

  CKEDITOR.plugins.add("deleteText", {
    lang: "en,es,pt-br",
    version: 1,
    init: function (editor) {

      editor.addCommand("deleteText", {
        exec: function (instance) {
          var del = confirm(instance.element.$.dataset.deleteText);

          if(del) {
            $.ajax({
              url: instance.element.$.form.action,
              method: 'DELETE'
            }).done(function (data, status, jqXHR) {
              window.location.href = data.redirect_to;
            });
          }
        }
      });

      editor.ui.addButton("DeleteText", {
        label: editor.lang.deleteText.deleteText,
        command: "deleteText",
        icon: null
      });

    }
  })
})();

CKEDITOR.plugins.setLang('deleteText', 'en', {
    deleteText: "Delete"
});
CKEDITOR.plugins.setLang('deleteText', 'es', {
    deleteText: "Deletar"
});
CKEDITOR.plugins.setLang('deleteText', 'pt-br', {
    deleteText: "Deletar"
});