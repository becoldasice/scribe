(function () {
  "use strict";

  var publisher = {
    errors: []
  }

  function isValid(editor) {

    var isValid = true;
    if ($('#text-category').find('[data-selected]').data('value') === undefined || $('#text-category').find('[data-selected]').data('value').toString().trim() === ''){
      isValid = false;
      publisher.errors.push(editor.element.$.dataset.emptyCategory);
    } 

    if ($('#text-language').find('[data-selected]').data('value') === undefined || $('#text-language').find('[data-selected]').data('value').toString().trim() === ''){
      isValid = false;
      publisher.errors.push(editor.element.$.dataset.emptyLanguage);
    }

    if ($('#title').text() === undefined || $('#title').text().trim() === '') {
      isValid = false;
      publisher.errors.push(editor.element.$.dataset.emptyTitle);
    }

    if (editor.editable().$.classList.contains('placeholder') || editor.getData().trim() === '') {
      isValid = false;
      publisher.errors.push(editor.element.$.dataset.emptyDescription);
    }

    return isValid;
  }

  CKEDITOR.plugins.add("publisher", {
    lang: "en,es,pt-br",
    version: 1,
    init: function (editor) {

      editor.addCommand("publishText", {
        exec: function (instance) {

          if(isValid(instance)) {
            $.ajax({
              url: instance.element.$.dataset.publishUrl,
              data: {
                text: {
                  id: instance.element.$.dataset.textId,                  
                  description: instance.getData(),
                  title: $('#title').text(),
                  subtitle: $('#subtitle').text(),
                  category_id: $('#text-category').find('[data-selected]').data('value'),
                  language_id: $('#text-language').find('[data-selected]').data('value')
                }
              },
              method: 'POST'
            }).done(function (data, status, jqXHR) {
              if (data.redirect_url) {
                window.location.href = data.redirect_url;
              }
            });
          } else {
            publisher.errors.forEach(function (el, idx, array) {
              var error = document.createElement('p');
              error.textContent = el;
              $('#scribe-modal').find('.modal-body').append(error);
            });
            $('#scribe-modal').modal('show');

            $('#scribe-modal').on('hidden.bs.modal', function (e) {
              publisher.errors = [];
              $(this).find('.modal-body').empty();
            });
          }
        }
      });
      
      var draftMode = editor.element.$.dataset.draftMode,
        label = draftMode === "true" ? editor.lang.publisher.publishText : editor.lang.publisher.update;

      editor.ui.addButton("Publisher", {
        label: label,
        command: "publishText",
        icon: null
      });

    }
  })
})();

CKEDITOR.plugins.setLang('publisher', 'en', {
    publishText: "Publish",
    update: "Update"
});
CKEDITOR.plugins.setLang('publisher', 'es', {
    publishText: "Publicar",
    update: "Actualizar"
});
CKEDITOR.plugins.setLang('publisher', 'pt-br', {
    publishText: "Publicar",
    update: "Atualizar"
});