(function () {
  "use strict";

  CKEDITOR.plugins.add("addLink", {
    version: 1,
    init: function (editor) {

      editor.addCommand("AddLink", {
        exec: function (edt) {
          edt.focusManager.blur();
          var element = CKEDITOR.plugins.link.getSelectedLink(edt);

          if($('.link-container.opened').length > 0) {
            $('.link-container').remove();
          } else if(element !== null) {
            edt.document.$.execCommand('unlink');
          } else {

            var lContainer = document.createElement('div'),
              hyperlink = document.createElement('input');

            lContainer.classList.add('link-container', 'opened');
            
            hyperlink.id = 'hyperlink';
            hyperlink.type = 'text';
            hyperlink.placeholder = 'Link + Enter';

            lContainer.appendChild(hyperlink);

            $(document).off('.hyperlink-events')
              .on({
                'keyup.hyperlink-events': function (e) {
                  e.preventDefault();
                  e.stopPropagation();
                  if(e.which === 13) {
                    var link = this.value;

                    if(link.indexOf('http://') === -1 && link.indexOf('https://') === -1) {
                      link = 'http://' + link;
                    }

                    var style = new CKEDITOR.style({
                      element: 'a',
                      attributes: {
                        rel: 'nofollow',
                        target: '_blank',
                        href: link
                      }
                    });

                    style.type = CKEDITOR.STYLE_INLINE;
                    style.apply(edt.document);

                    $(this).blur();
                  }
                  return false;
                },
                'keydown.hyperlink-events': function (e) {
                  if(e.which === 13) {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                  } else if(e.which === 27) {
                    $('.link-container').remove();
                  }
                },
                'click.hyperlink-events': function (e) {
                  $('#hyperlink').focus();
                },
                'blur.hyperlink-events': function (e) {
                  $('.link-container').remove();
                }
              }, '#hyperlink');

            $(edt.element.$).siblings('#cke_ck_editor_ta').find('.cke_toolgroup').append(lContainer);
            $(hyperlink).focus();
          }
        }
      });

      editor.ui.addButton("AddLink", {
        label: "Link",
        command: "AddLink",
        icon: null
      });
    }
  })
})();