
var ready = function () {
  var currentInstance = CKEDITOR.instances["ck_editor_ta"];

  if(currentInstance) {

    var updater = null;

    function save (evt, redirect) {
      $.ajax({
        url: evt.editor.element.$.form.action,
        data: {
          text: {
            description: evt.editor.getData(),
            title: $('#title').text(),
            subtitle: $('#subtitle').text(),
            category_id: $('#text-category').find('[data-selected]').data('value'),
            language_id: $('#text-language').find('[data-selected]').data('value')
          }
        },
        dataType: 'JSON',
        method: 'PUT'
      }).done(function (data, status, jqXHR) {
        if(redirect)
          window.location.href = data.redirect_to;
      });
    }

    CKEDITOR.editorConfig = function ( config ) {
      config.extraPlugins = 'confighelper,wordcount,autogrow,deleteText,addLink,addImage,publisher';
      config.removePlugins = 'elementspath,magicline,liststyle,tabletools,contextmenu';
      config.extraAllowedContent = "div(text-element,image-container,contains-image,full-show-parent);" + 
        "figure(text-element,text-center,is-full-show);" +
        "figcaption(text-element,image-caption);" +
        "img(text-element)[!src,!data-image,data-width,data-defered-url,data-processed,data-pre-process];" +
        "a[href,target,rel]";

      config.autoGrow_onStartup = true;
      config.autoParagraph = false;

      config.defaultLanguage = 'en';
      config.toolbar = "scribeCommands";

      config.toolbar_scribeCommands = [
        ['Bold', 'Italic', 'AddLink', 'Blockquote', 'AddImage', "DeleteText", 'Save', 'Publisher']
      ];

      config.wordcount = {
        showWordCount: true
      };

      config.uiColor = "#FFFFFF";
      config.resize_enabled = false;

    };

    currentInstance.on("save", function (evt) {
      save(evt, true);
      return false;
    });

    currentInstance.on("doubleclick", function (evt) {
      var element = CKEDITOR.plugins.link.getSelectedLink(currentInstance) || evt.data.element;

      if(element.is('img')) {
        
      } else if(!element.isReadOnly()) {
        if(element.is('a')) {
          return false;
        }
      }
    });

    currentInstance.on('change', function (evt) {
      if(updater === null && $('#_preview').length === 0) {
        updater = setTimeout(function () {
          save(evt, false);
          clearTimeout(updater);
          updater = null;
        }, 5000);
      }
    });

    currentInstance.on('instanceReady', function (evt) {
      var doc = currentInstance.document.$,
        baskervile = document.createElement('link'),
        blockTags = ['p', 'div', 'blockquote'],
        hasImage = currentInstance.element.$.dataset.hasImage;

      baskervile.href = "https://fonts.googleapis.com/css?family=Libre+Baskerville:400,700,400italic";
      baskervile.rel = "stylesheet"

      currentInstance.document.$.head.appendChild(baskervile);

      if(hasImage !== undefined && hasImage != true) {
        $(doc).find('.contains-image')
          .add('.contains-image > figure')
          .add('figure > .image-container')
          .add('.image-container > img')
          .attr('contentEditable', false);

        $(doc).find('figure > figcaption')
          .attr('contentEditable', true);

        $($(doc).find('figcaption')).off('.figcaption-events')
          .on({
            'keydown.figcaption-events': function (e) {
              if(e.which === 8 && this.textContent.trim() === '') {
                var el = new CKEDITOR.dom.element($(this).closest('.contains-image').prev().get(0)) || new CKEDITOR.dom.element($(this).closest('.contains-image').next().get(0)),
                  newRange = currentInstance.createRange();

                if($(this).closest('.contains-image').find('img').get(0).id !== '_preview') {
                  $(this).prev().addClass('focused-figure');
                  el = new CKEDITOR.dom.element($(this).closest('.contains-image').find('img').get(0));
                  currentInstance.focusManager.focus(true);
                }

                newRange.moveToPosition(el,CKEDITOR.POSITION_AFTER_START);
                e.preventDefault();
                e.stopPropagation();

              } else if(e.which === 46) {
                var rangeElement = currentInstance.getSelection().getRanges()[0],
                  rangeElementAncestor = rangeElement.getCommonAncestor(true, false),
                  rangeElementLength = (rangeElementAncestor.getLength && rangeElementAncestor.getLength()) || rangeElement.endOffset,
                  newRange = currentInstance.createRange();

                if( (rangeElement.startOffset === rangeElement.endOffset) && rangeElement.endOffset === rangeElementLength) {
                  newRange.moveToPosition(new CKEDITOR.dom.element($(this).closest('.contains-image').next().get(0)), CKEDITOR.POSITION_AFTER_START);
                  e.preventDefault();
                  e.stopPropagation();
                } else {
                  newRange = null;
                }

              } else if(e.which === 13) {
                var newRange = currentInstance.createRange();
                newRange.moveToPosition(new CKEDITOR.dom.element($(this).closest('.contains-image').next().get(0)), CKEDITOR.POSITION_AFTER_START);
              }

              if(newRange){
                currentInstance.getSelection().selectRanges([newRange]);
                return false;
              }
            },
            'focus.figcaption-events': function (e) {
              // Remove zero-width spaces inserted  
              // by CKEditor when ranges are changed
              if(this.textContent.length === 1) {
                var code = this.textContent.charCodeAt(0);
                if(code === 8203) {
                  this.innerHTML = ''
                }
              }
              $(this).removeClass('placeholdify');
            },
            'blur.figcaption-events': function (e) {
              if(this.textContent.trim() === '')
                $(this).addClass('placeholdify');
            }
          });

        $.each($(doc).find('img.text-element'), function (idx, el) {
          if(el.dataset.width !== undefined && el.dataset.width.trim() !== '') {
            $(el).closest('.image-container').css({
              maxWidth: el.dataset.width + "px"
            });
          }

          if(el.dataset.deferedUrl !== undefined && el.dataset.deferedUrl.trim() !== '') {
            $.ajax({
              method: 'GET',
              url: el.dataset.deferedUrl,
            }).done(function (data, status, jqXHR) {
              el.src = el.dataset.deferedUrl;
              delete el.dataset.deferedUrl;
              el.id = '';
            });
          }
        });
      }

      delete currentInstance.element.$.dataset.hasImage

      $('.cke_button__addimage').append($('#insert_image'));
      $('.cke_button__addimage').attr('onclick', $('.cke_button__addimage').attr('onclick').replace('return false;', ''));

      $(doc).off('.img-click').on('click.img-click', 'img', function (e) {
        $('.focused-figure').removeClass('focused-figure');
        $(this).parent().addClass('focused-figure');
        return false;
      });

      for(var i = 0; i < blockTags.length; i++) {
        evt.editor.dataProcessor.writer.setRules(blockTags[i], {
          indent : false,
          breakBeforeOpen : false,
          breakAfterOpen : false,
          breakBeforeClose : false,
          breakAfterClose : false
        });
      }

      $(doc).off('.delete-image').on('keydown.delete-image', function (e) {
        var currentSel = currentInstance.getSelection(),
          currentElement = currentSel.getSelectedElement() || currentSel.getStartElement();
        if(e.which === 8 && $(currentElement.$).hasClass('contains-image')) {
          var newRng = currentInstance.createRange(),
            caption = new CKEDITOR.dom.element($(currentElement.$).find('figcaption').get(0));
          newRng.selectNodeContents(caption);
          currentSel.selectRanges([newRng]);
        }

        if( (e.which === 8 || e.which === 46) && $(doc).find('.focused-figure').length > 0 ) {
          
          e.preventDefault();
          var del = window.confirm($('#text-messages').data('delete-image'));
          if(del) {
            
            $.ajax({
              url: currentInstance.element.$.dataset.deleteImage,
              method: 'DELETE',
              data: {
                image_id: $(doc).find('.focused-figure').find('img').data('image')
              }
            });

            $(doc).find('.focused-figure').closest('.contains-image').remove();
          } else {
            var ckel = new CKEDITOR.dom.element($(doc).find('.focused-figure').closest('.contains-image').next().get(0)),
              rng = currentInstance.createRange(),
              sel = currentInstance.getSelection();

            $(doc).find('.focused-figure').removeClass('focused-figure');
            rng.moveToPosition(ckel, CKEDITOR.POSITION_AFTER_START);
            sel.selectRanges([rng]);
          }
          return false;
        }
      });

      $(doc).off('.focus-click').on('click.focus-click', function (e) {
        if($(doc).find('.focused-figure').length > 0) {
          $(doc).find('.focused-figure').removeClass('focused-figure');
        }
      });

    });

  }
  
};

$(document).ready( ready );
$(document).on("page:load", ready);