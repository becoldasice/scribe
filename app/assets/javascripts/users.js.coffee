ready = ->
    do ->
        $('.s3_uploader').each () ->
            $(this).S3Uploader {
                remove_completed_progress_bar: true,
                remove_failed_progress_bar: true
            }
            $(this).on 's3_upload_failed', (e, content) ->
                errorMsg = document.createElement 'div'
                errorMsg.id = "upload-error"
                errorMsg.style.color = "red"
                errorMsg.textContent = e.currentTarget.getAttribute('data-upload-error')
                $('#user-profile-img-header').append errorMsg
                setTimeout () ->
                    $('#upload-error').fadeOut "slow", () ->
                        $(this).remove()
                    return
                , 3500
                return
            $(this).on 's3_upload_complete', (e, data, status, xhr) ->
                if (e.target.id == 'profile_photo')
                    $('#profile-photo').attr('src', data.url)
                else
                    $("#cover-header").css {background: "url(#{data.url}) center center no-repeat transparent", backgroundSize: 'cover'}
                return
        return

    ajaxHandlers = {
        success:
            loadMore: (evt, data) ->
                container = $(data).find($(evt.currentTarget).data('container'))
                container.css({display: "none"})
                $(evt.currentTarget.parentNode).prev().append(container)
                container.show("fast")
                
                $(evt.currentTarget.parentNode).replaceWith($(data).find(evt.currentTarget.parentNode.dataset.container))
                return
    }
    
    $(document).off '.load'
      .on 'ajax:success.load', '.load-more:not([data-disabled])', (evt, data, status, xhr) ->
        ajaxHandlers.success.loadMore(evt, data)

    $(document).off '.deactivate-modal'
      .on 'click.deactivate-modal', '#deactivate-account', (e) ->
        e.preventDefault()

        confirmation = document.createElement('p')
        confirmation.textContent = $(this).data 'confirmation'
        action = $(this).data 'action'

        $('#scribe-modal').find('.modal-body').append confirmation
        $('#scribe-modal').modal 'show'
        $('#scribe-modal').find('.confirm-deactivation').attr 'data-action', action
        return

    $(document).off '.confirm-deactivation'
      .on 'click.confirm-deactivation', '.confirm-deactivation', (e) ->
        $.ajax {
            url: $(this).data('action'),
            method: "POST",
            success: (data) ->
                window.location.href = data.redirect_url
        }

    $(document).off '.tabs'
      .on 'click.tabs', 'a[data-toggle="tab"]', (e) ->
        e.preventDefault()
        $(this).tab('show')
        $(this).parents('li').addClass 'active'
        return

    $(document).off '.pre-rec-details'
      .on 'ajax:before.pre-rec-details', '.recommendation-details', (e) ->
        if $(this).attr('href') == ''
            e.preventDefault()
            e.stopPropagation()
            return false
        else
            return true

    $(document).off '.rec-details'
      .on 'ajax:success.rec-details', '.recommendation-details', (e, data) ->
        $('#scribe-modal').find('.modal-body').append data
        $('#scribe-modal').find('.modal-content').attr 'id', 'recommendations-modal'

        $('#scribe-modal').on 'hidden.bs.modal', (e) ->
            $('#scribe-modal').find('.modal-body').empty()
            return

        $('#scribe-modal').modal 'show'
        return

    $('#cover-header').find('.upload-btn').hover (e) ->
        $('#profile-pics-requirements').addClass 'fade-in'
        return
    , (e) ->
        $('#profile-pics-requirements').removeClass 'fade-in'
        return

    if $('#merge-acc-modal').length > 0
        $('#merge-acc-modal').modal {
            backdrop: 'static'
            keyboard: false
        }
        $('#merge-acc-modal').find('.user-grid-container').find('a').attr('target', '_blank')
        $(document).off '.merge-accounts'
            .on 'click.merge-accounts', '.merge-accounts', (e) ->
                $.ajax {
                    url: $(this).data('merge-url')
                    method: "POST"
                    success: (data) ->
                        window.location = window.location.pathname
                        return
                }

    $(document).off '.unlink-account'
        .on 'ajax:success.unlink-account', '.unlink-account', (evt, data) ->
            window.location = window.location.pathname
            return

    return

$(document).ready ready
$(document).on 'page:load', ready