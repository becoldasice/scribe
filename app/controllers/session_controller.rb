class SessionController < ApplicationController

  before_filter only: [:create] do
    render_header(true)
    render_footer(true)
  end
  
  ## GET /login
  ## Params: 
  ## Login page
  def new
    redirect_to root_url
  end

  ## POST /authenticate
  ## Params: email, password
  ## Authenticates user based on email and password. If invalid, shows error. If valid, log user in
  ## Success: redirect_to user_show_url(user)
  ## Error: Renders flash message
  ## TODO: Review method
  def create
  	user = User.authenticate(params[:email], params[:password])
    
    respond_to do |format|
    	if user
    		session[:user_id] = user.id
        if params[:persist_session]
          set_cookies_for(user)
        end
    		format.json { render json: {url: timeline_url}}
    	else
    		format.json { render json: {error: t(:invalid_e_or_p)}, status: :unprocessable_entity}
    	end
    end
  end

  ## GET /(facebook | twitter | instagram)_auth
  ## Params: 
  ## Authenticates user using facebook, twitter or instagram
  %w(facebook twitter instagram).each do |service|
    define_method("#{service}_auth") do
      if @current_user
        User.send("link_#{service}", @current_user, auth_hash)

        respond_to do |format|
          flash[:notice] = t(:linked_with_success)
          format.html { redirect_to edit_user_url(@current_user) }
        end
      else
        user = User.send("#{service}_auth", auth_hash)
        
        respond_to do |format|
          if user.is_active
            session[:user_id] = user.id
            set_cookies_for(user)
          else
            session[:provider] = service
            session[:social_uid] = user.send("#{service}_uid")
          end
          format.html { redirect_to (request.referrer || root_url)}
        end
      end
    end
  end

  ## GET /logout
  ## Params: 
  ## User logout, destroys current session
  def destroy
  	session[:user_id] = nil
    cookies.delete :_rm_i, domain: :all
    cookies.delete :_rm_k, domain: :all
  	redirect_to root_url
  end

  private

  def auth_hash
    request.env['omniauth.auth']
  end

  def set_cookies_for(user)
    identifier = user.id
    value = Digest::SHA1.hexdigest(user.created_at.to_s)[6,10]

    cookies['_rm_i'] = {:value => identifier, :expires => 30.days.from_now, domain: :all}
    cookies['_rm_k'] = {:value => value, :expires => 30.days.from_now, domain: :all}
  end

end
