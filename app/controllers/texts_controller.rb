class TextsController < ApplicationController

  before_action :set_text, only: [:show, :edit, :update, :destroy, :create_tag,
      :delete_tag, :save_text_cover, :save_text_image, :delete_text_image]
  before_action :check_credentials, only: [:edit, :update, :destroy, :new]
  before_action :check_ownership, only: [:edit, :destroy]
  before_action only: [:new, :edit] do
    render_footer(false)
    render_header(false)
  end
  before_action only: [:index, :show] do
    render_header(true)
    render_footer(false)
  end

  helper_method :text_style


  ## GET /t
  ## Unused rails generated method
  ## TODO: Define usage
  def index
    @texts = Text.all
  end

  ## GET /t/new/[id]
  ## Params: [id]
  ## Success: render new text page
  ## Error: 
  ## TODO: Error handling
  ## TODO: Find smarter way to create or retrieve drafts to avoid Database garbage (Using id as a parameter)
  ## Rails generated method
  def new
    @text = @current_user.texts.create

    respond_to do |format|
      format.html { redirect_to edit_text_url(@text) }
    end
  end

  # GET /t/id/edit
  def edit
  end

  ## GET /t/id
  ## Params: id
  ## Success:
  ##    - If user is logged and is the text owner, render it!
  ##    - If user is logged and isn't the text owner/If user isn't logged in:
  ##        - If text is draft, redirect_to user profile
  ##        - If text isn't draft, render it!
  ## Error: 
  ## TODO: Error handling
  ## TODO: Rewrite success flow
  ## Rails generated method
  def show
    if @text.is_draft && @text.user != @current_user
      redirect_to root_url
    else
      impressionist(@text) unless @text.is_draft 
      logger.debug "Impressions: #{@text.impressionist_count(filter: :ip_address)}"
      @suggestions = Text.generate_suggestions(2, @text.id)
      respond_to do |format|
        format.html {render layout: !request.xhr?}
      end
    end
  end

  ## PUT /t/id
  ## Params: id, title, sub_title, description, category_id, language_id
  ## Success: redirect_to text page
  ## Error: Render models errors
  ## TODO: Error handling
  ## Rails generated method
  def update
    respond_to do |format|
      if @text.update(text_params)
        format.json { render json: {saved: true, redirect_to: text_url(@text), publish: text_publish_url(@text),tags_url: url_for(create_tag_path(@text))}, status: :ok, location: @text }
      else
        format.json { render json: @text.errors, status: :unprocessable_entity }
      end
    end
  end

  ## DELETE /t/id
  ## Params: id
  ## Success: redirect_to user profile
  ## Error: 
  ## TODO: Error handling
  ## TODO: Rewrite code structure so text doesn't get ACTUALLY deleted from database
  def destroy
    @text.destroy
    respond_to do |format|
      format.html { redirect_to user_show_path(@current_user)} unless request.xhr?
      format.json { render json: {redirect_to: user_show_url(@current_user)}, status: :ok}
    end
  end

  ## POST /cover_text_photo
  ## Params: text_cover
  ## Success: Return text_cover url
  ## Error: Renders model errors
  ## TODO: Error handling
  def save_text_cover
    unless @text.cover.nil?
      @text.text_images.find(@text.cover.id).destroy
    end
    @text.text_images.create({deferred_photo: params[:text][:text_cover], is_cover: true})
    respond_to do |format|
      if @text.save
        format.json {render json: @text.cover.photo.url, status: :ok}
      else
        format.json {render json: @text.errors, status: :unprocessable_entity}
      end
    end
  end

  ## POST /text_image
  ## Params: text_image
  ## Success: Return text_image url
  ## Error: Renders model errors
  ## TODO: Error handling
  def save_text_image
    @text.text_images.create({deferred_photo: params[:text][:text_image], is_cover: false})

    respond_to do |format|
      if @text.save
        format.json {render json: {url: @text.text_images.last.photo.url(:full_show), id: @text.text_images.last.id}, status: :ok}
      else
        format.json {render json: @text.errors, status: :unprocessable_entity}
      end
    end
  end

  ## DELETE /text_image
  ## Params: image_id
  ## Success: Delete text image
  ## Error: Renders model errors
  ## TODO: Error handling
  def delete_text_image
    @img = @text.text_images.find(params[:image_id])

    respond_to do |format|
      if @img.destroy!
        format.json {render json: {destroyed: true}, status: :ok}
      else
        format.json {render json: @text.errors, status: :unprocessable_entity}
      end
    end
  end

  ## POST /t/id/publish
  ## Params: id
  ## Success: Change text from draft status and redirect to text show
  ## Error: Renders model errors
  ## TODO: Error handling
  ## TODO: Review slug generation in the proccess
  def publish
    @text = Text.friendly.find(params[:text][:id])
    @text.slug = nil

    respond_to do |format|
      if @text.update(text_params) && @text.publish
        format.json {render json: {redirect_url: text_url(@text)}, status: :ok, location: @text}
      else
        format.json {render json: @text.errors, status: :unprocessable_entity}
      end
    end
  end

  ## POST /id/create_tag
  ## Params: id, name
  ## Success: Create or retrieve tag for text
  ## Error: Returns uniqueness/limits error message
  def create_tag
    respond_to do |format|
      @tag = @text.insert_tag(tags_params)
      if @tag
        format.json {render json: {obj: @tag, url: delete_tag_url(@text,@tag), confirm: I18n.t(:exclusion_confirm)} , status: :ok}
      else
        format.json {render json: t(:tags_num_reached) , status: :unprocessable_entity}
      end
    end
  end

  ## GET /t/id/commentline
  ## Params: id
  ## Success: Retrieve text commentline and current user
  def commentline
    text = Text.friendly.find(params[:text_id])
    redirect_to text_url(text) unless request.xhr?
    if request.xhr?
      @commentline = {
        current_user: nil,
        comments: []
      }

      if @current_user
        @commentline[:current_user] = {
          id: @current_user.id,
          name: @current_user.name,
          avatarUrl: @current_user.profile_photo.url(:thumbs),
          authorUrl: user_show_url(@current_user)
        }
      end

      if text.comment_count > 0
        text.commentline(limit: 999).group_by{
          |c|
          c.section_id
        }.each {
          |s_id, comments|

          @commentline[:comments] << {
            sectionId: s_id.to_s,
            comments: []
          }

          comments.sort_by{
            |c|
            c.id
          }.each {
            |comment|
            json_comment = {
              authorAvatarUrl: comment.user.profile_photo.url(:thumbs),
              authorName: comment.user.name,
              comment: comment.body,
              authorId: comment.user.id,
              id: comment.id,
              authorUrl: user_show_url(comment.user)
            }

            if comment.parent_id.blank?
              json_comment[:comment] = '' if comment.deleted
              json_comment[:replies] = []
              json_comment[:deleted] = comment.deleted
              
              @commentline[:comments].last[:comments] << json_comment
            else
              json_comment[:parentId] = comment.parent_id
              parent = @commentline[:comments].last[:comments].find {
                |c|
                c[:id] == comment.parent_id
              }
              parent[:replies] << json_comment
            end
          }
        }
      end

      respond_to do |format|
        format.json {render json: @commentline, success: :ok}
      end
    end
  end

  ## POST /t/id/post_comment
  ## Params: id, section, comment, parent
  ## Success: Add comment to commentline
  def post_comment
    text = Text.friendly.find(params[:text_id])
    
    comment = {
      commentable: text,
      body: params[:comment],
      section_id: params[:section],
      parent_id: params[:parent]
    }

    @comment = @current_user.post_comment(comment)
    respond_to do |format|
      format.json {render json: @comment.to_json, status: :ok}
    end
  end

  ## DELETE /t/id/delete_comment
  ## Params: id, section, comment
  ## Success: Add comment to commentline
  def delete_comment
    @comment = @current_user.comments.where(id: params[:comment]).first
    @comment_id = @comment.id
    @section_id = @comment.section_id
    @parent_id = @comment.parent_id
    
    text = @comment.commentable

    is_parent = text.comments.where(parent_id: @comment_id)

    if is_parent.blank?
      @comment.destroy
    else
      @comment.update({
        deleted: true
      })
    end
    respond_to do |format|
      format.json {render json: {comment_id: @comment_id, section_id: @section_id, parent_id: @parent_id}, status: :ok}
    end
  end

  ## DELETE /id/delete_tag/tag_id
  ## Params: id, tag_id
  ## Success: Delete tag from text
  ## Error: 
  ## TODO: Error handling
  def delete_tag
    respond_to do |format|
      format.json {render json: @text.tags.delete(params[:tag_id])}
    end
  end

  ## POST /id/recommendations_details
  ## Params: id
  ## Success: Returns list of users who recommended the text
  def recommendations_details
    @text = Text.friendly.find(params[:text_id])

    respond_to do |format|
      format.html { render partial: "users/user_list", locals: {users: @text.recommenders}, status: :ok }
    end
  end

  private

  ## Callback helpers
  def set_text
    param = params[:id] || params[:text_id]
    @text ||= Text.friendly.find(param)
  end

  ## Meta tags helpers
  def facebook_meta
    prev = super
    if !@text.nil?
      prev[:og][:type] = "article"
      prev[:og][:title] = @text.title
      prev[:og][:url] = url_for(@text)
      prev[:og][:image].unshift(@text.user.profile_photo.url) unless @text.user.profile_photo.blank?
      prev[:og][:image].unshift(@text.cover.photo.url) unless @text.cover.blank?
      prev[:og][:description] = Sanitize.clean(@text.description).truncate(100, omission: '...') unless @text.description.blank?
      prev[:fb] = {
        app_id: 330793013745555
      }
      prev[:article] = {
        publisher: "https://www.facebook.com/wescribeapp",
        author: [@text.user.facebook_uid]
      }
    end
    prev
  end

  def twitter_meta
    prev = super
    if !@text.nil?
      if !@text.cover.blank?
        prev[:twitter][:card] = "summary_large_image"
        prev[:twitter][:image][:src] = @text.cover.photo.url
      else
        prev[:twitter][:image][:src] = @text.user.profile_photo.url
      end
      prev[:twitter][:title] = @text.title
      prev[:twitter][:description] = Sanitize.clean(@text.description) unless @text.description.blank?
      prev[:twitter][:url] = url_for(@text)
    end
    prev
  end

  def default_meta
    prev = super
    if !@text.nil?
      prev[:title] = @text.title.blank? ? "Scribe" : "#{Sanitize.clean(@text.title)} - Scribe"
      prev[:description] = Sanitize.clean(@text.description) unless @text.description.blank?
    end
    prev
  end

  ## Auth helpers
  def check_ownership
    if @text.user != @current_user
      respond_to do |format|
        format.html {redirect_to @text}
      end
    end
  end

  ## Params helpers
  def tags_params
    params.require(:tag).permit(:name)
  end

  def text_params
    params.require(:text).permit(:title, :subtitle, :description, :user_id, :category_id, :language_id, :tag)
  end

  ## HTML Classes helpers
  def text_style(txt, edit_page = false)
    image = ''
    repeat = ''
    bg_size = ''
    unless @text.cover.blank?
      if edit_page
        image << "url(#{view_context.image_url('gradiente_branco_reverse.png')}),"
        repeat << 'repeat-x,'
        bg_size << 'contain,'
      end
      
      image << "url(#{txt.cover.photo.url})"
      repeat << 'no-repeat'
      bg_size << 'cover'
    end
    style = "background-image: #{image}; background-position: center center; background-repeat: #{repeat}; background-size: #{bg_size};"
  end
end