class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :container_classes, :facebook_meta, :default_meta, :twitter_meta,
                :fol_container_date,
                :follow_unfollow_button, :generate_locale_url, :text_date,
                :text_block_classes, :mobile_request?

  before_filter :set_locale, :prepare_user
  
  before_action do
    render_footer(true)
    render_header(true)
  end

  def four_oh_four
    @text_suggestions = Text.generate_suggestions(2)
  end

  ## GET /search/query
  ## Params: query page per
  ## Success: Render JSON search results
  def search
    @query = params[:query]
    @user = @current_user || User.new

    @search_params = params[:query].blank? ? nil : params[:query]

    %w(texts_page users_page).each do |param|
      instance_variable_set("@#{param}", (params[param.to_sym].blank? || params[param.to_sym].to_i == 0) ? 1 : params[param.to_sym].to_i)
    end

    @user_results = User.search_for({
      fields: ['bio', 'first_name^2', 'last_name^1.5'],
      filter: {bool: {must: {term: {is_active: true}}}}
      },@search_params)
      .page(@users_page)
      .per(10)
      .records

    @text_results = Text.search_for({
      fields: ['title^3', 'subtitle^2', 'tags.name^3', 'description'],
      filter: {bool: {must: {term: {is_draft: false}}}}
      },@search_params)
      .page(@texts_page)
      .per(10)
      .records

    respond_to do |format|
      format.html
    end
  end

  def browse_by
    @type = params[:type]
    @user = @current_user || User.new

    if @type.eql?("discover")
      @texts = Text.generate_suggestions(150)
        .page(params[:page].blank? ? 1 : params[:page].to_i)
        .per(10)
    elsif @type.eql?("popular")
      @texts = Text.where(language: Language.where(locale: I18n.locale).first)
        .order("recommendations_count DESC")
        .page(params[:page].blank? ? 1 : params[:page].to_i)
        .per(10)
        .uniq
    end

    respond_to do |format|
      format.html
    end
  end

  def landing
    redirect_to timeline_url if @current_user
    unless @current_user
      @user = User.new
      @categories = Category.all
      @highlight = Text.where({is_draft: false, language: Language.where(locale: I18n.locale).first})
        .order("recommendations_count DESC")
        .limit(10)
        .to_a
        .sample

      respond_to do |format|
        format.html
      end
    end
  end

  ## POST /shortener/text
  ## Params: text
  ## Shorten text URL for twitter sharing
  ## Success: Returns shortened URL JSON formatted
  def shorten_url
    text = Text.find(params[:text])
    respond_to do |format|
      format.json {render json: {url: Bitly.client.shorten(url_for(text)).short_url}, status: :ok}
    end
  end

  ## POST contacts/:provider/callback
  ## Params: provider
  ## Fetches user contacts list from one of the following providers: Hotmail, Gmail or Yahoo!
  ## Success: List existing scruibe users from contacts and contacts to invite
  def contacts_callback
    @user = @current_user
    @contacts = request.env['omnicontacts.contacts']
    @on_scribe = User.friends_by_email(@contacts).to_a

    unless @on_scribe.blank?
      @contacts.reject! {
        |h|
        @on_scribe.map{ |u| u.email }.include? h[:email]
      }

      @on_scribe.reject! {
        |u|
        @user.follow?(u) || (u if u.id.eql?(@current_user.id))
      }
    end

    respond_to do |format|
      format.html
    end
  end

  private

  def current_user
    @current_user ||= login_from_session || login_from_cookie
  end

  def login_from_session
    User.find(session[:user_id]) unless session[:user_id].blank?
  end

  def login_from_cookie
    unless cookies['_rm_i'].blank?
      user = User.find(cookies['_rm_i'])
      if cookies['_rm_k'] == Digest::SHA1.hexdigest(user.created_at.to_s)[6,10]
        user.update({
          last_login: Time.now,
          prefered_language: I18n.locale
        })
        user
      else
        nil
      end
    end
  end

  ## Auth helpers

  def check_credentials
    if (@current_user.nil?)
      respond_to do |format|
        format.html {redirect_to root_url, notice: t(:need_login)}
      end
    end
  end

  ## Locale helpers

  def set_locale
    I18n.locale = extract_locale_from_subdomain || I18n.default_locale
    @current_user.update({prefered_language: I18n.locale}) if @current_user
  end

  def prepare_user
    @user ||= User.new
  end


  def extract_locale_from_subdomain
    parsed_locale = request.subdomains.first
    I18n.available_locales.map(&:to_s).include?(parsed_locale) ? parsed_locale : nil
  end

  ## Meta tags helpers

  def facebook_meta
    {
      og: {
        type: "website",
        title: "Scribe",
        site_name: "Scribe",
        url: url_for(root_url),
        description: t(:social_description),
        image: [view_context.image_url('covers/facebook/facebook-cover.jpg')]
      }
    }
  end

  def default_meta
    {
      title: "Scribe",
      description: t(:social_description)
    }
  end

  def twitter_meta
    {
      twitter: {
        card: "summary",
        site: "@wescribeapp",
        title: "Scribe",
        description: t(:social_description),
        url: url_for(root_url),
        image: {
          src: view_context.image_url('covers/facebook/facebook-cover.jpg')
        }
      }
    }
  end

  ## Blocks render helpers

  def render_footer(bool)
    @render_footer = bool
  end

  def render_header(bool)
    @render_header = bool
  end

  ## HTML Classes helpers

  def text_block_classes(has_gutter, index, size)
    classes = ["is#{size.capitalize}Sized"]
    if has_gutter && size.eql?("half")
      classes << (index % 2 == 0 ? "scribeGrid--gutterRight1 scribeGrid--mobile-noGutterRight" : "scribeGrid--gutterLeft1 scribeGrid--mobile-noGutterLeft" )
    end
    classes
  end

  def container_classes
    classes = ''
    if !@text.nil? && request.fullpath.include?(edit_text_path(@text))
      classes  << 'is-editor reader-page'
    end
    if (params[:controller].eql?("texts") && params[:action].eql?("show"))
      classes << 'reader-page'
    end
    classes
  end

  def fol_container_date(user,text)
    time_used = text.blank? ? Time.now : text.published_at
    day = Haml::Engine.new("%h2.day #{time_used.strftime('%d')}").render
    month = Haml::Engine.new("%h5.month #{I18n.l(time_used, format: :short_month).upcase}").render
    year = Haml::Engine.new("%h5.year #{time_used.strftime('%Y')}").render

    day + month + year
  end

  def follow_unfollow_button(user)
    if @current_user.blank?
      view_context.link_to(
        t(:follow_or_unfollow, count: 0),
        '#',
        class: 'socialBtn socialBtn--followBtn followIcons sup-trigger'
      )
    else
      is_following = @current_user.follow?(user)
      current_state = t(:follow_or_unfollow, count: (is_following ? 1 : 0))
      hover_state = t(:follow_or_unfollow, count: (is_following ? 2 : 0))
      fob_class = is_following ? 'isFollowing' : ''
      view_context.link_to(
        current_state,
        follow_or_unfollow_url(user), 
        method: :post, 
        remote: true, 
        class: "socialBtn socialBtn--followBtn followIcons js-followUnfollow #{fob_class}",
      ) unless user == @current_user
    end
  end

  def generate_locale_url(locale)
    case locale
    when "en"
      "#{Rails.application.config.en_locale_url}#{request.env['REQUEST_URI']}"
    when "es"
      "#{Rails.application.config.es_locale_url}#{request.env['REQUEST_URI']}"
    else
      "#{Rails.application.config.default_locale_url}#{request.env['REQUEST_URI']}"
    end
  end

  def text_date(text, format)
    date = text.published_at.blank? ? text.updated_at : text.published_at

    if format.eql?(:short_month)
      return I18n.l(date, format: format)
    end

    date.strftime(format)
  end

  def mobile_request?
    /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.match(request.user_agent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.match(request.user_agent[0..3])
  end
end
