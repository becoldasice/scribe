
## Rails generated class, currently unused

class CategoriesController < ApplicationController
  before_action :define_locale
  before_action :set_details, only: [:last_texts_full, :most_viewed_full]

  helper_method :categories_header_attrs

  ## GET /c
  ## Load categories page with it's subdivisions
  def index
    @categories = Category.includes(:texts).all
  end

  ## GET /c/:locale/last_texts/:page
  ## Load last 10 texts

  ## GET /c/:locale/most_viewed/:page
  ## Load 10 most viewed texts
  %w(last_texts most_viewed).each do |type|
    define_method("#{type}_full") do
      @texts = @category.list_texts_by_type(type, @locale, @page, 10)
      respond_to do |format|
        format.html { render "detailed_category", locals: {title: I18n.t(type.to_sym), action: "#{type}_full".to_sym} }
      end
    end
  end

  def load_from
    @texts = Text.where({category_id: params[:category], is_draft: false, language: Language.where(locale: I18n.locale).first})
      .where('recommendations_count > (?)', 9)
      .limit(params[:limit])
      .order("RANDOM()")

    respond_to do |format|
      format.html {render partial: "application/text/blocks", locals: {texts: @texts, container_class: 'landingPage--categoryTexts'}}
    end
  end

  private

    def default_meta
      prev = super
      prev[:title] = "#{t(:categories)} - Scribe"
      prev
    end

    def categories_header_attrs(cat)
      attrs = {
        href: "#", 
        role: "tab",
        data: {
          toggle: "tab",
          target: "##{cat.locale_key}"
        }
      }

      if !params[:action].eql?("index")
        attrs[:href] = categories_path
        attrs[:data][:toggle] = nil
      end

      attrs
    end

    # Use callbacks to share common setup or constraints between actions.
    def define_locale
      @locale = params[:locale].blank? ? Language.where(locale: I18n.locale).first : Language.find(params[:locale])
    end

    def set_details
      @categories = Category.all
      @category = Category.where(locale_key: params[:category_locale_key]).first
      @page = params[:page].blank? ? 1 : params[:page]
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:name, :about)
    end
end
