class TagsController < ApplicationController

  def index
    cloud_size = mobile_request? ? 10 : 30
    @tags = Tag.get_cloud(cloud_size)
    @user = @current_user || User.new
    respond_to do |format|
      format.html
    end
  end

  def load_texts
    redirect_to(root_url) and return unless request.xhr?
    query = ''
    tags = params[:tags].split(",")

    unless tags.blank?
      page = (params[:page].blank? || params[:page].to_i == 0) ? 1 : params[:page].to_i
      
      tags.each do |tag|
        query << "#{tag} "
      end

      @texts = Text.search_for({
          fields: ['tags.name'],
          opeartor: "AND",
          minimum_should_match: '100%',
          filter: {bool: {must: {term: {is_draft: false}}}}
        },query)
        .page(page)
        .per(30)
        .records
    end

    respond_to do |format|
      if tags.blank?
        format.json {render json: t(:select_one_tag), status: :unprocessable_entity}
      elsif @texts.blank?
        format.json {render json: t(:no_texts_with_tags), status: :unprocessable_entity}
      else
        format.html {render partial: "application/text/blocks", locals: {texts: @texts, container_class: 'tagsSection--tagsTexts'}}
      end
    end
  end

end