class SubscribersController < ApplicationController

  ## POST /subscribers
  ## Params: email
  ## Success: 
  ##    {
  ##      success: true
  ##      message: t(:successful_subscription)
  ##    }
  ## Error: Render model validation errors
  def create
    @subscriber = Subscriber.new(subscriber_params)
  	@subscriber.locale = I18n.locale.to_s
  	gb = Gibbon::API.new
  	respond_to do |format|
  		if @subscriber.save
  			gb.lists.subscribe({id: 'a1a59639b9', email: {email: @subscriber.email, uid: @subscriber.id}, merge_vars: {mc_language: I18n.locale.to_s}})
  			@response = {success: true, message: t(:successful_subscription)}
  			format.json {render json: @response}
  		else
  			format.json {render json: @subscriber.errors, status: :unprocessable_entity }
  		end
  	end
  end

  private

  def subscriber_params
  	params.require(:subscriber).permit(:email)
  end

end
