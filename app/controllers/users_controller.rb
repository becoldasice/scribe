## TODO: Review load_* methods. DRY!
## Maybe add them to ApplicationController and use Delegator??
class UsersController < ApplicationController
  
  before_action :set_user, only: [:show, :edit, :update, :destroy, :follow_or_unfollow, :profile, :following, :followers, :deactivate_account, :see_stats, :recommend]
  before_action :check_credentials, only: [:edit, :update, :destroy, :following, :followers, :timeline]
  before_action :check_permission, only: [:edit, :destroy, :see_stats]
  before_action :fill_suggestions, only: [:timeline, :profile]
  before_action :check_activity, only: [:profile]
  before_action :show do
    render_header(true)
    render_footer(true)
  end

  helper_method :link_facebook, :link_twitter, :link_instagram

  ## GET /u
  ## Params: id
  ## Rails generated resource
  def show
  end

  ## GET /u/id/edit
  ## Params: id
  ## Rails generated resource
  def edit
    @country_codes = Country.all.collect { |c| ["#{c[0]} - #{Country[c[1]].country_code}", Country[c[1]].country_code] }
    @phone_code_selected = @user.phone_country_code || Country[@user.country_code].country_code
  end

  ## GET /u/new
  ## Params: 
  ## Rails generated resource
  def new
    #@user = User.new
    redirect_to root_url
  end

  ## POST /reset_password
  ## Params: email
  ## Success: redirect_to login page
  ## Error: TODO
  def reset_password_email
    @user = User.where(email: params[:email]).first
    @user.send_reset_email if @user
    if @user.nil?
      redirect_to reset_password_url, flash: {error: t(:email_doesnt_exist)}
    else
      redirect_to root_url, notice: t(:email_sent)
    end
  end

  ## GET /password/reset/password_reset_token
  ## Params: password_reset_token
  ## Success: renders reset_password view
  ## Error: redirect_to login page with invalid confirmation token message
  def reset_password
    @user = User.where(password_reset_token: params[:password_reset_token]).first
    
    unless @user
      redirect_to root_url, notice: t(:invalid_confirmation_token)
    end
  end

  ## PUT /update_password
  ## Params: password_reset_token, password, password_confirmation
  ## Success: redirect_to login page with reset password success notice
  ## Error: Renders model errors
  def update_password
    @user = User.where(update_password_params).first
    respond_to do |format|
      if @user.update(user_params)
        @user.password_reset_token = nil
        @user.save
        format.html {redirect_to root_url, notice: t(:pass_reset_success)}
      else
        format.json {render json: @user.errors, status: :unprocessable_entity}
      end
    end
  end

  ## POST /save_profile_photo
  ## Params: profile_photo
  ## Success: Returns profile_photo url
  ## Error: Renders model errors

  ## POST /save_cover_photo
  ## Params: cover_photo
  ## Success: Returns cover_photo url
  ## Error: Renders model errors

  %w(profile_photo cover_photo).each do |att|
    define_method("save_#{att}") do
      @current_user.send("#{att}=", params[:user][att.to_sym])
      respond_to do |format|
        if @current_user.save
          format.json {render json: @current_user.send("#{att}").url, status: :ok}
        else
          format.json {render json: @current_user.errors, status: :unprocessable_entity}
        end
      end
    end
  end

  ## POST /u
  ## Params: email, email_confirmation, password, password_confirmation, first_name, last_name
  ## Success: redirect_to user profile
  ## Error: Renders model errors
  ## TODO: Review error handling
  def create
    @user = User.new(user_params)
    @user.last_login = Time.now

    respond_to do |format|
      if @user.save
        session[:user_id] = @user.id
        SystemMailer.delay({run_at: 10.seconds.from_now}).welcome_email(@user)
        format.json { render json: {url: discover_url}, status: :created, location: @user }
      else
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  ## POST /complete_registration
  ## Params: email, password, password_confirmation, twitter_uid
  ## Success: redirect_to user profile
  ## Error: Renders model errors
  ## TODO: Review error handling
  def complete_registration
    @user = User.where(email: params[:email]).first || User.where(:"#{session[:provider]}_uid" => session[:social_uid]).first
    attrs = {
      email: params[:email],
      last_login: Time.now,
      is_active: true
    }

    if @user.send("#{session[:provider]}_uid").nil?
      @social_user = User.where(:"#{session[:provider]}_uid" => session[:social_uid]).first
      user_attrs = @user.attributes.select{ |k,v| k.start_with?(session[:provider])}
      user_attrs.each{|k,v| user_attrs[k] = @social_user.send(k)}

      attrs.merge!(user_attrs)
    end
    @user.update(attrs)

    respond_to do |format|
      session[:provider] = nil
      session[:social_uid] = nil
      session[:user_id] = @user.id
      SystemMailer.delay({run_at: 10.seconds.from_now}).welcome_email(@user)
      redirect_url = (!@user.facebook_uid.nil? && !@user.done_facebook_follow_friends) ? facebook_friends_url : (request.referrer || discover_url )

      format.html { redirect_to redirect_url }
      format.json { render :show, status: :created, location: @user }
    end
  end

  ## PUT /finish_tour
  ## Params: 
  ## Success: Mark tour flag for user as true
  def finish_tour
    @current_user.done_update = true
    @current_user.save!
    respond_to do |format|
      format.json { render json: {success: true}, status: :ok}
    end
  end

  ## PUT /u/id
  ## Params: id
  ## Success: redirect_to user profile
  ## Error: Renders model errors
  ## TODO: Review error handling
  def update
    respond_to do |format|
      
      action = if params[:user].nil?
        @user.preferences.update_attributes(preferences_params)
      else
        @user.update(user_params)
      end

      if action
        format.json { render json: {url: user_show_url(@user)}, status: :ok, location: @user } if request.xhr?
        format.html { redirect_to user_show_url(@user) } unless request.xhr?
      else
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  ## POST /merge_accounts
  ## Params: 
  ## Success: Merge existent facebook account with current user
  def merge_accounts
    @merged_account = User.where('facebook_uid = ? AND id <> ?', @current_user.facebook_uid, @current_user.id).first
    User.merge(@current_user, @merged_account)

    respond_to do |format|
      flash[:notice] = t(:merged_with_success)
      format.json { render json: {success: true, curr: @current_user}, status: :ok }
    end
  end

  ## PUT /u/id/unlink_facebook
  ## Params: id
  ## Success: Unlink all facebook data from user

  ## PUT /u/id/unlink_twitter
  ## Params: id
  ## Success: Unlink all twitter data from user

  ## PUT /u/id/unlink_instagram
  ## Params: id
  ## Success: Unlink all instagram data from user
  %w(twitter facebook instagram).each do |service|
    define_method("unlink_#{service}") do
      attrs = @current_user.attributes.select{ |k,v| k.start_with?(service)}
      attrs.each{|k,v| attrs[k] = nil}
      @current_user.update(attrs)

      respond_to do |format|
        flash[:notice] = t(:removed_with_success)
        format.html { render partial: "users/social/link_#{service}", locals: {linked: false} }
      end
    end
  end

  ## DELETE /u/id
  ## Params: id
  ## Unused method
  ## Rails generated method
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url}
      format.json { head :no_content }
    end
  end

  ## GET /u/id/profile
  ## Params: id (friendly)
  ## Success: redirect_to user profile page
  ## Error: 404
  ## TODO: Error handling
  def profile
    @texts_page = params[:texts_page] || 1
    @texts = @user.texts.order(published_at: :desc).page(@texts_page).per(3).where(is_draft: false)

    @drafts_page = params[:drafts_page] || 1
    @drafts = @user.texts.order(created_at: :desc).page(@drafts_page).per(4).where(is_draft: true)
    
    respond_to do |format|
      format.html {render layout: !request.xhr?, status: :ok}
    end
  end

  ## GET /timeline
  ## Params: 
  ## Success: Renders entire timeline page if normal request, render timeline partial if AJAX request.
  ## TODO: Error handling
  def timeline
    @user = @current_user

    @timeline_page = params[:page].blank? ? 1 : params[:page]
    @tl = @current_user.timeline(@timeline_page)
    
    respond_to do |format|
      format.html {render layout: !request.xhr?, status: :ok}
    end

  end

  ## POST /follow_or_unfollow/id
  ## Params: id
  ## Success:
  ##    {
  ##      refreshed_counter: Followed user refreshed counter
  ##      refresh_who: Section to be updated
  ##      refreshed_action: Followed or unfollowed?
  ##    }
  ## Error: 
  ## TODO: Error handling
  def follow_or_unfollow
    was_following = @current_user.follow?(@user)
    was_following ? @current_user.unfollow(@user) : @current_user.follow(@user)
    respond_to do |format|
      format.json {
        render json: {
          his_refreshed_counter: t(:followers, count: @user.follower_count), 
          my_refreshed_counter: t(:following, count: @current_user.following_count),
          his_refreshed_container: "#follower-count-#{@user.id}", 
          my_refreshed_container: "#following-count-#{@current_user.id}",
          refreshed_current: t(:follow_or_unfollow, count: (!was_following ? 1 : 0)),
          refreshed_hover: t(:follow_or_unfollow, count: (!was_following ? 2 : 0)),
          old_class: !was_following ? 'follow-btn' : 'following-btn',
          refreshed_class: !was_following ? 'following-btn' : 'follow-btn'
        }
      }
    end
  end

  ## GET /u/id/following
  ## Params: id, page
  ## Success: Render list of users the followed by the selected user
  ## Error: 
  ## TODO: Error handling

  ## GET /u/id/followers
  ## Params: id, page
  ## Success: Render list of users the who follow the selected user
  ## Error: 
  ## TODO: Error handling

  %w(following followers).each do |action|
    define_method("#{action}") do
      @page = params[:page].blank? ? 1 : params[:page]
      @size = @user.send("#{action.singularize}_count")
      @users = User.includes(:texts).where(id: @user.send("#{action.pluralize}")).page(@page).per(9)

      respond_to do |format|
        format.html { render template: "users/following_or_followers", layout: !request.xhr?, locals: {section_id: "#{action}", section_title: "activerecord.attributes.user.#{action}", empty_rendering: t(action.to_sym, count: "0")} ,status: :ok}
      end
    end
  end

  ## GET /u/id/deactivate_account
  ## Params: id
  ## Success: Log user out
  ## Error: 
  ## TODO: Create user deactivation page
  def deactivate_account
    @user.deactivate_account
    respond_to do |format|
      format.json {render json: {redirect_url: logout_url}}
    end
  end

  ## GET /u/id/stats
  ## Params: id
  ## Success: Render stats page
  ## Error: 
  ## TODO: Error handling
  def see_stats
    @texts = @current_user.published_texts
    respond_to do |format|
      format.html
    end
  end
  
  ## GET /u/facebook_friends
  ## Params: 
  ## Success: Show facebook friends using Scribe to follow
  ## Error: 
  ## TODO: Error handling
  def facebook_friends
    @user = User.find(session[:user_id])
    @scribe_friends = User.friends_by_uid(@user)
    
    respond_to do |format|
      if !@user.done_facebook_follow_friends
        @user.joined_with_facebook(@scribe_friends)
        format.html
      else
        format.html { redirect_to root_url }
      end
    end
  end
  
  ## GET /u/follow_all_facebook_friends
  ## Params: 
  ## Success: Follow all user facebook friends
  ## Error: 
  ## TODO: Error handling and change to AJAX
  def follow_all_facebook_friends
    @scribe_friends = User.friends_by_uid(@current_user)
    
    unless @scribe_friends.blank?
      @scribe_friends.each do |sf|
        @current_user.follow(sf) unless @current_user.follow?(sf)
      end
    end
    
    respond_to do |format|
      format.html { redirect_to root_url }
    end
  end

  ## GET /u/follow_all_email_contacts
  ## Params: emails
  ## Success: Follow all user email fetched contacts
  def follow_all_email_contacts
    @contacts = User.where("email in (?)", params[:emails])

    unless @contacts.blank?
      @contacts.each do |c|
        @current_user.follow(c) unless @current_user.follow?(c)
      end
    end

    respond_to do |format|
      format.json {render json: {success: true}, status: :ok}
    end
  end

  ## POST /u/invite_all_contacts
  ## Params: email
  ## Success: Follow all user email fetched contacts
  def invite_all_email_contacts
    @contacts = params[:email]
    User.delay(run_at: 10.seconds.from_now).mass_invite(@current_user, @contacts)

    respond_to do |format|
      format.html {redirect_to timeline_url, notice: t(:contacts_invited_success)}
    end
  end

  ## GET /u/invite_contact
  ## Params: email
  ## Success: Send invitation to single contact
  ## Error: Warn user about invalid email
  def invite_single_contact
    contact = params[:email]
    respond_to do |format|
      if !User.where(email: contact).blank?
        format.json {render json: {result: t(:existing_contact)}, status: :ok}
      elsif User.invite_contact(@current_user, contact)
        format.json {render json: {result: t(:invited_contact)}, status: :ok}
      else
        format.json {render json: {result: t('activerecord.errors.messages.invalid_email')}, status: :ok}
      end
    end
  end

  ## POST /u/id/recommend/text_id
  ## Params: id, text_id
  ## Success: Recommend text or undo recommendation
  def recommend
    text = Text.friendly.find(params[:text_id])
    @user.recommend(text)
    respond_to do |format|
      status = I18n.t(:recommend, count: @user.recommended?(text) ? 1 : 0)
      text.reload
      counter = text.recommendations_count
      format.json {render json: {status: status, recommended: @user.recommended?(text), counter: counter}, status: :ok}
    end
  end
  
  private

    ## Callback helpers

    def set_user
      id = params[:id] || params[:user_id]
      @user = User.friendly.find(id)
    end

    def fill_suggestions
      @excluded_suggestions = []
      @excluded_suggestions = [@current_user.id] unless @current_user.blank?
      @excluded_suggestions << @current_user.following_ids unless @current_user.blank? || @current_user.following_ids.blank?
      @excluded_suggestions << @user.id unless @user.blank?
      
      @suggestions = User.where.not(id: @excluded_suggestions, is_active: false).limit(12).order("RANDOM()")
    end

    ## Meta tags helpers

    def facebook_meta
      prev = super
      unless @user.nil?
        prev[:og][:type] = "profile"
        prev[:og][:title] = @user.name
        prev[:og][:url] = user_show_url(@user)
        prev[:og][:image].unshift(@user.cover_photo.url) unless @user.cover_photo.blank?
        prev[:og][:image].unshift(@user.profile_photo.url) unless @user.profile_photo.blank?
        prev[:og][:description] = @user.bio || "#{@user.name} Scribe profile"
        set_meta_tags fb: {
          app_id: 330793013745555
        }
        set_meta_tags profile: {
          first_name: @user.first_name,
          last_name: @user.last_name,
          gender: @user.gender.eql?("M") ? "male" : "female"
        }
      end
      prev
    end

    def default_meta
      prev = super
      if !@user.nil?
        prev[:title] = "#{@user.name} - Scribe"
        prev[:description] = @user.bio unless @user.bio.nil?
      end
      prev
    end

    ## Auth helpers

    def check_permission
      if @user != @current_user
        respond_to do |format|
          format.html {redirect_to user_show_path(@user)}
        end
      end
    end

    def check_activity
      unless @user.is_active
        respond_to do |format|
          raise ActionController::RoutingError.new('Not Found')
        end
      end
    end

    ## Params helpers

    def subscriber_params
      params.require(:subscriber).permit(:email)
    end

    def update_password_params
      params.require(:user).permit(:password_reset_token)
    end

    def user_params
      params.require(:user)
        .permit(:email, :email_confirmation, :password, :password_confirmation, :first_name, :last_name, :country_code, :phone_country_code, :whatsapp_number, :gender, :bio, :website, :profile_photo, :slug, :public_email, :email_notification_on_follow)
    end

    def preferences_params
      params.require(:user_preference)
        .permit(:email_on_follow, :email_on_recommendations, :get_whatsapp_texts, :email_top_ten, :email_weekly_writers, :email_week_trends, :email_steve_suggestions)
    end

    def confirmation_params
      params.require(:user).permit(:email, :confirmation_token)
    end

    %w(facebook twitter instagram).each do |service|
      define_method("link_#{service}") do
        render_to_string partial: "users/social/link_#{service}", locals: {linked: !@current_user.send("#{service}_uid").nil?}
      end
    end
end