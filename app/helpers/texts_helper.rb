module TextsHelper

	def sanitized_content_for(tag,tag_for,content)
		attrs = {class: 'scribeGrid--column10 textHeader--title'}
		if content.blank?
			content = ''
			attrs[:class] << ' placeholdify'
			if tag_for.eql?(:sub_title)
				return nil
			else
				attrs[:data] = {placeholder: t(tag_for)}
			end
		end
		content_tag(tag, content, attrs)
	end
end
