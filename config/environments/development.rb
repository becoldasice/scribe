Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  config.serve_statics_assets = false

  config.mandrill_mailer.default_url_options = {:host => "localhost:3000"}
  config.mandrill_mailer.asset_host = "http://strong-auto-27-125696.sae1.nitrousbox.com:3000/"
  config.preload_frameworks = true
  config.allow_concurrency = true

  # development.rb (or use environment.rb to setup defaults for all)
  Paperclip.options[:log] = true
  #Paperclip.options[:command_path] = "/usr/bin/"
  #Paperclip::Attachment.default_options[:url] = ':class/:id/:style.:extension'
  #Paperclip::Attachment.default_options[:path] = ':rails_root/assets/:class/:id_partition/:style.:extension'
  config.facebook_app_id = 338859376272252
  config.facebook_app_secret = '63dceaf9edd961b9a23c2d9b07ff75dc'
  config.twitter_api_key = 'TcYfVYqXJTFDcVGs2oQMNSgYf'
  config.twitter_api_secret = 'BADmeC2vf706KAWD42uDa51UV57yAmJmsoe1CZEYEqXwXa8zSU'
  config.instagram_app_id = '6f40cd98473743dba73ee1e501de08f5'
  config.instagram_app_secret = '9a1f4b52d79f4afea1c6f3babe66f2a8'
  
  Elasticsearch::Model.client = Elasticsearch::Client.new({log: true, host: 'localhost.local:9200'})


  config.after_initialize do
    Bullet.enable = true
    Bullet.bullet_logger = true
  end

end