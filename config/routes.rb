## Routes structure:
## => http_methods route_name => controller#action, as: route_name (As define the route as a named route. Check: http://guides.rubyonrails.org/routing.html#naming-routes)
## => http_methods order: GET, POST, PUT, DELETE
## => Indentation: One empty line between method changes. One empty line and one commented line before new controller route/resource definition
Rails.application.routes.draw do

    ## Application controller routes
    root to: "application#landing"

    get "search" => "application#search", as: "search"
    get "/404", :to => "application#four_oh_four"
    get "/contacts/:importer/callback" => "application#contacts_callback"
    get "/descubra" => "application#browse_by", type: "discover",  as: "discover"
    get "/hot" => "application#browse_by", type: "popular", as: "popular"

    post "/shortener/:text" => "application#shorten_url", as: "shortener"

    ## Session controller routes
    get "login" => "session#new", as: "login"
    get "logout" => "session#destroy", as: "logout"
    get "auth/facebook/callback" => "session#facebook_auth", as: "facebook_auth"
    get "auth/twitter/callback" => "session#twitter_auth", as: "twitter_auth"
    get "auth/instagram/callback" => "session#instagram_auth", as: "instagram_auth"

    post "authenticate" => "session#create", as: "authenticate"

    ## Users controller routes
    get "password/reset/:password_reset_token" => "users#reset_password", as: "reset_password_edit"
    get "timeline" => "users#timeline", as: "timeline"
    get "facebook_friends" => "users#facebook_friends", as: "facebook_friends"
    get "follow_all_facebook_friends" => "users#follow_all_facebook_friends", as: "follow_all_facebook_friends"

    post "save_profile_photo" => "users#save_profile_photo", as: "save_profile_photo"
    post "save_cover_photo" => "users#save_cover_photo", as: "save_cover_photo"
    post "reset_password" => "users#reset_password_email", as: "reset_password"
    post "follow_or_unfollow/:id" => "users#follow_or_unfollow", constraints: {id: /.*/}, as: "follow_or_unfollow"
    post "follow_all_email_contacts" => "users#follow_all_email_contacts", as: "follow_all_email_contacts"
    post "invite_friend" => "users#invite_single_contact", as: "invite_friend"
    post "invite_all_contacts" => "users#invite_all_email_contacts", as: "invite_all_contacts"
    post "merge_accounts" => "users#merge_accounts", as: "merge_accounts"
    post "complete_registration" => "users#complete_registration", as: "complete_registration"

    put "update_password" => "users#update_password", as: "update_password"
    put "finish_tour" => "users#finish_tour", as: "finish_tour"

    ## Texts controller routes
    get "load_search_results" => "texts#load_search_results", as: "load_search_results"

    post ":id/create_tag_path" => "texts#create_tag", as: "create_tag"

    delete ":id/delete_tag/:tag_id" => "texts#delete_tag", as: "delete_tag"

    ## Categories controller routes
    get "/load_from/:category" => "categories#load_from", as: "load_from"
    get "c/:locale" => "categories#index", as: "category_with_locale"

    ## Tags controller routes
    get "/tags" => "tags#index", as: "tags"
    get "/load_texts" => "tags#load_texts", as: "load_from_tags"

    ## Categories resources
    resources :categories, param: :locale_key, only: [:index], path: "c" do
        get "last/:locale" => "categories#last_texts_full", as: "last_texts_full"
        get "most/:locale" => "categories#most_viewed_full", as: "most_viewed_full"
    end

    ## Texts resources
    resources :texts, only: [:new, :create, :edit, :update, :show, :destroy], path: "t" do
        get "commentline" => "texts#commentline", as: "commentline"
        
        post "publish" => "texts#publish", as: "publish"
        post "post_comment" => "texts#post_comment", as: "post_comment"
        post "recommendations_details" => "texts#recommendations_details", as: "recommendations_details"
        post "cover_text_photo" => "texts#save_text_cover", as: "insert_cover"
        post "insert_text_image" => "texts#save_text_image", as: "insert_image"
        
        delete "delete_comment" => "texts#delete_comment", as: "delete_comment"
        delete "delete_text_image" => "texts#delete_text_image", as: "delete_image"
    end

    ## Users resources
    resources :users, :param => :id, constraints: {id: /.*/}, only: [:new, :create, :edit, :update], path: "u" do
        get "/profile" => "users#profile", as: "show"
        get "/following" => "users#following", as: "following"
        get "/followers" => "users#followers", as: "followers"
        get "/stats" => "users#see_stats", as: "see_stats"
        get "/link_facebook" => "users#link_facebook_account", as: "link_facebook"

        post "/deactivate_account" => "users#deactivate_account", as: "deactivate_account"

        put "/unlink_facebook" => "users#unlink_facebook", as: "unlink_facebook"
        put "/unlink_twitter" => "users#unlink_twitter", as: "unlink_twitter"
        put "/unlink_instagram" => "users#unlink_instagram", as: "unlink_instagram"

        member do
            post "/recommend/:text_id/" => "users#recommend", as: "recommend"
        end
    end

    devise_for :master_scribers
    authenticate :master_scriber do
      mount RailsAdmin::Engine => '/admin'
    end
end
