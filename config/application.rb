require File.expand_path('../boot', __FILE__)

require 'rails/all'
require 'http_accept_language'
require 'elasticsearch/rails/instrumentation'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ScribeApp
  
  class Application < Rails::Application
    config.generators do |g| 
        g.test_framework :rspec,
            fixtures: true ,
            view_specs: false,
            helper_specs: false,
            routing_specs: false,
            controller_specs: true,
            request_specs: false
        g.fixture_replacement :factory_girl, dir: "spec/factories"
    end

    config.exceptions_app = self.routes
    config.autoload_paths += %W{#{config.root}/app/models/ckeditor}
    Koala.config.api_version = "v2.0"

    config.default_locale_url = Rails.env.eql?("development") ? "http://localhost.local:3000" : "http://wescribe.co"
    config.en_locale_url = Rails.env.eql?("development") ? "http://en.localhost.local:3000" : "http://en.wescribe.co"
    config.es_locale_url = Rails.env.eql?("development") ? "http://es.localhost.local:3000" : "http://es.wescribe.co"
  end
end
