module Paperclip
	
	Paperclip.interpolates("user_id") do |attachment, style|
		attachment.instance.user.id.parametrize
	end

	Paperclip.interpolates("text_id") do |attachment, style|
		attachment.instance.text_image.text_id
	end

	Paperclip.interpolates(:placeholder_profile) do |attachment, style|
	  ActionController::Base.helpers.asset_path("default_photo.png")
	end

	Paperclip.interpolates(:placeholder_cover) do |attachment, style|
	  ActionController::Base.helpers.asset_path("default_cover.jpg")
	end

	Paperclip::Attachment.default_options.merge!(
	  url:                  ':s3_alias_url',
	  path:                 ':class/:attachment/:id/:style/:filename',
	  storage:              :s3,
	  s3_credentials:       Rails.configuration.aws,
	  s3_permissions:       :private,
	  s3_host_alias: 				'd2ugcna7ztqj8b.cloudfront.net',
	  s3_host_name: 				's3-sa-east-1.amazonaws.com',
	  s3_protocol:          'https'
	)

end