Rails.application.config.middleware.use OmniAuth::Builder do
	#TODO: Set to environment variables
	provider :facebook, Rails.configuration.facebook_app_id, Rails.configuration.facebook_app_secret,
        image_size: 'large', 
        display: 'popup', 
        info_fields: 'first_name, last_name, email, gender, link, cover', 
        secure_image_url: true,
        scope: 'email, user_friends'
    provider :twitter, Rails.configuration.twitter_api_key, Rails.configuration.twitter_api_secret, {
    	secure_image_url: true,
    	image_size: "original"
    }

    provider :instagram, Rails.configuration.instagram_app_id, Rails.configuration.instagram_app_secret
end