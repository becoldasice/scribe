RailsAdmin.config do |config|
  
  config.main_app_name = "Scribe"
  config.excluded_models = ["MasterScriber", "Notifications", "Inkwell::BlogItem", "Inkwell::TimelineItem", "Inkwell::Following",
                            "Inkwell::FavoriteItem", "Inkwell::BlogItemCategory", "Inkwell::CommunityUser", "Asset", "AttachmentFile", "Picture"]

  config.model "User" do
    
    list do
      field :id
      field :name
      field :email
      field :is_active
      field :last_login
      field :prefered_language

      field :user_url do
        formatted_value do
          %{<a href="#{bindings[:controller].main_app.user_show_url(bindings[:object])}" target="_blank">#{bindings[:object].name}<a>}.html_safe
        end
      end

      field :texts_count do
        formatted_value do
          bindings[:object].texts.count
        end
      end

      field :published_count do
        formatted_value do
          bindings[:object].published_texts.count
        end
      end

      field :drafts_count do
        formatted_value do
          bindings[:object].drafts.count
        end
      end

      field :total_views do
        formatted_value do
          bindings[:object].published_texts.sum :unique_views_counter
        end
      end

      field :views_per_text do
        formatted_value do
          texts_count = bindings[:object].published_texts.count
          views = bindings[:object].published_texts.sum :unique_views_counter

          (texts_count > 0 && views > 0) ? views/texts_count : 0
        end
      end

      field :texts_recommendations do
        formatted_value do
          bindings[:object].published_texts.sum :recommendations_count
        end
      end

      field :recommendations_per_text do
        formatted_value do
          texts_count = bindings[:object].published_texts.count
          recommendations = bindings[:object].published_texts.sum :recommendations_count

          (texts_count > 0 && recommendations > 0) ? recommendations/texts_count : 0
        end
      end
    end

    export do
      field :id
      field :name
      field :email
      field :is_active
      field :prefered_language
      
      field :last_login do
        formatted_value do
          bindings[:object].last_login.strftime("%d/%m/%Y") unless bindings[:object].last_login.blank?
        end
      end

      field :updated_at do
        formatted_value do
          bindings[:object].updated_at.strftime("%d/%m/%Y") unless bindings[:object].updated_at.blank?
        end
      end

      field :texts_count do
        formatted_value do
          bindings[:object].texts.count
        end
      end

      field :published_count do
        formatted_value do
          bindings[:object].published_texts.count
        end
      end

      field :drafts_count do
        formatted_value do
          bindings[:object].drafts.count
        end
      end

      field :total_views do
        formatted_value do
          bindings[:object].published_texts.sum :unique_views_counter
        end
      end

      field :views_per_text do
        formatted_value do
          texts_count = bindings[:object].published_texts.count
          views = bindings[:object].published_texts.sum :unique_views_counter

          (texts_count > 0 && views > 0) ? views/texts_count : 0
        end
      end

      field :texts_recommendations do
        formatted_value do
          bindings[:object].published_texts.sum :recommendations_count
        end
      end

      field :recommendations_per_text do
        formatted_value do
          texts_count = bindings[:object].published_texts.count
          recommendations = bindings[:object].published_texts.sum :recommendations_count

          (texts_count > 0 && recommendations > 0) ? recommendations/texts_count : 0
        end
      end
    end

  end
end