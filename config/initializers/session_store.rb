# Be sure to restart your server when you modify this file.

Rails.application.config.session_store :cookie_store, 
  key: '_scribe-app_session',
  domain: ['.localhost.local', '.ngrok.com']
##  domain: Rails.env.eql?("development") ? '.localhost.local' : '.wescribe.co'
