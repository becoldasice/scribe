module Inkwell
  class Engine < Rails::Engine
    config.post_table = :texts
    config.user_table = :users
  end
end