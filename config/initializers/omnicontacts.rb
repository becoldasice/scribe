require "omnicontacts"

Rails.application.middleware.use OmniContacts::Builder do
  importer :gmail, ENV["GMAIL_CLIENT_ID"], ENV["GMAIL_CLIENT_SECRET"], :max_results => 3000
  importer :yahoo, ENV["YAHOO_CONSUMER_ID"], ENV["YAHOO_CONSUMER_SECRET"]
  importer :hotmail, ENV["MS_CLIENT_ID"], ENV["MS_CLIENT_SECRET"]
end