# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

every 1.day, at: "5am" do
  rake "sitemap:generate"
end
every :monday, at: "9am" do
  runner "User::MailerTasks.top_ten"
end

every :tuesday, at: "9am" do
  runner "User::MailerTasks.weekly_writers"
end

every :wednesday, at: "9am" do
  runner "User::MailerTasks.steve_suggestions('wed')"
end

every :thursday, at: "9am" do
  runner "User::MailerTasks.week_trends"
end

every :friday, at: "9am" do
  runner "User::MailerTasks.steve_suggestions('fri')"
end