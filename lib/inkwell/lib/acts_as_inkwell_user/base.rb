module Inkwell
  module ActsAsInkwellUser

    module InstanceMethods
      include ::Inkwell::Constants
      include ::Inkwell::Common

      def follow(user)
        raise "user tries to follow already followed user" if self.follow? user
        raise "user tries to follow himself." if self == user

        ::Inkwell::Following.create :follower_id => self.id, :followed_id => user.id

        self.following_count += 1
        self.save

        user.follower_count += 1
        user.save

        post_class = Object.const_get ::Inkwell::Engine::config.post_table.to_s.singularize.capitalize
        user_id_attr = "#{::Inkwell::Engine::config.user_table.to_s.singularize}_id"
        ::Inkwell::BlogItem.where(:owner_id => user.id, :owner_type => OwnerTypes::USER).order("created_at DESC").each do |blog_item|
          if blog_item.is_reblog
            item_class = blog_item.item_type == ItemTypes::COMMENT ? ::Inkwell::Comment : post_class
            next if item_class.find(blog_item.item_id).send(user_id_attr) == self.id
          end

          item = TimelineItem.where(:item_id => blog_item.item_id, :owner_id => self.id, :owner_type => OwnerTypes::USER, :item_type => blog_item.item_type).first
          if item
            item.has_many_sources = true unless item.has_many_sources
            sources = ActiveSupport::JSON.decode item.from_source
            
            sources << Hash['user_id' => user.id, 'type' => blog_item.is_reblog ? 'reblog' : 'following']

            item.from_source = ActiveSupport::JSON.encode sources
            item.save
          else
            sources = []
            sources << Hash['user_id' => user.id, 'type' => blog_item.is_reblog ? 'reblog' : 'following']
            ::Inkwell::TimelineItem.create :item_id => blog_item.item_id, :item_type => blog_item.item_type, :owner_id => self.id, :owner_type => OwnerTypes::USER,
                                           :from_source => ActiveSupport::JSON.encode(sources), :created_at => blog_item.created_at
          end
        end
      end

    end
  end
end